import RNLanguages from 'react-native-languages';
import i18n from 'i18n-js';
import { AsyncStorage, I18nManager } from 'react-native';
import RNRestart from "react-native-restart";

import ar from './translations/ar.json';
import en from './translations/en.json';

i18n.locale = RNLanguages.language;
i18n.fallbacks = true;

export const I18n = (translate) => {
    i18n.translations = { ar, en };
    i18n.defaultLocale = I18nManager.isRTL ? 'ar': 'en';
    i18n.locale = I18nManager.isRTL ? 'ar': 'en';
    return i18n.t(translate);
};

I18nManager.allowRTL(i18n.locale in i18n.translations);

i18n.start  = I18nManager.isRTL ? 'right' : 'left';
i18n.end   = I18nManager.isRTL ? 'left' : 'right';

export const changeLanguage = (lang) => {
    i18n.defaultLocale = lang;
    i18n.locale = lang;
    lang == 'ar' ? I18nManager.forceRTL(true): I18nManager.forceRTL(false)
    AsyncStorage.setItem('USER_LANGUAGE', lang).then(() => {
        RNRestart.Restart();
    })
}

i18n.translations = { ar, en };

export default i18n;