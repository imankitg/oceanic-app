'use strict';

import PayPal from 'react-native-paypal-wrapper';

const Constants = {
    url: {
        wc: 'https://demo.tradebakerz.com',
    },
    Keys: {

        ConsumeKey: 'ck_b7edea5027e30fc31a5a32b141f8f3825ed77119',
        ConsumerSecret: 'cs_72d42e8e21c79484cbb409a93449b8f4c0134e02',
        PaypalClientId: 'AVV7COoIlpQUFIrDhmVmMF4XXUhLWw3liXp5J6-wqC6zqn5SyGds5VZLvOAqBHr7h-uEIRuhW7eo8Ow4', // Paypal Client ID
        PaypalMethod: PayPal.SANDBOX // 3 env available: NO_NETWORK, SANDBOX, PRODUCTION
    }
};

module.exports = Constants;
