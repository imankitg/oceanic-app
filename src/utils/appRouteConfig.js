import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import StyleVars from '../Style/StyleVars';
import SharedStyles from '../Style/SharedStyles';


function headerLeft(navigation, options) {
    return (
        <View style={SharedStyles.headerLeft}>
            <TouchableOpacity onPress={() => navigation.navigate('DrawerOpen')}>
                <Icon name="bars" size={30} color={StyleVars.Colors.primary} />
            </TouchableOpacity>
        </View>
    )
}

function headerRight(navigation, options, screenProps) {
    return (
        <View style={SharedStyles.headerRight}>
            <TouchableOpacity onPress={() => { navigation.navigate("Cart") }} style={{ padding: 10 }}>
                <Icon name="shopping-cart" size={30} style={SharedStyles.cartIcon} />
                {screenProps.cartData.cart.length > 0 ? <Text style={SharedStyles.counter}>{screenProps.cartData.cart.length}</Text> : null}
            </TouchableOpacity>
        </View>
    )
}


exports.StackNavigationOptions = (options) => {

    return ({ navigation, screenProps }) => {
        let { state } = navigation;

        return {
            title: (state.params && state.params.title) || options && options.title,
            headerStyle: SharedStyles.heading,
            headerTitleStyle: SharedStyles.headingText,
            headerTintColor: '#fff',
            headerRight: (options && options.headerRight) ? headerRight(navigation, options, screenProps) : <View />
        }
    };
};

exports.DrawerNavigationOptions = (options) => {

    return ({ navigation, screenProps }) => {
        let { state } = navigation;

        return {
            title: (state.params && state.params.title) || options && options.title,
            headerStyle: SharedStyles.heading,
            headerTitleStyle: SharedStyles.headingText,
            headerTintColor: '#fff',
            headerLeft: headerLeft(navigation, options),
            headerRight: (options && options.headerRight) ? headerRight(navigation, options, screenProps) : <View />
        }
    };
};