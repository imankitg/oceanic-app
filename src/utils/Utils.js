import { Alert } from "react-native";
import { LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk'

export const FBAuth = () => {
    return new Promise((resolve, reject) => {
        LoginManager.logOut();
        LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
            (result) => {
                if (!result.isCancelled) {
                    const infoRequest = new GraphRequest(
                        '/me',
                        {
                            parameters: {
                                'fields': {
                                    'string': 'email, first_name, middle_name, last_name, gender'
                                }
                            }
                        },
                        (error, result) => {
                            if (error) {
                                return reject(error);
                            }
                            resolve(result);
                        }
                    );
                    new GraphRequestManager().addRequest(infoRequest).start();
                }
            },
            (error) => {
                reject(error);
            }
        )
            .catch((err) => {
                Alert.alert(
                    'FBAuth Failed',
                    'FBAuth ' + err
                );
            });
    });
};