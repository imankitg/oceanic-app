import WooCommerceApi from "../../WooCommerce/Api";

const Api = {
    fetchOrders: function (per_page, page) {
        let params = {};
        per_page && (params.per_page = per_page);
        page && (params.page = page);
        return WooCommerceApi.get('orders', params)
    },
    updateOrders: function (id, status, u_id) {
        if(u_id) {
            return WooCommerceApi.put(`orders/${id}`, {
                transaction_id: u_id,
                status: 'processing'
            })
        }
        return WooCommerceApi.put(`orders/${id}`, {
            status: status
        })
    }
};

export default Api;