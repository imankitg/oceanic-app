import WooCommerceApi from "../../WooCommerce/Api";

const Api = {
    allCustomers: function () {
        return WooCommerceApi.get('customers')
    },
    customer: function (userId) {
        return WooCommerceApi.get('customers/' + userId)
    },
    updateCustomer: function (userId, data) {
        return WooCommerceApi.put('customers/' + userId, data)
    }
};

export default Api;