import WooCommerceApi from "../../WooCommerce/Api";

const Api = {
    retrieveShippingZone: function () {
        return WooCommerceApi.get(`shipping/zones`)
    },
    retrieveShippingZoneLocation: function (id) {
        return WooCommerceApi.get(`shipping/zones/${id}/locations`)
    },
    retrieveShippingZoneRates: function (id) {
        return WooCommerceApi.get(`shipping/zones/${id}/methods`)
    }
};
export default Api;