import WooCommerceApi from "../../WooCommerce/Api";
import Const from '../../WooCommerce/Config';
import axios from 'axios';
import i18n from '../../i18n';

const Api = {
    fetchProducts: function (category, per_page, page, language) {
        return WooCommerceApi.get(`products`, {
            per_page,
            page,
            category,
            lang: language || 'en'
        })
    },
    fetchSingleProduct: function (id) {
        return WooCommerceApi.get(`products/${id}`)
    },

    fetchTopProducts: function (payload, check, lang) {
        console.log('payload, check, lang', payload, check, lang)
        let payloadData = Object.assign({}, payload);
        if ((payloadData != undefined) && !check) {
            payloadData.min_price = 0;
            payloadData.max_price = 0;
        }
        return WooCommerceApi.get('products', {
            lang,
            payloadData
        })
    },

    fetchVariationProducts: function (id) {
        return WooCommerceApi.get(`products/${id}/variations`, {
            // order: 'asc'
        })
    },

    BestSellingProductsDetails: function (payload, language) {
        return WooCommerceApi.get(`products`, {
            lang: language || 'en',
            include: payload
        })
    },

    categories: function (language) {
        return WooCommerceApi.get('products/categories', {
            lang: language || 'en'
        })
    },

    getReviews: function (id) {
        return WooCommerceApi.get(`products/${id}/reviews`)
    },

    PostReview: function (id, state) {
        return WooCommerceApi.post(`products/${id}/reviews`, {
            review: state.Feedback,
            name: state.FullName,
            rating: state.ratings,
            email: state.email
        })
    },

    paymentMethod: function () {
        var data = {
            enabled: true
        };
        return WooCommerceApi.get('payment_gateways');
    },

    bestSellingProduct: function (date_max, date_min, language) {
        return WooCommerceApi.get(`reports/top_sellers`, {
            lang: language || 'en',
            date_min: `${date_min[0]}`,
            date_max: `${date_max[0]}`
        });
    },

    paymentFunction: function (method) {
        var data = {
            enabled: true
        };
        return WooCommerceApi.put(`payment_gateways/${method}`, data);
    },

    placeOrder: function (order) {
        return WooCommerceApi.post('orders', order)
    },

    getPage: function () {
        return axios.get(`${Const.url.wc}/wp-json/wp/v2/pages`)
    },

    getPageWithSlug: function (slug) {
        return axios.get(`${Const.url.wc}/wp-json/wp/v2/pages?slug=${slug}`)
    },

    convertingURL: function (url) {
        // return url.replace('https', 'http');
        return url;
    },

    convertingLanguage: function (changeLanguage) {
        let lang = i18n.locale;
        if (changeLanguage.charAt(0) == '[' || changeLanguage.charAt(3) == '[') {
            return changeLanguage.split(`[:${lang}]`)[1].split('[', 1);
        }
        else return changeLanguage;
    }
};


export default Api;
