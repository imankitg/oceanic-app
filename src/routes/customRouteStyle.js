import { StyleSheet, Platform, I18nManager } from 'react-native';
import { Header } from 'react-navigation';
let headerHeight = Header.HEIGHT;

export default CustomRouteStyle = StyleSheet.create({
    headerTitleStyle: {
        fontFamily: 'Lato-Bold',
        textAlign: 'center',
        alignSelf:'center',
        fontSize: 20,
        flex: 1
    },
    buttonStyle: {
        height: headerHeight,
        paddingHorizontal: 15,
        justifyContent: 'center',
        position: 'relative',
    },
    leftWrapper: {
        flexDirection: 'row',
        height: headerHeight,
        alignItems: 'center'
    },
    logoImageStyle: {
        width: 130,
        height: headerHeight,
        resizeMode: 'contain'
    },
    cartBadgeView: {
        backgroundColor: '#ff8400',
        borderRadius: Platform.OS == 'ios' ? 10 : 50,
        width: 20,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: Platform.OS == 'ios' ? 10 : 5,
        right: I18nManager.isRTL ? null: 5,
        left: I18nManager.isRTL ? 5 : null,
        zIndex: 5
    },
    cartBadgeText: {
        color: '#fff',
        fontSize: 12,
        fontFamily: 'Lato-Bold'
    }
});