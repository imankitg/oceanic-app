import React from 'react';
import { View, I18nManager } from 'react-native';
import { createStackNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';

import DrawerMenu from '../components/drawerMenu/DrawerMenu';
import Home from '../components/home/Home';
import ProductByCategory from '../components/productByCategory/ProductByCategory';
import Cart from '../components/cart/Cart';
import Profile from '../components/profile/Profile';
import PersonalInformation from '../components/profile/PersonalInformation';
import BillingInformation from '../components/profile/BillingInformation';
import ShippingInformation from '../components/profile/ShippingInformation';
import Checkout from '../components/checkout/Checkout';
import Payment from '../components/checkout/Payment';
import selectCard from '../components/checkout/selectCard';
import Confirm from '../components/checkout/Confirm';
import Complete from '../components/checkout/Complete';
import FeedBack from '../components/FeedBack/FeedBack.js';
import Filter from '../components/Filter/Filter.js'
import CustomerOrder from '../components/orders/CustomerOrder';
import ShippingAdress from "../components/orders/CustomerShippingAddress";
import Login from '../components/auth/Login';
import Signup from '../components/auth/Signup';
import WishList from '../components/wishList/WishList';
import Categories from '../components/categories/Categories';
import ShopNew from '../components/shopNew/ShopNew';
import ProductDetails from '../components/shopNew/ProductDetails';
import ThemeCustomization from '../components/changeThemeColor/ThemeCustomization';
import CustomRouteStyle from './customRouteStyle';
import CustomHeaderRight, { HomeHeaderLeftIcon, HomeHeaderBackIcon, MainHeaderTitle } from './appRouteCustoms';
import i18n from '../../i18n';

const LoggedIn = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => {
            return {
                headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />,
                headerLeft: <HomeHeaderLeftIcon home='homelogo' _onPress={() => navigation.openDrawer()} />
            }
        }
    },
    ProductByCategory: {
        screen: ProductByCategory,
        navigationOptions: ({ navigation }) => {
            const categoryTitle = navigation.state.params.categoryTitle;
            return {
                headerTitle: <MainHeaderTitle text={categoryTitle} />,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderBackIcon _onPress={() => navigation.goBack()} />,
                headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />
            }
        }
    },
    'Cart': {
        screen: Cart,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.CART')} />,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderLeftIcon _onPress={() => navigation.openDrawer()} />,
                headerRight: <View />
            }
        }
    },
    'Profile': {
        screen: Profile,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.PROFILE')} />,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderLeftIcon _onPress={() => navigation.openDrawer()} />,
                headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />
            }
        }
    },
    'PersonalInformation': {
        screen: PersonalInformation,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.PERSONAL_INFORMATION')} />,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderBackIcon _onPress={() => navigation.goBack()} />,
                headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />
            }
        }
    },
    'BillingInformation': {
        screen: BillingInformation,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.BILLING_INFORMATION')} />,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderBackIcon _onPress={() => navigation.goBack()} />,
                headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />
            }
        }
    },
    'ShippingInformation': {
        screen: ShippingInformation,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.SHIPPING_INFORMATION')} />,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderBackIcon _onPress={() => navigation.goBack()} />,
                headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />
            }
        }
    },
    'Checkout': {
        screen: Checkout,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.CHECKOUT')} />,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderBackIcon _onPress={() => navigation.goBack()} />,
                headerRight: <View />
            }
        }
    },
    'Payment': {
        screen: Payment,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.CHECKOUT')} />,
            headerTitleStyle: CustomRouteStyle.headerTitleStyle,
            headerLeft: <HomeHeaderBackIcon navigation={navigation} _onPress={() => navigation.goBack()} />,
            headerRight: <View />
        })
    },
    'selectCard': {
        screen: selectCard,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.CHECKOUT')} />,
            headerTitleStyle: CustomRouteStyle.headerTitleStyle,
            headerLeft: <HomeHeaderBackIcon navigation={navigation} _onPress={() => navigation.goBack()} />,
            headerRight: <View />
        })
    },
    'Confirm': {
        screen: Confirm,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.CHECKOUT')} />,
            headerTitleStyle: CustomRouteStyle.headerTitleStyle,
            headerLeft: <HomeHeaderBackIcon navigation={navigation} _onPress={() => navigation.goBack()} />,
            headerRight: <View />
        })
    },
    'Complete': {
        screen: Complete,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.CHECKOUT')} />,
            headerTitleStyle: CustomRouteStyle.headerTitleStyle,
            headerLeft: <HomeHeaderLeftIcon _onPress={() => navigation.openDrawer()} />,
            headerRight: <View />
        })
    },
    'Orders': {
        screen: CustomerOrder,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.ORDERS')} />,
            headerTitleStyle: CustomRouteStyle.headerTitleStyle,
            headerLeft: <HomeHeaderLeftIcon _onPress={() => navigation.openDrawer()} />,
            headerRight: <CustomHeaderRight navigation={navigation} _onPress={() => navigation.navigate('Cart')} />
        })
    },
    'ShippingAdress': {
        screen: ShippingAdress,
        navigationOptions: ({ navigation }) => {
            const categoryTitle = navigation.state.params.categoryTitle;
            return {
                title: `Order#${categoryTitle}`,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderBackIcon navigation={navigation} _onPress={() => navigation.goBack()} />,
                headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />
            }
        }
    },
    'WishList': {
        screen: WishList,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.WISHLIST')} />,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderLeftIcon _onPress={() => navigation.openDrawer()} />,
                headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart', navigation.state.params)} />
            }
        }
    },
    'Categories': {
        screen: Categories,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.CATEGORIES')} />,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderLeftIcon navigation={navigation} _onPress={() => navigation.openDrawer()} />,
                headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />

            }
        }
    },
    'ShopNew': {
        screen: ShopNew,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.SHOP')} />,
            headerTitleStyle: CustomRouteStyle.headerTitleStyle,
            headerLeft: <HomeHeaderLeftIcon _onPress={() => navigation.openDrawer()} />,
            headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />
        })
    },
    'ShopNewDetail': {
        screen: ProductDetails,
        navigationOptions: ({ navigation }) => {
            return {
                headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.PRODUCT')} />,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle,
                headerLeft: <HomeHeaderBackIcon navigation={navigation} _onPress={() => navigation.goBack()} />,
                headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />
            }
        }
    },
    'Filter': {
        screen: Filter,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.SEARCH_BY_FILTERS')} />,
            headerTitleStyle: CustomRouteStyle.headerTitleStyle,
            headerLeft: <HomeHeaderBackIcon _onPress={() => navigation.navigate('ShopNew')} iconName='md-close' />,
            headerRight: <View />
        })
    },
    'ColorTheme': {
        screen: ThemeCustomization,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.COLOR_THEME')} />,
            headerLeft: <HomeHeaderLeftIcon _onPress={() => navigation.openDrawer()} />,
            headerRight: <View />
        })
    },
    'FeedBack': {
        screen: FeedBack,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <MainHeaderTitle text={i18n.t('Routes_Component.FEEDBACK')} />,
            headerTitleStyle: CustomRouteStyle.headerTitleStyle,
            headerLeft: <HomeHeaderBackIcon navigation={navigation} _onPress={() => navigation.goBack()} />,
            headerRight: <CustomHeaderRight _onPress={() => navigation.navigate('Cart')} />
        })
    },
    'Login': {
        screen: Login,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <HomeHeaderLeftIcon home='homelogo' _onPress={() => navigation.openDrawer()} />,
            headerRight: <View />
        })
    }
},
    {
        initialRouteName: 'Home'
    });

const AppNavigator = createDrawerNavigator({
    Home: { screen: LoggedIn }
}, {
        contentComponent: DrawerMenu,
        drawerPosition: I18nManager.isRTL ? 'right' : 'left',
    });

export default createAppContainer(AppNavigator);
