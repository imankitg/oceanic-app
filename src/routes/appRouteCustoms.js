import React from 'react';
import { Image, Text, View, TouchableOpacity, I18nManager } from 'react-native';
import CustomRouteStyle from './customRouteStyle';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import customRouteStyle from './customRouteStyle';

class HomeHeaderBack extends React.Component {
    render() {
        let { _onPress, iconName, primaryColor } = this.props;
        return (
            <TouchableOpacity style={CustomRouteStyle.buttonStyle} onPress={_onPress}>
                {
                    I18nManager.isRTL ? 
                        <Icon name={iconName ? iconName : 'ios-arrow-forward'} size={28} color={primaryColor} />: 
                        <Icon name={iconName ? iconName : 'ios-arrow-back'} size={28} color={primaryColor} />
                }
            </TouchableOpacity>
        )
    }
};

class HomeHeaderLeft extends React.Component {
    render() {
        let { _onPress, home, primaryColor } = this.props;
        return (
            <View style={CustomRouteStyle.leftWrapper}>
                <TouchableOpacity onPress={_onPress} activeOpacity={0.5} style={CustomRouteStyle.buttonStyle}>
                    <Icon name='md-menu' size={28} color={primaryColor} />
                </TouchableOpacity>
                {home === 'homelogo' ? <Image style={CustomRouteStyle.logoImageStyle} source={require('../assets/images/logo.png')} resizeMethod={'resize'} /> : null}
            </View>
        )
    }
};

class HeaderTitle extends React.Component {
    render() {
        let { text, secondaryColor } = this.props;
        return <Text style={[customRouteStyle.headerTitleStyle, { color: secondaryColor }]}>{text}</Text>
    }
};


class CustomHeaderRight extends React.Component {
    render() {
        let { _onPress, cartLength, primaryColor, secondaryColor } = this.props;
        return (
            <TouchableOpacity style={CustomRouteStyle.buttonStyle} onPress={_onPress} activeOpacity={0.5}>
                {cartLength ?
                    <View style={[CustomRouteStyle.cartBadgeView, { backgroundColor: primaryColor }]}>
                        <Text style={CustomRouteStyle.cartBadgeText}>{cartLength}</Text>
                    </View> : null
                }
                <MaterialIcon name='shopping-cart' size={32} color={secondaryColor} />
            </TouchableOpacity>
        )
    }
}

function mapStateToProps(state) {
    return {
        cartLength: state.cartData.cart.length,
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor,
        languageFlag: state.productsData.language
    }
}

const mySpecialReduxContainer = connect(mapStateToProps);
export const HomeHeaderLeftIcon = mySpecialReduxContainer(HomeHeaderLeft);
export const HomeHeaderBackIcon = mySpecialReduxContainer(HomeHeaderBack);
export const MainHeaderTitle = mySpecialReduxContainer(HeaderTitle);

export default connect(mapStateToProps)(CustomHeaderRight);