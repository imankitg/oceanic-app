import { REMOVE_PROFILE, FETCHING_PROFILE_SUCCESS, FETCHING_PROFILE_FAILURE, IS_LOADING } from "../actions/Profile";

const initialState = {
    isLoading: false,
    getProfile: false,
    profile: {
        id: '',
        email: '',
        username: '',
        first_name: '',
        last_name: '',
        avatar_url: '',
        billing: {},
        shipping: {}
    }
};

const userProfileData = (state = initialState, action) => {
    switch (action.type) {
        case IS_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case REMOVE_PROFILE:
            return {
                ...state,
                ...initialState,
                getProfile: true
            };
        case FETCHING_PROFILE_SUCCESS:
            return {
                ...state,
                profile: action.profile,
                isLoading: false, 
                getProfile: true
            };
        case FETCHING_PROFILE_FAILURE:
            return {
                ...state,
                error: true,
                isLoading: false
            };
        default:
            return state
    }
};

export default {
    userProfileData
}