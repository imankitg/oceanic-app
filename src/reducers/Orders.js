import {
    ORDER_FIRST_TIME_LOAD, ON_ORDER_REFRESH_COMPLETE, ON_UPDATE_ORDER_COUNT,
    ON_ORDER_LOAD_MORE_START, ON_ORDER_LOAD_MORE_COMPLETE, ORDER_UPDATE_PAGE_NO,
    SELECT_ORDER, SHIPPING_RATES, SHIPPING_METHOD, ON_REFRESHING_START
} from "../actions/Orders";

const initialState = {
    isRefreshing: false,
    isLoadingMore: false,
    loadMore: true,
    firstTimeLoaded: false,
    orders: [],
    pageNo: 1,
    limit: 10,
    shippingRates: 0,
    shippingMethod: '',
    count: []
};

const ordersData = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_FIRST_TIME_LOAD:
            return {
                ...state,
                orders: action.data,
                firstTimeLoaded: true
            };
        case ON_REFRESHING_START: 
            return{
                ...state,
                isRefreshing: true
            };
        case ON_ORDER_REFRESH_COMPLETE:
            return {
                ...state,
                isRefreshing: false,
                loadMore: true,
                orders: action.data
            };
        case ON_ORDER_LOAD_MORE_START:
            return {
                ...state,
                isLoadingMore: true,
                pageNo: state.pageNo + 1
            };
        case ON_ORDER_LOAD_MORE_COMPLETE:
            if (action.data && action.data.length == state.limit) {
                return {
                    ...state,
                    isLoadingMore: false,
                    orders: [...state.orders, ...action.data]
                };
            }
            return {
                ...state,
                orders: [...state.orders, ...action.data],
                loadMore: false,
                isLoadingMore: false,
                pageNo: state.pageNo - 1
            };
        case ORDER_UPDATE_PAGE_NO:
            return {
                ...state,
                pageNo: action.pageNo
            };
        case SHIPPING_RATES:
            return {
                ...state,
                shippingRates: action.rate
            };
        case ON_UPDATE_ORDER_COUNT:
            return {
                ...state,
                isRefreshing: false,
                count: action.count
            };
        case SHIPPING_METHOD:
            return {
                ...state,
                shippingMethod: action.method
            };
        case SELECT_ORDER:
            return action.payload;
        default:
            return state
    }
};

export default {
    ordersData
}