import {
    FIRST_TIME_LOAD, ON_REFRESH_COMPLETE, ON_TOP_PRODUCT_LOAD,
    ON_LOAD_MORE_START, ON_LOAD_MORE_COMPLETE, UPDATE_PAGE_NO, ON_PRODUCT_DETAIL,
    SORT_BY_CATEGORY, PRODUCT_COMPLETED, PRODUCT_REFRESH, RELOAD_STATE, CHANGE_PRIMARY_COLOR_THEME,
    CHANGE_SECONDARY_COLOR_THEME, UPDATE_PRIMARY_COLOR_SHEME, UPDATE_SECONDARY_COLOR_SHEME, CHANGE_LANGUAGE
} from "../actions/Products";

import { Animated, AsyncStorage } from "react-native";

const initialState = {
    isRefreshing: false,
    isLoadingMore: false,
    primaryColor: '#ff8400',
    secondaryColor: '#1a5086',
    loadMore: true,
    firstTimeLoaded: false,
    products: [],
    topProducts: [],
    productDetail: {},
    pageNo: 1,
    limit: 10,
    _animatedMenu: new Animated.Value(0),
    productcomplete: false
};

AsyncStorage.getItem('USER_LANGUAGE', async (err, result)=> {
    if(result == 'ar'){
        initialState['isRTL'] = true;
    }
    else  initialState['isRTL'] = false;
})

const productsData = (state = initialState, action) => {
    switch (action.type) {
        case ON_TOP_PRODUCT_LOAD:
            return {
                ...state,
                topProducts: action.data
            };
        case FIRST_TIME_LOAD:
            return {
                ...state,
                products: action.data,
                firstTimeLoaded: true,
                isLoadingMore: false
            };
        case ON_REFRESH_COMPLETE:
            return {
                ...state,
                isRefreshing: false,
                loadMore: true,
                products: action.data
            };
        case ON_LOAD_MORE_START:
            return {
                ...state,
                isLoadingMore: true,
                pageNo: state.pageNo + 1
            };
        case ON_PRODUCT_DETAIL:
            return {
                ...state,
                productDetail: action.productDetail
            };
        case ON_LOAD_MORE_COMPLETE:
            if (action.data && action.data.length == state.limit) {
                return {
                    ...state,
                    isLoadingMore: false,
                    products: [...state.products, ...action.data]
                };
            }
            return {
                ...state,
                loadMore: false,
                isLoadingMore: false,
                pageNo: state.pageNo - 1
            };
        case UPDATE_PAGE_NO:
            return {
                ...state,
                pageNo: action.pageNo
            };
        case SORT_BY_CATEGORY:
            return {
                ...state,
                products: action.data
            };
        case PRODUCT_COMPLETED:
            return {
                ...state,
                productcomplete: true
            };
        case PRODUCT_REFRESH:
            return {
                ...state,
                productcomplete: false
            };
        case CHANGE_PRIMARY_COLOR_THEME:
            return {
                ...state,
                primaryColor: action.color
            };
        case CHANGE_SECONDARY_COLOR_THEME:
            return {
                ...state,
                secondaryColor: action.color
            };
        case UPDATE_PRIMARY_COLOR_SHEME:
            return {
                ...state,
                primary_Color: action.color_obj
            };
        case UPDATE_SECONDARY_COLOR_SHEME:
            return {
                ...state,
                Secondary_Color: action.color_obj
            };
        case CHANGE_LANGUAGE: 
            return{
                ...state,
                language: false
            }
        case RELOAD_STATE:
            return {
                ...state,
                products: [],
                firstTimeLoaded: false
            };
        default:
            return state
    }
};

export default {
    productsData
}