import { ON_WISHLIST_RESET, ON_CART_UPDATE, ON_TOTAL_RESET, ON_TOTAL_UPDATE, WISHLIST_COUNT, SHIPPING_ADDRESS, WISHLIST_DATA, RESET_CART} from "../actions/Cart";

const initialState = {
    cart: [],
    total: 0,
    counter: 0,
    wishListCount: 0,
    shippingAddress: {},
    wishListData: []
};

const cartData = (state = initialState, action) => {
    switch (action.type) {
        case ON_WISHLIST_RESET:
            return {
                ...state,
                wishListData: [],
                wishListCount: 0
            };
            case RESET_CART:
            return {
                ...state,
                cart: []
            };
        case WISHLIST_COUNT:
            return {
                ...state,
                wishListCount: action.data
            };
        case WISHLIST_DATA: 
            return{
                ...state,
                wishListData: action.data
            };
        case SHIPPING_ADDRESS:
            return {
                ...state,
                shippingAddress: action.data
            };
        case ON_CART_UPDATE:
            return {
                ...state,
                cart: action.data
            };
        case ON_TOTAL_RESET:
            return {
                ...state,
                total: 0
            };
        case ON_TOTAL_UPDATE:
            return {
                ...state,
                total: action.data
            };
        default:
            return state
    }
};

export default {
    cartData
}