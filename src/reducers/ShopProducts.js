import {
    ON_FETCH_PRODUCTS_START,
    ON_GET_PRODUCT_SUCCESS,
    ON_FETCH_PRODUCT_FAIL,
    CHANGE_GRID,
    ON_REFRESH_PRODUCT_START,
    ON_REFRESH_PRODUCT_SUCCESS,
    ON_END_REACHED,
    FILTER_APPLIED,
    ON_FETCH_CATEGORY_START,
    ON_GET_CATEGORIES_SUCCESS,
    CHANGED_SLIDER_RANGE,
    CHANGED_INPUT_RANGE,
    ON_GET_FILTER_PRODUCT_SUCCESS,
    ON_END_FILTER_DATA,
    ON_FETCH_FILTER_PRODUCTS_START,
    GET_MORE_FILTER_PRODUCT_SUCCESS,
    UPDATE_CATEGORY,
    CHANGED_FILTER_DATA,
    NEWARIVALS
} from '../actions/types';

const initialState = {
    products: [],
    loadingData: false,
    loading: true,
    error: false,
    isRefreshing: false,
    payload: {
        per_page: 10,
        page: 1,
        min_price: 0,
        max_price: 5000
    },
    grid: false,
    categoryLoading: false,
    categories: [],
    isFilter: false,
    categorySelectedValue: '',
    selectedCategory: {},
    isCategorySelect: false,
    filterDataEnd: true,
    allProducts: [],
    firstTimeProductLoaded: true
};

const shopData = (state = initialState, action) => {
    switch (action.type) {
        case ON_FETCH_PRODUCTS_START:
            return {
                ...state,
                loadingData: true
            };
        case ON_FETCH_FILTER_PRODUCTS_START:
            return {
                ...state,
                loadingData: true,
                loading: true
            };
        case ON_GET_PRODUCT_SUCCESS:
            return {
                ...state,
                products: [...state.products, ...action.data],
                per_page: [...state.products, ...action.data].length,
                loadingData: false,
                loading: false,
                firstTimeProductLoaded: false
            };
        case ON_GET_FILTER_PRODUCT_SUCCESS:
            return {
                ...state,
                products: action.data,
                isFilter: true,
                per_page: [...state.products, ...action.data].length,
                loadingData: false,
                loading: false
            };
        case GET_MORE_FILTER_PRODUCT_SUCCESS:
            return {
                ...state,
                products: [...state.products, ...action.data],
                per_page: [...state.products, ...action.data].length,
                loadingData: false,
                loading: false
            };
        case ON_END_FILTER_DATA:
            return {
                ...state,
                loadingData: false,
                loading: false,
                filterDataEnd: false
            };
        case ON_FETCH_PRODUCT_FAIL:
            return {
                ...state,
                error: true,
                loadingData: false,
                loading: false
            };
        case CHANGE_GRID:
            return {
                ...state,
                grid: action.data
            };
        case ON_REFRESH_PRODUCT_START:
            return {
                ...state,
                isRefreshing: true
            };
        case ON_REFRESH_PRODUCT_SUCCESS:
            return {
                ...state,
                products: action.data,
                isRefreshing: false,
                loading: false,
                loadingData: false,
                payload: {
                    ...state.payload,
                    per_page: 10,
                    page: 1,
                    min_price: 0,
                    max_price: 5000
                },
                isFilter: false,
                categorySelectedValue: '',
                selectedCategory: {},
                isCategorySelect: false,
                filterDataEnd: true
            };
        case ON_END_REACHED:
            return {
                ...state,
                loadingData: false,
                loading: false
            };
        case FILTER_APPLIED:
            return {
                ...state,
                products: action.data,
                isFilter: true
            };
        case ON_FETCH_CATEGORY_START:
            return {
                ...state,
                categoryLoading: true
            };
        case ON_GET_CATEGORIES_SUCCESS:
            return {
                ...state,
                categories: action.data,
                categoryLoading: false
            };
        case CHANGED_SLIDER_RANGE:
            return {
                ...state,
                payload: {
                    ...state.payload,
                    min_price: action.values[0],
                    max_price: action.values[1]
                }
            };
        case CHANGED_INPUT_RANGE:
            return {
                ...state,
                payload: {
                    ...state.payload,
                    [action.data.prop]: Number([action.data.value])
                }
            };
        case UPDATE_CATEGORY:
            return {
                ...state,
                categorySelectedValue: action.selectedCategory.value,
                selectedCategory: action.selectedCategory,
                isCategorySelect: true
            };
        case CHANGED_FILTER_DATA:
            return {
                ...state,
                filterDataEnd: true
            };
        case NEWARIVALS:
            return{
                ...state,
                allProducts: action.data
            }
        default:
            return state
    }
};

export default { shopData };