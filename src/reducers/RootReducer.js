import AppNavigator from "../routes/Routes";
import { LOGIN, LOGOUT, SET_LANGUAGE_INTO_REDUX } from "../actions/App";
import ProductsReducer from "./Products";
import ProfileReducer from "./Profile";
import CartReducer from "./Cart";
import OrdersReducer from "./Orders";
import ShopReducer from './ShopProducts';

const auth = (state = { isLoggedIn: false, language: 'en' }, action) => {
    switch (action.type) {
        case LOGIN:
            return Object.assign(state, {isLoggedIn: true});
        case LOGOUT:
            return Object.assign(state, {isLoggedIn: false});
        case SET_LANGUAGE_INTO_REDUX:
            return Object.assign(state, {language: action.language});
        default:
            return state
    }
};

const nav = (state, action) => {
    const newState = AppNavigator.router.getStateForAction(action, state);
    return newState || state
};

export default {
    ...ProductsReducer,
    ...ProfileReducer,
    ...CartReducer,
    ...OrdersReducer,
    ...ShopReducer,
    auth,
    nav
}
