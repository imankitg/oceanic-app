import React, { Component } from 'react';
import {TextInput, Text, View, Animated, TouchableOpacity, FlatList, RefreshControl, AsyncStorage} from "react-native";
import ViewContainer from '../shared/viewContainer/ViewContainer';
import Spinner from '../../components/shared/Spinner';
import Product from "../../components/shared/product/Product";
import Separator from "../../components/shared/Separator";
import Footer from "../../components/shared/Footer";
import ProductByCategoryStyle from "./ProductByCategoryStyle";
import EmptyDataScreen from '../shared/emptyDataScreen/EmptyDataScreen';
// redux
import { connect } from 'react-redux';
import { fetchProducts, refreshProducts, loadMoreProducts, reloadState, onProductDetail, productRefresh, ON_REFRESH_COMPLETE, ON_LOAD_MORE_COMPLETE } from '../../actions/Products';
import i18n from '../../../i18n';

class ProductByCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            productsCopy: [],
            filterProducts: []
        }
    }

    async componentDidMount() {
        const { params } = this.props.navigation.state;
        if (!this.props.productsData.firstTimeLoaded) {
            this.props.fetchProducts({ id: params.id, language: this.props.language });
        }
        this.handleProducts(this.props);
    }

    componentWillUnmount() {
        this.props.reloadState();
    }

    componentWillReceiveProps(nextProps) {
        this.handleProducts(nextProps);
    }

    handleProducts({ productsData }) {
        this.setState({
            productsCopy: [...productsData.products],
            filterProducts: [...productsData.products]
        })
    }

    canLoadMoreContent() {
        return !this.props.productsData.isLoadingMore && this.props.productsData.loadMore;
    };

    hideMenu() {
        Animated.spring(this.props.productsData._animatedMenu, {
            toValue: -120
        }).start();
    }

    showMenu() {
        Animated.spring(this.props.productsData._animatedMenu, {
            toValue: 0
        }).start();
    }

    onRefresh = () => {
        const { params } = this.props.navigation.state;
        this.props.refreshProducts(1);
        this.props.productRefresh();
        this.props.fetchProducts({
            type: ON_REFRESH_COMPLETE,
            id: params.id,
            language: this.props.language
        });
    };

    onLoadMore = () => {
        const { params } = this.props.navigation.state;
        this.props.loadMoreProducts();
        this.props.fetchProducts({
            type: ON_LOAD_MORE_COMPLETE,
            id: params.id,
            language: this.props.language
        });
    };

    onSelectProduct(product) {
        this.props.onProductDetail(product);
        this.props.navigation.navigate("ShopNewDetail", { product });
    }

    productList({ item }) {
        return (
            <TouchableOpacity onPress={() => this.onSelectProduct(item)}>
                <Product product={item} />
            </TouchableOpacity>
        );
    }

    filterMethod = (text) => {
        let search = text.toLowerCase();
        this.setState({
            filterProducts: this.state.productsCopy.filter(obj => obj.name.toLowerCase().includes(search))
        });
    };

    renderFooter = () => {
        if (!this.props.productsData.isLoadingMore) return null;

        return (
            <Footer topBorder />
        );
    };

    render() {
        let { firstTimeLoaded, isRefreshing, _animatedMenu, productcomplete } = this.props.productsData;
        let { filterProducts } = this.state;

        return firstTimeLoaded ?
            <ViewContainer style={{ backgroundColor: '#f2f2f2' }}>
                <Animated.View style={[ProductByCategoryStyle.toolbarView, { transform: [{ translateY: _animatedMenu }] }]}>
                    <TextInput style={ProductByCategoryStyle.inputSearch} onChangeText={(val) => { this.filterMethod(val) }} underlineColorAndroid='rgba(0,0,0,0)' placeholder={i18n.t('ProductDetails_Screen.SEARCH')} />
                </Animated.View>

                <FlatList
                    data={filterProducts}
                    renderItem={this.productList.bind(this)}
                    keyExtractor={(item, index) => { return index.toString() }}
                    ListHeaderComponent={() => <View style={ProductByCategoryStyle.emptyView} />}
                    ListFooterComponent={this.renderFooter}
                    ItemSeparatorComponent={Separator}
                    refreshControl={<RefreshControl
                            refreshing={isRefreshing}
                            onRefresh={this.onRefresh}
                            tintColor="#ff8400" />
                        }
                    onEndReached={(productcomplete === false) && this.onLoadMore}
                    onEndReachedThreshold={0.1}
                    ListEmptyComponent={<View style={ProductByCategoryStyle.emptyDataComponent}><EmptyDataScreen message={i18n.t("Common.NO_PRODUCTS_FOUND")} icon='filter'/></View>}
                />
            </ViewContainer> : <Spinner color='#ff8400' />;
    }
}

function mapStateToProps(state) {
    return {
        productsData: state.productsData,
        secondaryColor: state.productsData.secondaryColor,
        language: state.auth.language
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchProducts: (event) => dispatch(fetchProducts(event)),
        refreshProducts: (pageNo) => dispatch(refreshProducts(pageNo)),
        onProductDetail: (productDetail) => dispatch(onProductDetail(productDetail)),
        loadMoreProducts: () => dispatch(loadMoreProducts()),
        productRefresh: () => dispatch(productRefresh()),
        reloadState: () => dispatch(reloadState())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductByCategory);
