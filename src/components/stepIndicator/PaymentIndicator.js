import React, { Component } from 'react';
import StepIndicator from 'react-native-step-indicator';
import { connect } from 'react-redux';
import {I18n} from '../../../i18n';

const checkoutStepIndicatorLabels = [I18n('CheckOutForm_Screen.SHIPPING'), I18n('CheckOutForm_Screen.PAYMENT'), I18n('CheckOutForm_Screen.CONFIRM')];

class Stepper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkoutStepIndicatorStyles: {
                stepIndicatorSize: 25,
                currentStepIndicatorSize: 30,
                separatorStrokeWidth: 2,
                currentStepStrokeWidth: 3,
                stepStrokeCurrentColor: props.primaryColor,
                stepStrokeWidth: 3,
                stepStrokeFinishedColor: props.primaryColor,
                stepStrokeUnFinishedColor: '#aaaaaa',
                separatorFinishedColor: props.primaryColor,
                separatorUnFinishedColor: '#aaaaaa',
                stepIndicatorFinishedColor: props.primaryColor,
                stepIndicatorUnFinishedColor: '#ffffff',
                stepIndicatorCurrentColor: '#ffffff',
                stepIndicatorLabelFontSize: 13,
                currentStepIndicatorLabelFontSize: 13,
                stepIndicatorLabelCurrentColor: props.primaryColor,
                stepIndicatorLabelFinishedColor: '#ffffff',
                stepIndicatorLabelUnFinishedColor: '#aaaaaa',
                labelColor: '#999999',
                labelSize: 13,
                currentStepLabelColor: props.primaryColor
            }
        }
    }

    render() {
        return (
            <StepIndicator
                stepCount={this.props.stepCount}
                customStyles={this.state.checkoutStepIndicatorStyles}
                currentPosition={this.props.currentPosition}
                labels={checkoutStepIndicatorLabels}
            />
        )
    }
}

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor
    }
}

export default connect(mapStateToProps)(Stepper);





