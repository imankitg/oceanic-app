import { StyleSheet, Dimensions, Platform } from 'react-native';
let { height, width } = Dimensions.get('screen');
import StyleVars from '../../Style/StyleVars';
import { Header } from 'react-navigation';

export default StyleSheet.create({

    backgroundStyle: {
        backgroundColor: '#f9f9f9',
        position: 'relative'
    },
    titleContainer: {
        flex: 1,
        flexDirection: 'row'
    },
    titleBox: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonsContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#f4f4f4',
        paddingVertical: 10,
        elevation: 4,
        shadowOffset: { width: 0, height: 1 },
        shadowRadius: 2,
        shadowOpacity: 0.2,
        shadowColor: 'black',
        paddingHorizontal: 15,
        marginTop: 10
    },
    formButton: {
        backgroundColor: '#1a5086',
        paddingHorizontal: 15,
        paddingVertical: 7,
        borderRadius: 5
    },
    formButtonText1: {
        color: '#2a2a2a',
        textAlign: 'center',
        fontSize: 17,
        borderRadius: 5
    },
    formButtonText: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 17,
        borderRadius: 5
    },
    inputSearch: {
        "borderColor": "#a3a3a3",
        "fontSize": 14,
        "color": "#333",
        "backgroundColor": "rgba(255,255,255,0.9)",
        "width": '100%',
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderRadius: 3,
        height: 40
    },
    errorBorder: {
        "borderColor": "#ff8400",
        borderWidth: 1.5
    },
    textArea: {
        height: 'auto',
        minHeight: 100,
        alignSelf: 'flex-start',
        textAlignVertical: "top"
    },
    inputContainer: {
        marginTop: 5
    },
    checkboxContainer: {
        marginLeft: 10,
        marginVertical: 20
    },
    menuTitle: {
        backgroundColor: StyleVars.Colors.primary,
        paddingLeft: 10,
        paddingVertical: 20,
        color: StyleVars.Colors.headingTextColor
    },
    paymentText: {
        alignItems: 'center',
        textAlign: 'center',
        fontSize: 17,
        marginTop: 20
    },
    paymentImageContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 25,
        paddingHorizontal: 15,
        flexWrap: 'wrap'
    },
    paymentImageContainerBox: {
        backgroundColor: '#fff',
        position: 'relative',
        padding: 10,
        shadowOpacity: 0.4,
        shadowColor: '#444',
        shadowOffset: { width: 0, height: 0.8 },
        width: (width / 2) - 20,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 5,
        borderRadius: 3,
        marginVertical: 5
    },
    paymentImage: {
        width: 60,
        resizeMode: 'contain'
    },
    paymentImage1: {
        width: 150
    },
    paymentImageContainerBox1: {
        width: (width - 30)
    },
    body: {
        flex: 1
    },
    formButtonDisable: {
        backgroundColor: '#e1e1e1'
    },
    ViewPosition: {
        position: 'absolute',
        backgroundColor: 'rgba(255, 255, 255, 0.60)',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkOutContainer: {
        paddingHorizontal: 50,
        paddingTop: 100
    },
    checkOutFieldContainer: {
        paddingHorizontal: 15
    },
    card: {
        marginHorizontal: 16,
        flexDirection: 'row',
        backgroundColor: 'green',
        elevation: 3,
        flex: 1
    },
    cardBody: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        paddingVertical: 5,
        paddingHorizontal: 10,
        flex: 1
    },
    cardText: {
        fontSize: 19,
        fontWeight: '300',
        color: '#2a2a2a'
    },
    cardButton: {
        color: '#1a5086',
        fontSize: 20,
        fontWeight: '500'
    },
    cardBodyText: {
        color: '#a3a3a3',
        width: '87%',
        textAlign: 'left'
    },
    cardBodyShippingText: {
        color: '#a3a3a3'
    },
    completeImage: {
        width: 125,
        height: 125
    },
    ConfirmText: {
        alignItems: 'center',
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'Lato-Regular'
    },
    CompleteContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        paddingHorizontal: 20,
        marginVertical: 10
    },
    shopMore: {
        alignItems: 'center',
        textAlign: 'center',
        fontSize: 20,
        paddingHorizontal: 40,
        marginTop: 25,
        color: 'black'
    },
    giveRatingContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20
    },
    giveRatingWrapper: {
    },
    ratingWrapper: {
    },
    giveRatingText: {
        color: '#2a2a2a',
        marginTop: 2
    },
    feedBackContainer: {
        marginHorizontal: 15,
        marginTop: 10
    },
    filterColor: {
        backgroundColor: '#f9f9f9'
    },
    descriptionView: {
        marginHorizontal: 20
    },
    descriptionHeading: {
        fontSize: 15,
        fontFamily: 'Lato-BoldItalic'
    },
    descriptionText: {
        fontFamily: 'Lato-Regular',
        marginTop: 15
    },
    pickerWrapper: {
        backgroundColor: 'transparent',
        marginTop: 15,
        marginBottom: 10
    },
    titleStyle: {
        fontSize: 12,
        color: 'rgba(0, 0, 0, .38)',
        marginBottom: Platform.OS == 'ios' ? 0 : -5,
        textAlign: "left"
    },
    pickerContainer: {
        marginLeft: -8
    },
    FinalCartView: {
        flex: 1,
        marginTop: 15,
        shadowOpacity: 0.3,
        shadowColor: '#444',
        shadowOffset: { width: 0, height: 0.8 }
    },
    loadingView: {
        position: 'absolute',
        top: -Header.HEIGHT,
        left: 0,
        height: height,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)'
    }
});