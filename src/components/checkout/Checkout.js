import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Alert, I18nManager } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import SpinKit from '../shared/SpinKit/SpinKit';
import CheckoutStyles from './CheckoutStyle';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// redux
import { connect } from 'react-redux';
import { shippingAddress } from '../../actions/Cart';
import { shippingRates, shippingMethod } from '../../actions/Orders';
import { updateProfile } from '../../actions/Profile';
import { login } from '../../../src/actions/App';
import Stepper from '../stepIndicator/PaymentIndicator';
import ShippingApi from '../../services/ShippingServices';
import countries from '../../assets/countries/countries.json';
import LoginService from '../auth/LoginService';
import { fetchProfile } from '../../../src/actions/Profile'
import CustomPicker from '../shared/picker/Picker';
import { TextField } from 'react-native-material-textfield';
import { MKCheckbox } from 'react-native-material-kit';
import i18n from '../../../i18n';

class Checkout extends Component {

    constructor(props) {
        super(props);
        this.onFocus = this.onFocus.bind(this);
        let { shipping, billing } = props.profile;
        this.state = {
            isLoading: false,
            disable: false,
            country: props.profile && props.profile.billing.country || 'US',
            state: props.profile && props.profile.billing.state || 'FL',
            shippingCountry: props.profile && props.profile.shipping.country || 'US',
            shippingState: props.profile && props.profile.shipping.state || 'FL',
            statesData: [],
            shippingStatesData: [],
            language: "",
            checked: false,
            signup: {
                email: '',
                username: '',
                password: ''
            },
            passwordchecked: false,
            newOrder: {
                first_name: billing.first_name,
                last_name: billing.last_name,
                email: props.profile.email,
                username: props.profile.username,
                billing: props.profile.billing,
                shipping: props.profile.shipping
            },
            billing_shipping_name: false,
            payment_method: {
                method_id: '',
                method_title: ''
            },
            arabiclanguage: {
                billing_First_name: "الفوترة الاسم الأول",
                billing_Last_name: "الفواتير اسم العائلة",
                billing_Email_address: "الفواتير عنوان البريد الإلكتروني",
                billing_Street_address: "فوترة عنوان الشارع",
                billing_Postcode: "شيفرة البريد لإرسال الفواتير",
                billing_Phone: "فوتير الهاتف",
                billing_City: "مدينة الفوترة",
                shipping_First_name: "الشحن الاسم الأول", 
                shipping_Last_name: "الشحن الاسم الأخير", 
                shipping_Street_address: "عنوان الشحن الشارع", 
                shipping_Postcode: "الشحن الرمز البريدي", 
                shipping_City: "مدينة الشحن",
                Password: "كلمه السر"
            },
            zoneLocations: [],
            zoneRates: []
        };
        this.paymentMethods = [
            { method_id: "", method_title: " Please select Payment Method" },
            { method_id: "bacs", method_title: "Direct Bank Transfer" },
            { method_id: "cheque", method_title: "Cheque Payment" },
            { method_id: "cod", method_title: "Cash On Delivery" },
            { method_id: "paypal", method_title: "PayPal" }
        ];
        this.countriesData = countries;
    }
    componentWillMount() {
        this.getStates()
    }

    componentDidMount() {
        let zoneLocations = [];
        let zoneRates = [];
        const newOrder = { ...this.state.newOrder };
        ShippingApi.retrieveShippingZone().then(res => {
            let shipping = res.data.filter(shipping => shipping.name !== 'Locations not covered by your other zone');
            const promises = [];
            for (let key in shipping) {
                var count = 0;
                promises.push(ShippingApi.retrieveShippingZoneLocation(shipping[key].id));
            }
            Promise.all(promises).then(res => {
                res.forEach(({ data }) => {
                    data.forEach(zone => {
                        zone.id = shipping[count].id;
                        zoneLocations.push(zone);
                        this.setState({ zoneLocations })
                    });
                    count = count + 1
                })
            }).catch(err => {
                console.log(err)
            })
        })
    }

    setStateData = (key, selectedCountry) => {
        this.setState({ [key]: selectedCountry && selectedCountry.states || [] }, () => {
            if (!this.state[key]) {
                this.setState({ state: '' })
            }
        });
    };

    getStates(type) {
        let { country, shippingCountry } = this.state;
        let selectedCountry;

        if (type === 'billing') {
            selectedCountry = this.countriesData.find((obj) => obj.value === country);
            this.setStateData('statesData', selectedCountry);
        }
        else {
            selectedCountry = this.countriesData.find((obj) => obj.value === shippingCountry);
            this.setStateData('shippingStatesData', selectedCountry);
        }
    }

    handleCountryChange(country, type) {
        const newOrder = { ...this.state.newOrder };
        if (type === 'billing') {
            newOrder.billing.country = country;
            this.setState({ country, newOrder }, () => this.getStates('billing'))
        }
        else {
            newOrder.shipping.country = country;
            this.setState({ shippingCountry: country, newOrder }, () => this.getStates('shipping'))
        }
    }

    handleStateChange = (state, type) => {
        const newOrder = { ...this.state.newOrder };
        if (type === 'billing') {
            newOrder.billing.state = state;
            this.setState({ state, newOrder })
        }
        else if (type === 'shipping') {
            newOrder.shipping.state = state;
            this.setState({ shippingState: state, newOrder })
        }
    };

    setPassword(name, val) {
        const signup = { ...this.state.signup };
        signup[name] = val;
        this.setState({ signup })
    }

    onCustomerChange(name, val) {
        const newOrder = { ...this.state.newOrder };
        const signup = { ...this.state.signup };
        if (name === 'username') {
            signup[name] = val;
            this.setState({ signup })
        }
        newOrder[name] = val;
        this.setState({
            newOrder
        });
    }

    onCustomerBillingChange(name, val) {
        let newOrder = { ...this.state.newOrder };
        const signup = { ...this.state.signup };
        if (name === 'email') {
            signup[name] = val;
            this.setState({ signup })
        }
        newOrder.billing[name] = val;
        this.setState({
            newOrder
        });
    }

    onCustomerShippingChange(name, val) {
        const newOrder = { ...this.state.newOrder };
        newOrder.shipping[name] = val;
        this.setState({
            newOrder
        })
    }

    onCustomerShippingState(name, val) {
        const newOrder = { ...this.state.newOrder };
        newOrder.shipping.state = val;
        this.setState({
            newOrder,
            [name]: val
        })
    }

    calculateShippingRates = (itemValue) => {
        let countryNotFound = true;
        let locations = this.state.zoneLocations;
        this.setState({ isLoading: true });
        for (var key in locations) {
            if (locations[key].code === itemValue) {
                countryNotFound = false;
                ShippingApi.retrieveShippingZoneRates(locations[key].id).then(res => {
                    let shippingRates = res.data;
                    this.setState({ isLoading: false });
                    this.props.shippingRate(shippingRates[0].settings.cost.value);
                    this.props.shippingMethod(shippingRates[0].method_title);
                    this.props.navigation.navigate('selectCard')
                })
            }
        }
        if (countryNotFound) {
            ShippingApi.retrieveShippingZoneRates(0).then(res => {
                let shippingrate = res.data;
                if (shippingrate.length > 0 && shippingrate[0].enabled) {
                    this.props.shippingRate(shippingrate[0].settings.cost.value);
                    this.props.shippingMethod(shippingrate[0].method_title);
                    this.props.navigation.navigate('selectCard');
                    this.setState({ isLoading: false })
                }
                else {
                    this.setState({ isLoading: false });
                    Alert.alert('Message', 'No shipment is available!!')
                }
            })
        }
    };

    renderError(name) {
        if (I18nManager.isRTL) {
            return `${[name]} هو حقل مطلوب`
        }
        else {
            let firstLetterCap = name.charAt(0).toUpperCase() + name.slice(1);
            firstLetterCap = firstLetterCap.replace(/_/g, ' ');
            return `${[firstLetterCap]} is a required field`;
        }
    };

    placeOrder() {
        let errors = {};
        ['billing_First_name', 'billing_Last_name', 'billing_Email_address', 'billing_Street_address', 'billing_Postcode', 'billing_Phone', 'billing_City']
            .forEach((name) => {
                let value = this[name].value();
                if (!value) {
                    errors[name] = this.renderError(I18nManager.isRTL ? this.state.arabiclanguage[name] : name)
                }
            });
        if (this.state.checked) {
            ['shipping_First_name', 'shipping_Last_name', 'shipping_Street_address', 'shipping_Postcode', 'shipping_City']
                .forEach((name) => {
                    let value = this[name].value();
                    if (!value) {
                        errors[name] = this.renderError(I18nManager.isRTL ? this.state.arabiclanguage[name] : name);
                    }
                })
        }
        if (this.state.passwordchecked) {
            ['Password']
                .forEach((name) => {
                    let value = this[name].value();
                    if (!value) {
                        errors[name] = this.renderError(I18nManager.isRTL ? this.state.arabiclanguage[name] : name)
                    }
                })
        }
        this.setState({ errors }, () => {
            if (!Object.keys(errors).length) {
                let { cart } = this.props.cartData;
                let { signup, passwordchecked } = this.state;
                var order = {
                    payment_method: this.state.payment_method.method_id,
                    payment_method_title: this.state.payment_method.method_title,
                    set_paid: true,
                    billing: this.state.newOrder.billing,
                    shipping: this.state.newOrder.shipping,
                    line_items: [],
                    "shipping_lines": [
                        {
                            "method_title": "Flat Rate",
                            "method_id": "flat_rate",
                            "total": "150"
                        }
                    ]
                };
                if (this.props.profile.id) order.customer_id = this.props.profile.id;

                if (this.state.checked === false) {
                    const newOrder = { ...this.state.newOrder };
                    order.shipping = newOrder.billing;
                }

                if (this.state.payment_method.method_id == 'paypal') {
                    //TODO   
                }

                else {
                    cart.forEach(cart => {
                        order.line_items.push({
                            product_id: cart.id,
                            quantity: cart.quantity,
                            variation_id: cart.variation_id ? cart.variation_id : 0
                        })
                    })
                }

                if (passwordchecked && signup.email && signup.username && signup.password) {
                    this.setState({
                        isLoading: true
                    });
                    LoginService.signUp(signup).then((response) => {
                        this.setState({
                            isLoading: false
                        });
                        if (response.data && response.data.errors) {
                            return Alert.alert(
                                'Signup Error',
                                response.data.errors[0].message
                            )
                        }
                        else {
                            Alert.alert(
                                'Message',
                                'Account Created Successfully'
                            );
                            order.customer_id = response.data.id;
                            this.props.updateUserProfile(response.data.id);
                            this.props.login();
                            this.props.shipping(order);
                            this.props.profile.id ? this.props.updateProfile(this.props.profile.id, order) : this.props.updateProfile(response.data.id, order);
                            this.calculateShippingRates(this.state.country)
                        }
                    }).catch((error, data) => {
                        this.setState({
                            isLoading: false
                        });
                        Alert.alert(
                            'Signup Error',
                            'Email or user name already exist'
                        )
                    });
                }

                else {
                    this.props.shipping(order);
                    this.props.profile.id ? this.props.updateProfile(this.props.profile.id, order) : null;
                    this.calculateShippingRates(this.state.country);
                }
            }
        });
    }

    onFocus() {
        let { errors = {} } = this.state;
        for (let name in errors) {
            let ref = this[name];
            if (ref && ref.isFocused()) {
                delete errors[name];
            }
        }
        this.setState({ errors });
    }

    render() {
        let { disable, newOrder, country, statesData, state, errors = {}, shippingCountry, shippingState, shippingStatesData } = this.state;
        let { first_name, last_name, email, address_1, postcode, phone, city } = newOrder.billing;
        let { profile, navigation, secondaryColor } = this.props;
        return (
            <ViewContainer style={CheckoutStyles.backgroundStyle}>
                <View style={CheckoutStyles.inputContainer}>
                    <Stepper stepCount={3}
                        currentPosition={0} />
                </View>
                <KeyboardAwareScrollView>
                    {
                        profile && profile.id ? null :
                            <View style={CheckoutStyles.checkOutFieldContainer}>
                                <TouchableOpacity onPress={() => navigation.navigate('Login')} style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ marginTop: 10, fontFamily: 'Lato-Bold' }}>{i18n.t('CheckOutForm_Screen.CLICK_HERE_TO_LOGIN')}</Text>
                                </TouchableOpacity>
                            </View>
                    }
                    <View style={CheckoutStyles.checkOutFieldContainer}>
                        <View style={CheckoutStyles.inputContainer}>
                            <TextField onSubmitEditing={() => this.billing_Last_name.focus()}
                                ref={(ref) => { this.billing_First_name = ref }}
                                value={first_name}
                                onFocus={this.onFocus}
                                onChangeText={this.onCustomerBillingChange.bind(this, 'first_name')}
                                tintColor={'#fe8400'}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                label={i18n.t('CheckOutForm_Screen.FIRST_NAME')}
                                error={errors.billing_First_name} />
                        </View>
                        <View>
                            <TextField onSubmitEditing={() => this.billing_Email_address.focus()}
                                value={last_name}
                                ref={(ref) => { this.billing_Last_name = ref }}
                                onChangeText={this.onCustomerBillingChange.bind(this, 'last_name')}
                                tintColor={'#fe8400'}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                label={i18n.t('CheckOutForm_Screen.LAST_NAME')}
                                onFocus={this.onFocus}
                                error={errors.billing_Last_name} />
                        </View>
                        <View>
                            <TextField onSubmitEditing={() => this.username.focus()}
                                value={email}
                                ref={(ref) => { this.billing_Email_address = ref }}
                                keyboardType={'email-address'}
                                tintColor={'#fe8400'}
                                autoCapitalize='none'
                                onChangeText={this.onCustomerBillingChange.bind(this, 'email')}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                label={i18n.t('CheckOutForm_Screen.EMAIL_ADRESS')}
                                onFocus={this.onFocus}
                                error={errors.billing_Email_address} />
                        </View>
                        <View>
                            <TextField onSubmitEditing={() => this.billing_City.focus()}
                                value={newOrder.username}
                                tintColor={'#fe8400'}
                                autoCapitalize='none'
                                ref={(ref) => { this.username = ref }}
                                onChangeText={this.onCustomerChange.bind(this, 'username')}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                label={i18n.t('CheckOutForm_Screen.USER_NAME')} />
                        </View>
                        <View style={CheckoutStyles.pickerWrapper}>
                            <Text style={CheckoutStyles.titleStyle}>{i18n.t('CheckOutForm_Screen.COUNTRY')}</Text>
                            <CustomPicker
                                itemsArray={this.countriesData}
                                selectedValue={country}
                                onValueChange={country => this.handleCountryChange(country, 'billing')}
                                pickerRef={'countries'}
                                pickerContainer={CheckoutStyles.pickerContainer} />
                        </View>
                        {
                            statesData && statesData.length ?
                                <View style={CheckoutStyles.pickerWrapper}>
                                    <Text style={CheckoutStyles.titleStyle}>{i18n.t('CheckOutForm_Screen.STATE')}</Text>
                                    <CustomPicker
                                        itemsArray={statesData}
                                        selectedValue={state}
                                        onValueChange={state => this.handleStateChange(state, 'billing')}
                                        pickerRef={'states'}
                                        pickerContainer={CheckoutStyles.pickerContainer} />
                                </View>
                                :
                                <TextField
                                    label={i18n.t('CheckOutForm_Screen.STATE')}
                                    onSubmitEditing={() => this.billing_City.focus()}
                                    value={newOrder.billing.state}
                                    onChangeText={this.onCustomerBillingChange.bind(this, 'state')}
                                    tintColor={'#fe8400'}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    onFocus={() => this.setState({ state: '' })} />
                        }
                        <View>
                            <TextField onSubmitEditing={() => this.billing_Phone.focus()}
                                value={city}
                                ref={(ref) => { this.billing_City = ref }}
                                onChangeText={this.onCustomerBillingChange.bind(this, 'city')}
                                tintColor={'#fe8400'}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                label={i18n.t('CheckOutForm_Screen.CITY')}
                                onFocus={this.onFocus}
                                error={errors.billing_City} />
                        </View>

                        <View>
                            <TextField onSubmitEditing={() => this.billing_Street_address.focus()}
                                value={phone}
                                ref={(ref) => { this.billing_Phone = ref }}
                                onChangeText={this.onCustomerBillingChange.bind(this, 'phone')}
                                tintColor={'#fe8400'}
                                keyboardType='numeric'
                                underlineColorAndroid='rgba(0,0,0,0)'
                                label={i18n.t('CheckOutForm_Screen.PHONE_NUMBER')}
                                onFocus={this.onFocus}
                                error={errors.billing_Phone} />
                        </View>

                        <View>
                            <TextField onSubmitEditing={() => this.address_2.focus()}
                                value={address_1}
                                ref={(ref) => { this.billing_Street_address = ref }}
                                onChangeText={this.onCustomerBillingChange.bind(this, 'address_1')}
                                tintColor={'#fe8400'}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                label={i18n.t('CheckOutForm_Screen.STREET_ADDRESS')}
                                onFocus={this.onFocus}
                                error={errors.billing_Street_address} />
                        </View>
                        <View>
                            <TextField onSubmitEditing={() => this.billing_Postcode.focus()}
                                value={newOrder.billing.address_2}
                                ref={(ref) => { this.address_2 = ref }}
                                onChangeText={this.onCustomerBillingChange.bind(this, 'address_2')}
                                tintColor={'#fe8400'}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                label={i18n.t('CheckOutForm_Screen.STREET_ADDRESS_2')} />
                        </View>
                        <View>
                            <TextField
                                value={postcode}
                                ref={(ref) => { this.billing_Postcode = ref }}
                                onChangeText={this.onCustomerBillingChange.bind(this, 'postcode')}
                                tintColor={'#fe8400'}
                                keyboardType='numeric'
                                underlineColorAndroid='rgba(0,0,0,0)'
                                label={i18n.t('CheckOutForm_Screen.POST_CODE')}
                                onFocus={this.onFocus}
                                error={errors.billing_Postcode} />
                        </View>
                        {
                            profile.id ? null :
                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                                    <MKCheckbox checked={this.state.passwordchecked} onCheckedChange={() => this.setState({ passwordchecked: !this.state.passwordchecked })}
                                        fillColor="#fe8400" borderOffColor="gray" borderOnColor="#fe8400" />
                                    <Text>{i18n.t('CheckOutForm_Screen.CREATE_AN_ACCOUTN')}</Text>
                                </View>
                        }
                        {
                            this.state.passwordchecked ?
                                <View>
                                    <TextField
                                        secureTextEntry={true}
                                        ref={(ref) => this.Password = ref}
                                        onChangeText={this.setPassword.bind(this, 'password')}
                                        tintColor={'#fe8400'}
                                        onFocus={this.onFocus}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        label={i18n.t('CheckOutForm_Screen.PASSWORD')}
                                        error={errors.Password} />
                                </View> : null
                        }
                    </View>
                    <View style={{ flexDirection: 'row', flex: 1, marginHorizontal: 15, justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text>{i18n.t('CheckOutForm_Screen.SHIP_TO_A_DIFFERENT_ADDRESS')}</Text>
                        <MKCheckbox checked={this.state.checked} onCheckedChange={() => this.setState({ checked: !this.state.checked })}
                            fillColor="#fe8400" borderOffColor="gray" borderOnColor="#fe8400" />
                    </View>
                    {
                        this.state.checked ?
                            <View style={CheckoutStyles.checkOutFieldContainer}>
                                <View style={CheckoutStyles.inputContainer}>
                                    <TextField onSubmitEditing={() => this.shipping_Last_name.focus()}
                                        value={newOrder.shipping.first_name}
                                        ref={(ref) => { this.shipping_First_name = ref }}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'first_name')}
                                        tintColor={'#fe8400'}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        onFocus={this.onFocus}
                                        error={errors.shipping_First_name}
                                        label={i18n.t('CheckOutForm_Screen.FIRST_NAME')} />
                                </View>
                                <View>
                                    <TextField onSubmitEditing={() => this.shipping_City.focus()}
                                        value={newOrder.shipping.last_name}
                                        ref={(ref) => { this.shipping_Last_name = ref }}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'last_name')}
                                        tintColor={'#fe8400'}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        onFocus={this.onFocus}
                                        error={errors.shipping_Last_name}
                                        label={i18n.t('CheckOutForm_Screen.LAST_NAME')} />
                                </View>
                                <View style={CheckoutStyles.pickerWrapper}>
                                    <Text style={CheckoutStyles.titleStyle}>{i18n.t('CheckOutForm_Screen.COUNTRY')}</Text>
                                    <CustomPicker
                                        itemsArray={this.countriesData}
                                        selectedValue={shippingCountry}
                                        onValueChange={country => this.handleCountryChange(country, 'shipping')}
                                        pickerRef={'countries'}
                                        pickerContainer={CheckoutStyles.pickerContainer} />
                                </View>
                                {
                                    shippingStatesData && shippingStatesData.length ?
                                        <View style={CheckoutStyles.pickerWrapper}>
                                            <Text style={CheckoutStyles.titleStyle}>{i18n.t('CheckOutForm_Screen.STATE')}</Text>
                                            <CustomPicker
                                                itemsArray={shippingStatesData}
                                                selectedValue={shippingState}
                                                onValueChange={state => this.handleStateChange(state, 'shipping')}
                                                pickerRef={'states'}
                                                pickerContainer={CheckoutStyles.pickerContainer} />
                                        </View>
                                        :
                                        <TextField
                                            label={i18n.t('CheckOutForm_Screen.STATE')}
                                            onSubmitEditing={() => this.shipping_City.focus()}
                                            value={shippingState}
                                            onChangeText={this.onCustomerShippingState.bind(this, 'shippingState')}
                                            tintColor={'#fe8400'}
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            onFocus={() => this.setState({ state: '' })} />
                                }
                                <View>
                                    <TextField onSubmitEditing={() => this.shipping_Street_address.focus()}
                                        value={newOrder.shipping.city}
                                        ref={(ref) => { this.shipping_City = ref }}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'city')}
                                        tintColor={'#fe8400'}
                                        error={errors.shipping_City}
                                        onFocus={this.onFocus}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        label={i18n.t('CheckOutForm_Screen.CITY')} />
                                </View>
                                <View>
                                    <TextField onSubmitEditing={() => this.shipping_Postcode.focus()}
                                        value={newOrder.shipping.address_1}
                                        ref={(ref) => { this.shipping_Street_address = ref }}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'address_1')}
                                        tintColor={'#fe8400'}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        onFocus={this.onFocus}
                                        error={errors.shipping_Street_address}
                                        label={i18n.t('CheckOutForm_Screen.SHIPPING_ADDRESS')} />
                                </View>
                                <View>
                                    <TextField
                                        value={newOrder.shipping.postcode}
                                        ref={(ref) => { this.shipping_Postcode = ref }}
                                        keyboardType='numeric'
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'postcode')}
                                        tintColor={'#fe8400'}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        error={errors.shipping_Postcode}
                                        onFocus={this.onFocus}
                                        label={i18n.t('CheckOutForm_Screen.POST_CODE')} />
                                </View>
                            </View> : null
                    }
                </KeyboardAwareScrollView>
                <View style={CheckoutStyles.buttonsContainer}>
                    <TouchableOpacity onPress={() => navigation.navigate('Cart')} activeOpacity={0.5}>
                        <Text style={CheckoutStyles.formButtonText1}>{i18n.t('CheckOutForm_Screen.CANCEL')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[CheckoutStyles.formButton, { backgroundColor: secondaryColor }, disable && CheckoutStyles.formButtonDisable]}
                        onPress={this.placeOrder.bind(this)} disabled={disable}>
                        <Text style={CheckoutStyles.formButtonText}>{i18n.t('CheckOutForm_Screen.PROCEED')}</Text>
                    </TouchableOpacity>
                </View>
                {
                    this.state.isLoading ?
                        <View style={CheckoutStyles.loadingView}>
                            <SpinKit />
                        </View> : null
                }
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        cartData: state.cartData,
        profile: state.userProfileData.profile,
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor
    }
}

function mapDispatchToProps(dispatch) {
    return {
        shipping: (data) => dispatch(shippingAddress(data)),
        shippingRate: (rates) => dispatch(shippingRates(rates)),
        shippingMethod: (method) => dispatch(shippingMethod(method)),
        updateProfile: (id, profile, done) => dispatch(updateProfile(id, profile, done)),
        login: () => dispatch(login()),
        updateUserProfile: (id) => dispatch(fetchProfile(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);