import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView, Platform } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import Stepper from '../stepIndicator/PaymentIndicator'
import { checkoutStepIndicatorStyles, checkoutStepIndicatorLabels } from '../stepIndicator/stepIndicator';
import CheckoutStyles from './CheckoutStyle';
import Api from '../../services/ProductService';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SpinKit from '../shared/SpinKit/SpinKit'

export default class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paymentMethod: [],
            selectedPaymentsMethodIndex: ''
        };
    }

    bankImageName = (name) => {
        switch (name) {
            case 'bacs':
                return require('../../assets/images/BankTransfer.png');
            case 'cheque':
                return require('../../assets/images/chequeMethod.png');
            case 'cod':
                return require('../../assets/images/cashOnDelivery.png');
            case 'braintree_paypal':
                return require('../../assets/images/paypal.png');
            case 'paypal':
                return require('../../assets/images/paypal.png');
            case 'braintree_credit_card':
                return require('../../assets/images/paypal.png');
            default:
                return 'not found!!!'
        }
    };

    componentDidMount() {
        Api.paymentMethod().then(res => {
            let data = res.data;
            let paymentMethod = [];
            for (var key in data) {
                if (data[key].enabled) {
                    paymentMethod.push(data[key])
                }
            }
            this.setState({ paymentMethod })
        })
    }

    changePaymentsMethod(index) {
        this.setState({ selectedPaymentsMethodIndex: index });
        this.props.navigation.navigate('selectCard', { selectCard: index, paymentMethod: this.state.paymentMethod })
    }

    render() {
        let { paymentMethod } = this.state;
        return (
            <ViewContainer style={CheckoutStyles.backgroundStyle}>
                <View style={CheckoutStyles.inputContainer}>
                    <Stepper stepCount={3}
                        customStyles={checkoutStepIndicatorStyles}
                        labels={checkoutStepIndicatorLabels}
                        currentPosition={1} />
                </View>
                <View style={CheckoutStyles.body}>
                    <ScrollView>
                        <View>
                            <Text style={CheckoutStyles.paymentText}>Please Select Payment Method</Text>
                        </View>
                        <View style={CheckoutStyles.paymentImageContainer}>
                            {
                                paymentMethod.length ?
                                    paymentMethod.map((obj, index) => {
                                        let imageUrl = this.bankImageName(obj.id);
                                        return (
                                            <TouchableOpacity
                                                key={index}
                                                style={[CheckoutStyles.paymentImageContainerBox, index === paymentMethod.length -1  && paymentMethod.length % 2 === 1 ? CheckoutStyles.paymentImageContainerBox1 : null]}
                                                onPress={this.changePaymentsMethod.bind(this, index)}>
                                                <Image
                                                    style={[CheckoutStyles.paymentImage,  index === paymentMethod.length -1 && paymentMethod.length % 2 === 1 ? CheckoutStyles.paymentImage1 : null]}
                                                    source={imageUrl}
                                                    resizeMethod={'resize'}/>
                                                {index === this.state.selectedPaymentsMethodIndex ?
                                                    <View style={CheckoutStyles.ViewPosition}>
                                                        <MaterialIcons name="check" size={34} color="#000" />
                                                    </View> :
                                                    null
                                                }
                                            </TouchableOpacity>
                                        )
                                    }) : <SpinKit />
                            }

                        </View>

                    </ScrollView>
                </View>
            </ViewContainer>
        );
    }
}
