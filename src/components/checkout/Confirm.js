import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Alert, AsyncStorage, ScrollView } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import Stepper from '../stepIndicator/PaymentIndicator'
import { checkoutStepIndicatorStyles, checkoutStepIndicatorLabels } from '../stepIndicator/stepIndicator';
import CheckoutStyles from './CheckoutStyle';
import Api from '../../services/ProductService';
import { fetchOrderNumber } from '../../../src/actions/Orders';
import SpinKit from '../shared/SpinKit/SpinKit';
import PayPal from 'react-native-paypal-wrapper';
import Constants from '../../../WooCommerce/Config';
import OrderService from '../../services/OrderService';
import i18n from '../../../i18n';

import { connect } from 'react-redux';
import { resetTotal, resetWishList, resetCartList } from '../../actions/Cart';

class Confirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            PaymentMethod: props.navigation.state.params && props.navigation.state.params.paymentMethod
        }
    }

    componentDidMount() {
        let orderobj = this.props.state.cartData;
        orderobj.shippingAddress.shipping_lines[0].total = `${this.props.shippingRate}`;
        orderobj.shippingAddress.payment_method = this.state.PaymentMethod.id;
        orderobj.shippingAddress.payment_method_title = this.state.PaymentMethod.method_title;
        this.setState({ orderobj })
    }

    payment = (order, response) => {
        Api.paymentFunction(order.payment_method).then(res => {
            this.setState({ isLoading: false });
            this.props.resetWishList();
            this.props.resetCartList();
            this.props.resetTotal();
            AsyncStorage.removeItem('user');
            AsyncStorage.removeItem('cart_data');
            AsyncStorage.removeItem('TOTAL');
            this.props.fetchOrders(this.props.profile_id);
            this.props.navigation.navigate('Complete', { orderNumber: response.data.number })
        })
    };

    orderPlaced = () => {
        let order = this.state.orderobj.shippingAddress;
        this.setState({ isLoading: true });
        Api.placeOrder(order)
            .then((response, err) => {
                if (order.payment_method == 'paypal') {
                    OrderService.updateOrders(response.data.id, 'pending');
                    PayPal.initialize(Constants.Keys.PaypalMethod, Constants.Keys.PaypalClientId);
                    PayPal.pay({
                        price: `${response.data.total}`,
                        currency: `${response.data.currency}`,
                        description: `TradeBakerZ order invoice`
                    }).then(confirm => {
                        OrderService.updateOrders(response.data.id, 'processing', confirm.response.id);
                        this.payment(order, response);
                    })
                        .catch(error => {
                            OrderService.updateOrders(response.data.id, 'cancelled');
                            this.setState({ isLoading: false });
                        });
                }
                else {
                    this.payment(order, response);
                }

            })
            .catch((error) => {
                this.setState({ isLoading: false });
                Alert.alert(
                    'Bad Request',
                    'SomeThing Bad Happened!! Please Try Again Later'
                )
            });
    };

    render() {
        const { cartList, navigation, secondaryColor } = this.props;
        let { title } = this.state.PaymentMethod;
        return (
            <ViewContainer style={CheckoutStyles.backgroundStyle}>
                <View style={CheckoutStyles.inputContainer}>
                    <Stepper stepCount={3}
                        customStyles={checkoutStepIndicatorStyles}
                        labels={checkoutStepIndicatorLabels}
                        currentPosition={2} />
                </View>
                <ScrollView>
                    <View style={CheckoutStyles.FinalCartView}>
                        {
                            cartList.length ?
                                <View>
                                    <View style={CheckoutStyles.card}>
                                        <View style={CheckoutStyles.cardBody}>
                                            <Text style={CheckoutStyles.cardText}>{i18n.t('Confirm_Screen.CART')}</Text>
                                            <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
                                                <Text style={[CheckoutStyles.cardButton, { color: secondaryColor }]}>{i18n.t('Confirm_Screen.EDIT')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    {
                                        cartList.map((data, index) => {
                                            return (
                                                <View style={CheckoutStyles.card} key={index}>
                                                    <View style={CheckoutStyles.cardBody}>
                                                        <Text style={CheckoutStyles.cardBodyText}>{`${Api.convertingLanguage(data.name)}   x${data.quantity}`}</Text>
                                                        <Text style={CheckoutStyles.cardBodyText}>${data.price}</Text>
                                                    </View>
                                                </View>
                                            )
                                        })
                                    }
                                    <View style={CheckoutStyles.card}>
                                        <View style={CheckoutStyles.cardBody}>
                                            <Text style={CheckoutStyles.cardBodyShippingText}>{i18n.t('Confirm_Screen.SHIPPING')}</Text>
                                            <Text style={CheckoutStyles.cardBodyShippingText}>{`${this.props.shippingMethod} : $${this.props.shippingRate}`}</Text>
                                        </View>
                                    </View>
                                    <View style={CheckoutStyles.card}>
                                        <View style={CheckoutStyles.cardBody}>
                                            <Text style={CheckoutStyles.cardBodyText}>{i18n.t('Confirm_Screen.TOTAL')}</Text>
                                            <Text style={CheckoutStyles.cardBodyText}>${`${this.props.shippingRate + this.props.totalPrice}`}</Text>
                                        </View>
                                    </View>
                                </View>
                                :
                                <View>
                                    <Text style={CheckoutStyles.paymentText}>No Product in Cart List</Text>
                                </View>
                        }
                    </View>
                    <View style={CheckoutStyles.FinalCartView}>
                        <View style={CheckoutStyles.card}>
                            <View style={CheckoutStyles.cardBody}>
                                <Text style={CheckoutStyles.cardText}>{i18n.t('Confirm_Screen.SHIPPING_ADDRESS')}</Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Checkout')}>
                                    <Text style={[CheckoutStyles.cardButton, { color: secondaryColor }]}>{i18n.t('Confirm_Screen.EDIT')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={CheckoutStyles.card}>
                            <View style={CheckoutStyles.cardBody}>
                                <Text style={CheckoutStyles.cardBodyText}>{this.props.shippingAddress}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={CheckoutStyles.FinalCartView}>
                        <View style={CheckoutStyles.card}>
                            <View style={CheckoutStyles.cardBody}>
                                <Text style={CheckoutStyles.cardText}>{i18n.t('Confirm_Screen.PAYMENT_METHOD')}</Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('selectCard')}>
                                    <Text style={[CheckoutStyles.cardButton, { color: secondaryColor }]}>{i18n.t('Confirm_Screen.EDIT')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={CheckoutStyles.card}>
                            <View style={CheckoutStyles.cardBody}>
                                <Text style={CheckoutStyles.cardBodyText}>{title.toUpperCase()}</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={CheckoutStyles.buttonsContainer}>
                    <TouchableOpacity onPress={() => navigation.navigate('selectCard')} activeOpacity={0.5}>
                        <Text style={CheckoutStyles.formButtonText1}>{i18n.t('Confirm_Screen.CANCEL')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.orderPlaced}
                        style={[CheckoutStyles.formButton, { backgroundColor: secondaryColor }]} >
                        <Text style={CheckoutStyles.formButtonText}>{title == 'PayPal' ? i18n.t('Confirm_Screen.PROCEED_TO_PAYPAL') : i18n.t('Confirm_Screen.PLACE_ORDER')}</Text>
                    </TouchableOpacity>
                </View>
                {
                    this.state.isLoading ?
                        <View style={CheckoutStyles.loadingView}>
                            <SpinKit />
                        </View> : null
                }

            </ViewContainer>
        );
    };
}

function mapStateToProps(state) {
    return {
        state: state,
        shippingRate: Number(state.ordersData.shippingRates),
        shippingMethod: state.ordersData.shippingMethod,
        totalPrice: Number(state.cartData.total),
        cartList: state.cartData.cart,
        shippingAddress: state.cartData.shippingAddress.shipping.address_1,
        profile_id: state.userProfileData.profile.id,
        secondaryColor: state.productsData.secondaryColor
    }
}
function mapDisptachToProps(dispatch) {
    return {
        resetTotal: () => dispatch(resetTotal()),
        resetWishList: () => dispatch(resetWishList()),
        resetCartList: () => dispatch(resetCartList()),
        fetchOrders: (id) => dispatch(fetchOrderNumber(id))
    }
}

export default connect(mapStateToProps, mapDisptachToProps)(Confirm);

