import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import Stepper from '../stepIndicator/PaymentIndicator'
import { checkoutStepIndicatorStyles, checkoutStepIndicatorLabels } from '../stepIndicator/stepIndicator';
import CheckoutStyles from './CheckoutStyle';
import { BlockButton } from '../shared/buttons/AppButton';
import { connect } from 'react-redux';
import i18n from '../../../i18n';


class Complete extends Component {
    render() {
        let { primaryColor } = this.props;
        return (
            <ViewContainer style={CheckoutStyles.backgroundStyle}>
                <View style={CheckoutStyles.inputContainer}>
                    <Stepper stepCount={3}
                        customStyles={checkoutStepIndicatorStyles}
                        labels={checkoutStepIndicatorLabels}
                        currentPosition={3} />
                </View>
                <View style={[CheckoutStyles.CompleteContainer, {marginTop: 30}]}>
                    <Image
                        style={CheckoutStyles.completeImage}
                        source={require('../../assets/images/check.png')}
                    />
                </View>
                <View style={CheckoutStyles.CompleteContainer}>
                    <Text style={CheckoutStyles.ConfirmText}> {i18n.t('Completed_Screen.ORDER_PLACED_MESSAGE')} {this.props.navigation.state.params.orderNumber}</Text>
                </View>
                <View style={CheckoutStyles.CompleteContainer}>
                    <BlockButton onPress={() => {this.props.navigation.navigate('Home')}} bgColor={primaryColor}>
                        {i18n.t('Completed_Screen.RETURN_TO_SHOP')}
                    </BlockButton>
                </View>
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor
    }
}

export default connect(mapStateToProps)(Complete)