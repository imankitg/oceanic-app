import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import Stepper from '../stepIndicator/PaymentIndicator'
import { checkoutStepIndicatorStyles, checkoutStepIndicatorLabels } from '../stepIndicator/stepIndicator';
import CheckoutStyles from './CheckoutStyle';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Api from '../../services/ProductService';
import SpinKit from '../shared/SpinKit/SpinKit';
import HTMLView from 'react-native-htmlview';
import { connect } from 'react-redux';
import i18n from '../../../i18n';

class selectCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visa: {
                CardHolderName: '',
                CardNumber: '',
                CVV: ''
            },
            disable: false,
            selectMethod: false,
            selectedPaymentsMethodIndex: '',
            paymentMethod: []
        };
    }

    componentDidMount() {
        Api.paymentMethod().then(res => {
            let data = res.data;
            let paymentMethod = [];
            for (var key in data) {
                if (data[key].enabled) {
                    paymentMethod.push(data[key])
                }
            }
            this.setState({ paymentMethod })
        })
    }

    bankImageName = (name) => {
        switch (name) {
            case 'bacs':
                return require('../../assets/images/BankTransfer.png');
            case 'cheque':
                return require('../../assets/images/chequeMethod.png');
            case 'cod':
                return require('../../assets/images/cashOnDelivery.png');
            case 'braintree_paypal':
                return require('../../assets/images/paypal.png');
            case 'paypal':
                return require('../../assets/images/paypal.png');
            case 'braintree_credit_card':
                return require('../../assets/images/paypal.png');
            default:
                return 'not found!!!'
        }
    };

    onCustomerChange(name, val) {
        const visa = this.state.visa;
        visa[name] = val;
        this.setState({
            visa
        });
    }

    selectPaymentMethod = (index) => {
        this.setState({
            selectedPaymentsMethodIndex: index,
            selectMethod: true
        })
    }

    render() {
        const { disable, selectedPaymentsMethodIndex, paymentMethod, selectMethod } = this.state;
        let { secondaryColor } = this.props;
        return (
            <ViewContainer style={CheckoutStyles.backgroundStyle}>
                <View style={CheckoutStyles.inputContainer}>
                    <Stepper stepCount={3}
                        customStyles={checkoutStepIndicatorStyles}
                        labels={checkoutStepIndicatorLabels}
                        currentPosition={1} />
                </View>
                <View style={CheckoutStyles.body}>
                    <ScrollView>
                        <View>
                            <Text style={CheckoutStyles.paymentText}>{i18n.t('SelectCard_Screen.SELECT_PAYMENT_METHOD')}</Text>
                        </View>
                        <View style={CheckoutStyles.paymentImageContainer}>
                            {
                                paymentMethod.length ?
                                    paymentMethod.map((obj, index) => {
                                        let imageUrl = this.bankImageName(obj.id);
                                        return (
                                            <TouchableOpacity onPress={() => this.selectPaymentMethod(index)} key={index} style={[CheckoutStyles.paymentImageContainerBox, index === paymentMethod.length - 1 && paymentMethod.length % 2 === 1 ? CheckoutStyles.paymentImageContainerBox1 : null]}>
                                                <Image
                                                    style={[CheckoutStyles.paymentImage, index === paymentMethod.length - 1 && paymentMethod.length % 2 === 1 ? CheckoutStyles.paymentImage1 : null]}
                                                    source={imageUrl}
                                                    resizeMethod={'resize'} />
                                                {index === this.state.selectedPaymentsMethodIndex ?
                                                    <View style={CheckoutStyles.ViewPosition}>
                                                        <MaterialIcons name="check" size={34} color="#000" />
                                                    </View> :
                                                    null
                                                }
                                            </TouchableOpacity>
                                        )
                                    }) : <SpinKit />
                            }
                        </View>
                        {
                            paymentMethod.length && paymentMethod[selectedPaymentsMethodIndex] ? 
                            <View style={CheckoutStyles.descriptionView}>
                                <Text style={CheckoutStyles.descriptionHeading}>PAYMENT METHOD DESCRIPTION</Text>
                                <HTMLView value={paymentMethod[selectedPaymentsMethodIndex].description} />
                            </View>: null
                        }
                    </ScrollView>
                </View>
                <View style={CheckoutStyles.buttonsContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Checkout')} activeOpacity={0.5}>
                        <Text style={CheckoutStyles.formButtonText1}>{i18n.t('SelectCard_Screen.CANCEL')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        disabled={!selectMethod}
                        style={[CheckoutStyles.formButton, { backgroundColor: secondaryColor }, !selectMethod && CheckoutStyles.formButtonDisable]}
                        onPress={() => { this.props.navigation.navigate('Confirm', { paymentMethod: paymentMethod[selectedPaymentsMethodIndex] }) }}>
                        <Text style={CheckoutStyles.formButtonText}>{i18n.t('SelectCard_Screen.PROCEED')}</Text>
                    </TouchableOpacity>
                </View>
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        secondaryColor: state.productsData.secondaryColor
    }
}

export default connect(mapStateToProps)(selectCard)
