import { StyleSheet, Dimensions } from 'react-native';

export default DrawerStyle = StyleSheet.create({
    mainViewStyle: {
        alignItems: 'center', 
        justifyContent: 'center', 
        flex: 1,
        marginTop: 10
    },
    themeView: {
        flexDirection: 'row', 
        marginHorizontal: 10, 
        marginVertical: 5
    },
    ColorTypeText: {
        fontSize: 20, 
        fontFamily: 'Lato-Bold'
    },
    themeColorText: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginLeft: 20, 
        flex: 1, 
        fontFamily: 'Lato-Regular'
    },
    colorWheelWidth: {
        width: Dimensions.get('window').width / 2
    },
    thumbStyle: {
        height: 30, 
        width: 30, 
        borderRadius: 30
    }
});