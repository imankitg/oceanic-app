import React, { Component } from 'react';
import { View, Text } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import { changePrimaryColorTheme, changeSecondaryColor } from '../../actions/Products';
import { connect } from 'react-redux';
import ThemeCustomizationStyle from './ThemeCustomizationStyle';
import { ColorWheel } from 'react-native-color-wheel';
import colorsys from 'colorsys';
import i18n from '../../../i18n';

class ThemeCustomization extends Component {

    render() {
        let { primaryColor, secondaryColor } = this.props;
        return (
            <ViewContainer>
                <View style={ThemeCustomizationStyle.mainViewStyle}>
                    <Text style={ThemeCustomizationStyle.ColorTypeText}>{i18n.t('ThemeCustomization_Screen.PRIMARY_COLOR')}</Text>
                    <ColorWheel
                        initialColor={primaryColor}
                        onColorChange={color => {
                            this.props.changePrimaryColor(colorsys.hsv2Hex(color));
                        }}
                        style={ThemeCustomizationStyle.colorWheelWidth}
                        thumbStyle={ThemeCustomizationStyle.thumbStyle} />

                    <Text style={ThemeCustomizationStyle.ColorTypeText}>{i18n.t('ThemeCustomization_Screen.SECONDARY_COLOR')}</Text>

                    <ColorWheel
                        initialColor={secondaryColor}
                        onColorChange={color => {
                            this.props.changeSecondaryColor(colorsys.hsv2Hex(color));
                        }}
                        style={ThemeCustomizationStyle.colorWheelWidth}
                        thumbStyle={ThemeCustomizationStyle.thumbStyle} />
                </View>
            </ViewContainer>
        )
    }
}

function mapStateToProps(state) {
    return {
        secondaryColor: state.productsData.secondaryColor,
        primaryColor: state.productsData.primaryColor
    }
}

function mapDispatchToProps(dispatch) {
    return {
        changePrimaryColor: (primaryColor) => dispatch(changePrimaryColorTheme(primaryColor)),
        changeSecondaryColor: (secondaryColor) => dispatch(changeSecondaryColor(secondaryColor)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ThemeCustomization)