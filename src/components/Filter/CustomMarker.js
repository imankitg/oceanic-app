import React from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

class CustomMarker extends React.Component {
    render() {
        const { primaryColor } = this.props
        return (
            <View style={this.props.pressed ? [styles.button1, { backgroundColor: primaryColor }] : [styles.button, { backgroundColor: primaryColor }]} />
        );
    }
}

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor
    }
}

export default connect(mapStateToProps)(CustomMarker);

const styles = StyleSheet.create({
    button: {
        height: 23,
        width: 23,
        borderRadius: 20,
        backgroundColor: '#ff8400',
        borderColor: "white",
        borderWidth: 1
    },
    button1: {
        height: 30,
        width: 30,
        borderRadius: 20,
        backgroundColor: '#ff8400',
        borderColor: "white",
        borderWidth: 1
    }
});