'use strict';

import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

import MultiSlider from '@ptomasroos/react-native-multi-slider';
import CustomMarker from './CustomMarker';
import { connect } from 'react-redux';
import i18n from '../../../i18n';

class Example extends React.Component {
    state = {
        sliderOneChanging: false
    };

    render() {
        const { primaryColor, secondaryColor } = this.props
        return (
            <View style={styles.container}>
                <Text style={styles.text}>{i18n.t('PriceScale_Screen.SELECT_PRICE_RANGE')}</Text>
                <View style={{ marginHorizontal: 15 }}>
                    <MultiSlider
                        selectedStyle={{ backgroundColor: primaryColor }}
                        unselectedStyle={{ backgroundColor: secondaryColor }}
                        values={this.props.values}
                        onValuesChange={this.props.onValuesChange}
                        min={this.props.min}
                        max={this.props.max}
                        step={this.props.step}
                        containerStyle={{ height: 1 }}
                        trackStyle={{ height: 4, width: 200, backgroundColor: 'red' }}
                        touchDimensions={{
                            height: 40,
                            width: 10,
                            borderRadius: 20,
                            slipDisplacement: 40
                        }}
                        customMarker={CustomMarker}
                        sliderLength={320}
                    />
                </View>
            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor
    }
}

export default connect(mapStateToProps)(Example);

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    sliders: {
        margin: 20,
        width: 280
    },
    multiSliderSelectedColor: {
        backgroundColor: '#ff8400'
    },
    text: {
        alignSelf: 'center',
        fontSize: 13,
        color: 'black',
        marginBottom: 13,
        fontFamily: 'Lato-Regular'
    },
    title: {
        fontSize: 30
    },
    sliderOne: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    }
});