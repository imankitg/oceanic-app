import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, TouchableOpacity, Picker, Modal, PickerIOS, Dimensions, TouchableWithoutFeedback, I18nManager } from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';


////////////////////    Android Picker   ///////////////////////
const PickerAndroid = ({ itemsArray, onValueChange, selectedValue }) => {

    let items = itemsArray.map((item, i) => <Picker.Item label={item.label} color="gray" size={14} value={item.value} key={i} />);

    return (
        <Picker
            onValueChange={onValueChange}
            style={{ backgroundColor: "transparent", height: 40 }}
            selectedValue={selectedValue}>
            {items}
        </Picker>
    )
};
////////////////////    Android Picker   ///////////////////////


////////////////////    IOS Picker   ///////////////////////
const PickerItemIOS = PickerIOS.Item;

const SCREEN_WIDTH = Dimensions.get('window').width;

const PickerIOSStyles = StyleSheet.create({

    basicContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: '#00000090'
    },
    overlayContainer: {
        flex: 1,
        width: SCREEN_WIDTH
    },
    modalContainer: {
        width: SCREEN_WIDTH,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        backgroundColor: '#fff'
    },
    buttonView: {
        width: SCREEN_WIDTH,
        padding: 8,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    bottomPicker: {
        width: SCREEN_WIDTH
    },
    mainBox: {
    },
    pickerBtn: {
        position: 'relative',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#e1e1e1',
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 2 },
        shadowOpacity: 0.1
    },
    pickerBtnText: {
        fontSize: 14,
        //fontFamily: 'QuicksandBook-Regular',
        color: 'gray'
    },
    iconCaretDown: {
        marginLeft: 10,
        color: '#ff8400'
    }
});

class PickerForIOS extends Component {
    constructor(props) {
        super(props);

        const selected = props.options.indexOf(props.selectedValue) || 0;

        this.state = {
            modalVisible: false,
            selectedOption: props.options[selected],
            selectedLabel: props.labels[selected]
        };

        this.onPressCancel = this.onPressCancel.bind(this);
        this.onPressSubmit = this.onPressSubmit.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onOverlayDismiss = this.onOverlayDismiss.bind(this);

        if ('buttonColor' in props) {
            console.warn('buttonColor as a prop is deprecated, please use buttonStyle instead.');
        }
    }

    componentWillReceiveProps(props) {
        // If options are changing, and our current selected option is not part of
        // the new options, update it.
        if (
            props.options
            && props.options.length > 0
            && props.options.indexOf(this.state.selectedOption) === -1
        ) {
            const previousOption = this.state.selectedOption;
            this.setState({
                selectedOption: props.options[0],
                selectedLabel: props.labels[0]
            }, () => {
                // Options array changed and the previously selected option is not present anymore.
                // Should call onSubmit function to tell parent to handle the change too.
                if (previousOption) {
                    this.onPressSubmit();
                }
            });
        }
    }

    onPressCancel() {
        const selected = this.props.options.indexOf(this.props.selectedValue) || 0;
        this.setState({
            selectedOption: this.props.options[selected],
            selectedLabel: this.props.labels[selected]
        });
        this.hide();
    }

    onPressSubmit() {
        if (this.props.onSubmit) {
            this.props.onSubmit(this.state.selectedOption, this.state.selectedLabel);
        }
        this.hide();
    }

    onOverlayDismiss() {
        this.hide();
    }

    onValueChange(option, index) {
        this.setState({
            selectedOption: option,
            selectedLabel: this.props.labels[index]
        });
    }

    show() {
        this.setState({
            modalVisible: true
        });
    }

    hide() {
        this.setState({
            modalVisible: false
        });
    }

    renderItem(option, index) {
        const label = (this.props.labels) ? this.props.labels[index] : option;

        return (
            <PickerItemIOS
                key={option}
                value={option}
                label={label}
            />
        );
    }

    render() {
        const { modalVisible, selectedOption, selectedLabel } = this.state;
        const {
            options,
            buttonStyle,
            itemStyle,
            cancelText,
            confirmText,
            disableOverlay,
        } = this.props;

        return (
            <Modal
                animationType={'fade'}
                transparent
                visible={modalVisible}
            >
                <View style={PickerIOSStyles.basicContainer}>
                    {!disableOverlay &&
                        <View style={PickerIOSStyles.overlayContainer}>
                            <TouchableWithoutFeedback onPress={this.onOverlayDismiss}>
                                <View style={PickerIOSStyles.overlayContainer} />
                            </TouchableWithoutFeedback>
                        </View>
                    }
                    <View style={PickerIOSStyles.modalContainer}>
                        <View style={PickerIOSStyles.buttonView}>
                            <TouchableOpacity onPress={this.onPressCancel}>
                                <Text style={buttonStyle}>
                                    {cancelText || 'Cancel'}
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={this.onPressSubmit}>
                                <Text style={buttonStyle}>
                                    {confirmText || 'Confirm'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={PickerIOSStyles.mainBox}>
                            <PickerIOS
                                ref={'picker'}
                                style={PickerIOSStyles.bottomPicker}
                                selectedValue={selectedOption}
                                onValueChange={(option, index) => this.onValueChange(option, index)}
                                itemStyle={itemStyle}
                            >
                                {options.map((option, index) => this.renderItem(option, index))}
                            </PickerIOS>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}
////////////////////    IOS Picker   ///////////////////////


class CustomPicker extends Component {

    constructor(props) {
        super(props);
        this.state = {
            options: props.itemsArray.map((obj) => obj.value),
            labels: props.itemsArray.map((obj) => obj.label)
        };
        props.onValueChange(props.selectedValue == 'undefined' ? props.itemsArray[0].value : props.selectedValue);
    }

    showLabel(value) {
        let label = '';
        for (let i = 0; i < this.props.itemsArray.length; i++) {
            if (this.props.itemsArray[i].value == value) {
                label = this.props.itemsArray[i].label;
                break;
            }
        }
        return <Text style={[PickerIOSStyles.pickerBtnText, this.props.IOSBtnText]}>{label}</Text>
    }

    renderPickerIOS() {
        return (
            <View>
                <TouchableOpacity onPress={() => this.refs[this.props.pickerRef].show()} style={[PickerIOSStyles.pickerBtn, this.props.IOSBtnStyle]}>
                    {this.showLabel(this.props.selectedValue)}
                    <FontAwesomeIcon name={'angle-down'} size={50} style={[PickerIOSStyles.iconCaretDown, this.props.caretStyle, { color: this.props.primaryColor }]} />
                </TouchableOpacity>
                <PickerForIOS
                    ref={this.props.pickerRef}
                    options={this.state.options}
                    labels={this.state.labels}
                    selectedValue={this.props.selectedValue}
                    onSubmit={(val) => {
                        this.props.onValueChange(val);
                    }}
                />
            </View>
        )
    }

    renderPickerAndroid() {
        return (
            <View style={PickerAndroidStyles.container}>
                <FontAwesomeIcon name={'angle-down'} size={50} style={[PickerAndroidStyles.iconCaretDown, this.props.caretStyle, { color: this.props.primaryColor }]} />
                <PickerAndroid itemsArray={this.props.itemsArray} selectedValue={this.props.selectedValue}
                    onValueChange={(val) => this.props.onValueChange(val)} />
            </View>
        )
    }

    render() {
        return Platform.OS === 'ios' ? this.renderPickerIOS() : this.renderPickerAndroid();
    }
}

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor
    }
}

export default connect(mapStateToProps)(CustomPicker);

const PickerAndroidStyles = StyleSheet.create({
    container: {
        position: 'relative',
        elevation: 5,
        backgroundColor: '#ffffff',
        marginHorizontal: 1,
        marginTop: 2,
        paddingVertical: 5
    },
    iconCaretDown: {
        position: 'absolute',
        bottom: Platform.OS === 'android' ? 0 : 3,
        color: '#ff8400',
        right: I18nManager.isRTL ? 20: null,
        left: I18nManager.isRTL ? null : 20,

    },
    iconCaretUp: {
        position: 'absolute',
        top: Platform.OS === 'android' ? 13.5 : 3,
        right: 20
    }
});