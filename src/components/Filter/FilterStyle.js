import { StyleSheet, I18nManager, Platform } from 'react-native';

export default StyleSheet.create({

    backgroundColor: {
        backgroundColor: '#f9f9f9'
    },
    filterContainer: {
        marginTop: 10,
        marginBottom: 10,
        marginHorizontal: 15,
        borderColor: '#e9e9e9',
        paddingBottom: 20,
        borderBottomWidth: 1,
    },
    selectColorText: {
        fontSize: 15,
        fontWeight: '300'
    },
    selectColorContainer: {
        flexDirection: 'row'
    },
    inputStyle: {
        flex: 1,
        fontSize: 18,
        paddingHorizontal: 8,
        color: 'white',
        height: 48
    },
    label: {
        fontSize: 18,
        paddingHorizontal: 10,
        marginLeft: 20,
        color: 'white',
        fontFamily: 'Lato-Regular'
    },
    containerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        elevation: 1,
        backgroundColor: '#1a5086',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.3
    },
    ratingStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    mainContainer: {
        flexDirection: 'row',
        marginHorizontal: 15,
        marginBottom: 30
    },
    containerStyle1: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 10,
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: 18,
        color: 'black',
        fontFamily: 'Lato-Regular'
    },
    genderTextStyle: {
        fontSize: 13,
        textAlign: 'center',
        marginTop: 15,
        marginBottom: 5,
        fontFamily: 'Lato-Bold',
        color: 'black'
    },
    genderContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 25
    },
    genderButtonStyle: {
        height: 'auto',
        marginHorizontal: 3,
        marginVertical: 5,
        width: 90,
        paddingVertical: 8,
        backgroundColor: '#ffffff',
        elevation: 5,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.3
    },
    activeGenderButton: {
        backgroundColor: '#ff8400'
    },
    activeGenderText: {
        color: 'white'
    },
    genderButtonTextStyle: {
        textAlign: 'center',
        fontSize: 15,
        fontFamily: 'Lato-Regular',
        color: 'black'
    },
    genderMainContainer: {
        borderBottomWidth: 1,
        borderTopWidth: 1,
        marginHorizontal: 15,
        borderColor: '#e9e9e9'
    },
    ratingFilterContainer: {
        marginHorizontal: 15,
        borderBottomWidth: 1,
        borderColor: '#e9e9e9',
        paddingVertical: 20
    },
    colorContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 15,
        marginVertical: 12
    },
    selectColor: {
        width: 25,
        height: 25,
        borderRadius: 20,
        margin: 2,
        borderWidth: 1,
        borderColor: '#bababa'
    },
    filterButtonStyle: {
        paddingHorizontal: 25,
        paddingBottom: 20
    },
    filterButtonTextStyle: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'Lato-Regular',
        color: 'white'
    },
    activeColorBorderOnActive: {
        borderWidth: 2,
        borderColor: 'black'
    },
    activeColorBorder: {
        borderWidth: 2,
        borderColor: 'red'
    },
    pickerIcon: {
        left: Platform.OS == 'ios' ? 0 : I18nManager.isRTL ? null : 290
    }
});