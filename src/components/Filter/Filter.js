import React, { Component } from 'react';
import { View, Text, TextInput, ScrollView, Alert } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import CustomPicker from './Picker';
import PriceScale from './PriceScale';
import FilterStyle from './FilterStyle';
import { BlockButton } from '../shared/buttons/AppButton';
import i18n from '../../../i18n';
// redux
import { connect } from 'react-redux';
// Actions
import { filterApplied, changeSliderRange, changeInputRange, updateCategory, changedFilterData } from '../../actions/ShopProducts';

class Filter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonColor: { backgroundColor: 'red' },
            gender: '',
            color: ''
        };
        this.previousFilter = {};
    }

    showAlert = (status, message) => {
        Alert.alert(
            status,
            message,
            [{ text: 'OK' }],
            { cancelable: false }
        );
    };

    filterData = () => {
        const { navigation, shopData, filterApplied, changedFilterData, language } = this.props;
        if (this.previousFilter.min_price != shopData.payload.min_price ||
            this.previousFilter.max_price != shopData.payload.max_price ||
            this.previousFilter.categorySelectedValue != shopData.categorySelectedValue) {

            changedFilterData();
        }

        this.previousFilter.min_price = shopData.payload.min_price;
        this.previousFilter.max_price = shopData.payload.max_price;
        this.previousFilter.categorySelectedValue = shopData.categorySelectedValue;

        shopData.payload.page = 1;
        shopData.payload.per_page = 10;
        filterApplied(shopData.payload, null, language);
        { navigation.navigate('ShopNew') }
    };

    radioButtonToggle(val) {
        this.setState({ gender: val })
    };
    colorButtonToggle(val) {
        this.setState({ color: val })
    };

    multiSliderValuesChange = (values) => {
        this.props.changeSliderRange(values);
    };

    handleCategorySelect = (val) => {
        const { categories, updateCategory } = this.props;
        let selectedCategory = categories.find(category => category.name === val);
        if (val) updateCategory(selectedCategory);
    };

    render() {
        const { categories, shopData, changeInputRange, primaryColor, secondaryColor } = this.props;
        return (
            <ViewContainer style={FilterStyle.backgroundColor}>
                <ScrollView>
                    <View style={FilterStyle.filterContainer}>
                        <CustomPicker
                            itemsArray={categories}
                            caretStyle = {FilterStyle.pickerIcon}
                            selectedValue={shopData.categorySelectedValue}
                            onValueChange={(val) => this.handleCategorySelect(val)}
                            pickerRef={'category'}
                        />
                    </View>
                    <View style={{ marginBottom: 30, width: '100%', justifyContent: 'center' }}>
                        <View style={{ paddingHorizontal: 50, justifyContent: 'center' }}>
                            <PriceScale
                                min={shopData.payload.min_price}
                                max={shopData.payload.max_price}
                                step={2}
                                values={[shopData.payload.min_price, shopData.payload.max_price]}
                                onValuesChange={this.multiSliderValuesChange}
                            />
                        </View>
                    </View>
                    <View style={FilterStyle.mainContainer}>
                        <View style={[FilterStyle.containerStyle, { backgroundColor: secondaryColor }]}>
                            <Text style={FilterStyle.label}>USD</Text>
                            <TextInput
                                style={FilterStyle.inputStyle}
                                value={shopData.payload.min_price.toString()}
                                keyboardType={'phone-pad'}
                                underlineColorAndroid={'transparent'}
                                onChangeText={value => changeInputRange({ prop: 'min_price', value })}
                            />
                        </View>
                        <View style={FilterStyle.containerStyle1}>
                            <Text style={FilterStyle.textStyle}>{i18n.t('Filter_Screen.TO')}</Text>
                        </View>
                        <View style={[FilterStyle.containerStyle, { backgroundColor: secondaryColor }]}>
                            <Text style={FilterStyle.label}>USD</Text>
                            <TextInput
                                value={shopData.payload.max_price.toString()}
                                style={FilterStyle.inputStyle}
                                underlineColorAndroid={'transparent'}
                                keyboardType={'phone-pad'}
                                onChangeText={value => changeInputRange({ prop: 'max_price', value })}
                            />
                        </View>
                    </View>
                </ScrollView>
                <View>
                    <View style={FilterStyle.filterButtonStyle}>
                        <BlockButton style={FilterStyle.filterButtonTextStyle} bgColor={primaryColor} onPress={this.filterData}>{i18n.t('Filter_Screen.APPLY_FILTERS')}</BlockButton>
                    </View>
                </View>
            </ViewContainer >
        );
    }
}

function mapStateToProps(state) {
    const categories = state.shopData.categories;
    categories.forEach((category) => {
        category.label = category.name;
        category.value = category.name;
    });
    return {
        shopData: state.shopData,
        categories,
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor,
        language: state.auth.language
    }
}

function mapDispatchToProps(dispatch) {
    return {
        filterApplied: (filterAppliedData, check, language) => dispatch(filterApplied(filterAppliedData, check, language)),
        changeSliderRange: (values) => dispatch(changeSliderRange(values)),
        changeInputRange: (value) => dispatch(changeInputRange(value)),
        updateCategory: (selectedCategory) => dispatch(updateCategory(selectedCategory)),
        changedFilterData: () => dispatch(changedFilterData())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
