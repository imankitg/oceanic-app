import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, FlatList, AsyncStorage, ImageBackground, I18nManager } from "react-native";
import DrawerStyle from './DrawerMenuStyle';
import ProductService from '../../services/ProductService';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { SafeAreaView } from 'react-navigation';
import SpinKit from '../shared/SpinKit/SpinKit';

import i18n, { I18n, changeLanguage } from '../../../i18n';

import CustomPicker from '../shared/picker/Picker';

// redux //
import { connect } from 'react-redux';
import { logout } from "../../actions/App";
import { updateWishListCount, updateWishList, updateCart } from '../../actions/Cart';
import { removeProfile } from '../../actions/Profile'
import { changeUserLanguage } from '../../actions/Products';

class DrawerMenu extends Component {
    constructor(props) {
        super(props);
        let { navigation, logout } = props;
        this.state = {
            ordersNumber: 0,
            categoryNav: [],
            wishListItems: 0,
            flag: true,
            user: [],
            loginAccountNav: [],
            logoutAccountNav: [],
            isFirstTime: true,
            userLanguage: [
                { label: I18n('drawer.CHOOSE_LANGUAGE'), value: "" },
                { label: "English", value: "en" },
                { label: "عربى", value: "ar" }
            ]
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ wishListItems: this.props.wishListCount });
        this.renderDrawerMenu()
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        try {
            await AsyncStorage.getItem('user').then(val => {
                if (JSON.parse(val) !== null) {
                    let parsed = JSON.parse(val);
                    this.props.wish_list_count(parsed.length);
                    this.props.update_wish_list(parsed);
                    this.setState({ wishListItems: JSON.parse(val).length })
                }
                else null
            }).catch((e) => {
                console.log(e)
            });
        }
        catch (e) {
            console.log(e)
        }
        AsyncStorage.getItem('cart_data', (err, result) => {
            if (result) {
                let cartData = JSON.parse(result);
                this.props.update_cart(cartData)
            }
            else {
                console.log('noo result found!!!!')
            }
        })
    };

    componentWillMount() {
        this.renderDrawerMenu();
    }

    renderDrawerMenu = () => {
        let { wishListCount, cartCount } = this.props;
        ProductService.categories()
            .then((response) => {
                let categoryNav = [
                    { title: I18n('drawer.HOME'), link: 'Home', icon: 'home' },
                    { title: I18n('drawer.SHOP'), link: 'ShopNew', icon: 'local-mall', new: 'New' },
                    { title: I18n('drawer.CATEGORIES'), link: 'Categories', icon: 'content-copy' },
                    { title: I18n('drawer.MY_WISHLIST'), link: 'WishList', icon: 'favorite-border', badge: wishListCount !== 0 ? wishListCount : '0' },
                    { title: I18n('drawer.MY_CART'), link: 'Cart', icon: 'local-grocery-store', badge: cartCount.length !== 0 ? cartCount.length : '0' },
                    { title: I18n('drawer.CHANGE_THEME'), link: 'ColorTheme', icon: 'color-lens' },
                ]
                let loginAccountNav = [
                    { title: I18n('drawer.ORDERS'), link: 'Orders', icon: 'view-list' },
                    {
                        title: I18n('drawer.LOGOUT'), icon: 'exit-to-app', func: () => {
                            AsyncStorage.removeItem('USER_LOGIN_INFO');
                            this.props.removeProfile();
                            this.props.navigation.navigate('Home');
                            this.props.logout();
                        }
                    }
                ]
                let logoutAccountNav = [
                    { title: I18n('drawer.LOGIN'), link: 'Login' }
                ]
                if (response.data && response.data.length) {
                    categoryNav = categoryNav.map(res => { return { title: res.title, link: res.link, icon: res.icon, badge: res.badge, new: res.new, data: response.data } });
                }
                typeof (response.data) !== 'string' ?
                    this.setState({ categoryNav, loginAccountNav, logoutAccountNav }) : null
            })
            .catch((err) => { });
    };

    onSelectItem(item) {
        const { isLoggedIn, navigation } = this.props;
        if (item.link == 'Categories') {
            let sortedCategory = item.data.filter(data => data.name != 'Uncategorized');
            navigation.navigate(item.link, { allCategories: sortedCategory })
        }
        else if (item.link == 'WishList') {
            if (isLoggedIn) {
                navigation.navigate(item.link)
            }
            else {
                navigation.navigate('Login')
            }
        }
        else if (item.link) {
            navigation.navigate(item.link)
        }
        else {
            item.func()
        }
    }

    renderMenuItem({ item }) {
        const { primaryColor, secondaryColor } = this.props;
        return (
            <TouchableOpacity style={DrawerStyle.container} onPress={() => this.onSelectItem(item)}>
                <View style={DrawerStyle.menuContainer}>
                    <MaterialIcon style={[DrawerStyle.leftMenuIcons, { color: secondaryColor }]} name={item.icon} />
                    <Text style={DrawerStyle.menuItem}>{item.title}</Text>
                    {
                        item.badge ?
                            <View style={[DrawerStyle.badgeContainer, { backgroundColor: primaryColor }]}>
                                <Text style={DrawerStyle.badgeText}>{item.badge}</Text>
                            </View> : null
                    }
                    {
                        item.new &&
                        <View>
                            <Text style={[DrawerStyle.shopNewText, { color: primaryColor }]}>{I18n('drawer.NEW')}</Text>
                        </View>
                    }
                </View>
            </TouchableOpacity>
        );
    }

    checkUser = () => {
        const { navigation } = this.props;
        AsyncStorage.getItem('USER_LOGIN_INFO', (err, result) => {
            let user = JSON.parse(result);
            if (user) {
                navigation.navigate('Profile')
            }
            else navigation.navigate('Login');
        });
    };

    renderUserName(name) {
        return name.charAt(0).toUpperCase() + name.slice(1);
    }

    changeLanguageFlag = () => {
        this.props.changeUserLanguage()
    }

    changeLanguage = (country) => {
        let { isFirstTime } = this.state;
        if (isFirstTime) this.setState({ isFirstTime: false });

        else changeLanguage(country)
    }

    render() {
        const { isLoggedIn, userProfileData, secondaryColor } = this.props;
        let { categoryNav, user, loginAccountNav } = this.state;
        let updatecategoryNav = categoryNav !== 0 ? categoryNav.concat([]) : [];
        let updateLoginAccountNav = loginAccountNav !== 0 ? loginAccountNav.concat([]) : [];
        let avatar = userProfileData.avatar_url ? { uri: userProfileData.avatar_url } : require('../../assets/images/avatar.png');
        return (
            <SafeAreaView style={{ backgroundColor: '#f2f2f2', flex: 1 }} forceInset={{ top: 'always', horizontal: 'never' }}>
                {isLoggedIn ?
                    <TouchableOpacity onPress={this.checkUser} activeOpacity={0.5}>
                        <ImageBackground style={DrawerStyle.drawerContainer} source={require('../../assets/images/drawer-banner.png')}>
                            <View style={DrawerStyle.userContainer}>
                                <Image style={DrawerStyle.userImage} source={avatar} resizeMethod={'resize'} />
                                <View style={DrawerStyle.userNameEmailContainer}>
                                    <Text style={DrawerStyle.userName}>{this.renderUserName(userProfileData.username) || 'User Name'}</Text>
                                    <Text style={DrawerStyle.userEmail}>{userProfileData.email || 'email@gmail.com'}</Text>
                                </View>
                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                    :
                    <ImageBackground style={[DrawerStyle.drawerContainer, DrawerStyle.logoImageContainer]} source={require('../../assets/images/drawer-banner.png')}>
                        <Image style={DrawerStyle.logoImage} source={require('../../assets/images/drawer-logo.png')} resizeMethod={'resize'} />
                    </ImageBackground>
                }
                {
                    typeof (allProducts) !== 'string' && categoryNav.length ?
                        <ScrollView>
                            {
                                <View style={DrawerStyle.container}>
                                    <FlatList
                                        data={updatecategoryNav}
                                        renderItem={this.renderMenuItem.bind(this)}
                                        keyExtractor={(item) => item.title} />

                                    <CustomPicker
                                        container={{marginHorizontal: 20}}
                                        itemsArray={this.state.userLanguage}
                                        selectedValue={i18n.locale}
                                        pickerContainer={DrawerStyle.pickerStyle}
                                        size={26}
                                        caretStyle={[DrawerStyle.pickerIcon, { color: secondaryColor }]}
                                        onValueChange={lang => this.changeLanguage(lang)}
                                        pickerRef={'language'} />
                                </View>
                            }
                            <View style={DrawerStyle.container}>
                                {
                                    isLoggedIn ?
                                        <FlatList
                                            data={updateLoginAccountNav}
                                            renderItem={this.renderMenuItem.bind(this)}
                                            keyExtractor={(item, index) => { return index.toString() }}
                                        /> :
                                        <View style={{ borderTopWidth: 0.4, borderTopColor: "#000" }}>
                                            <FlatList
                                                data={this.state.logoutAccountNav}
                                                renderItem={this.renderMenuItem.bind(this)}
                                                keyExtractor={(item, index) => { return index.toString() }}
                                            />
                                        </View>
                                }
                            </View>
                        </ScrollView> :
                        <SpinKit />
                }

            </SafeAreaView>
        )
    }
}

function mapStateToProps(state) {
    return {
        userProfileData: state.userProfileData.profile,
        wishListCount: state.cartData.wishListCount,
        cartCount: state.cartData.cart,
        isLoggedIn: state.auth.isLoggedIn,
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logout: () => dispatch(logout()),
        removeProfile: () => dispatch(removeProfile()),
        wish_list_count: (data) => dispatch(updateWishListCount(data)),
        update_wish_list: (data) => dispatch(updateWishList(data)),
        update_cart: (data) => dispatch(updateCart(data)),
        changeUserLanguage: () => dispatch(changeUserLanguage())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerMenu);