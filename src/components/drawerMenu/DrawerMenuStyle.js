import { StyleSheet, I18nManager, Platform } from 'react-native';
import StyleVars from '../../Style/StyleVars';

export default DrawerStyle = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 15
    },
    menuContainer: {
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'center'
    },
    leftMenuIcons: {
        fontSize: 26,
        color: '#1a5086',
        marginRight: 15
    },
    RightMenuIcons: {
        fontSize: 26,
        color: '#1a5086',
        marginLeft: 15
    },
    menuItem: {
        fontSize: 15.3,
        color: '#2a2a2a',
        flex: 1,
        fontWeight: '100',
        textAlign: 'left',
        fontFamily: 'Lato-Regular'
    },
    menuBadge: {
        marginLeft: 10,
        textAlign: 'center',
        backgroundColor: StyleVars.Colors.primary,
        color: StyleVars.Colors.headingTextColor,
        borderRadius: 100,
        fontSize: 16
    },
    drawerContainer: {
        width: '100%',
        height: 166,
        justifyContent: 'center'
    },
    userContainer: {
        flexDirection: 'row',
        paddingLeft: 20,
        paddingRight: 10
    },
    userNameEmailContainer: {
        flex: 1
    },
    userImage: {
        width: 70,
        height: 70,
        borderRadius: Platform.OS == 'ios' ? 35 : 50,
        overflow: 'hidden',
        marginRight: 15
    },
    logoImageContainer: {
        alignItems: 'center'
    },
    logoImage: {
        width: '70%',
        height: '100%',
        resizeMode: 'contain'
    },
    userName: {
        color: '#fff',
        fontSize: 18,
        fontFamily: 'Lato-Bold',
        marginTop: 12,
        marginBottom: 3,
        textAlign: 'left'
    },
    userEmail: {
        color: '#fff',
        fontSize: 14,
        fontFamily: 'Lato-Regular',
        width: '100%',
        textAlign: 'left'
    },
    badgeContainer: {
        backgroundColor: '#ff8400',
        borderRadius: 50,
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    badgeText: {
        color: '#fff',
        fontSize: 16,
        fontFamily: 'Lato-Bold'
    },
    shopNewText: {
        fontSize: 15.3,
        fontFamily: 'Lato-Bold',
        color: '#ff8400',
        alignItems: "flex-start"
    },
    languageBtn: {
        width: '50%', 
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'black',
        borderBottomWidth: 0.8,
        height: 30,
        backgroundColor: '#ff8400'
    },
    languageBtnIcon: {
        position: 'absolute', 
        top: 0, 
        left: 0, 
        bottom: 0, 
        right: 0, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    pickerStyle: {
        marginLeft: 15, 
        marginRight: 15,
        textAlign: 'left'
    },
    pickerIcon: {
        left: I18nManager.isRTL ? 200 : null
    }
});