import _ from 'lodash';

import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Star from '../../../node_modules/react-native-ratings/src/Star.js';

export default class Rating extends Component {
    static defaultProps = {
        defaultRating: 5,
        reviews: [""],
        count: 5,
        onFinishRating: (position) => console.log(position)
    };

    constructor(props) {
        super(props);

        this.state = {
            position: 5,
            ratingText: props.ratingText || "Give Rating"
        }
    }

    componentDidMount() {
        const { defaultRating } = this.props;

        this.setState({ position: defaultRating })
    }

    renderStars(rating_array) {
        return _.map(rating_array, (star, index) => {
            return star
        })
    }

    starSelectedInPosition(position) {
        const { onFinishRating } = this.props;

        onFinishRating(position);

        this.props.ratingHandler && this.props.ratingHandler(position);

        this.setState({ position: position })
    }

    onRating(text) {
        return (
            <Text style={styles.textStyle}>
                {text}
            </Text>
        );
    }
    render() {
        const { position } = this.state;
        const { count, reviews } = this.props;
        const rating_array = [];

        _.times(count, index => {
            rating_array.push(
                <Star
                    key={index}
                    imageSize={30}
                    size={16}
                    position={index + 1}
                    starSelectedInPosition={this.starSelectedInPosition.bind(this)}
                    fill={position >= index + 1}
                    {...this.props}
                />
            )
        });
        return (
            <View style={styles.starContainer}>
                {this.renderStars(rating_array)}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    ratingContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 2,
        marginTop: 10
    },
    textStyle: {
        fontSize: 15,
        fontWeight: '300'
    },
    starContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
});