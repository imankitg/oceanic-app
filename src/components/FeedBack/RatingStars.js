import _ from 'lodash';

import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import Stars from 'react-native-stars';
import RatingIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

class RatingStars extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { defaultRating, size, primaryColor } = this.props;
        let Rating = Number(defaultRating);
        return (
            <View>
                <Stars
                    default={Rating}
                    count={5}
                    update={(val) => this.props.ratingHandler && this.props.ratingHandler(val)}
                    starSize={80}
                    fullStar={<RatingIcon name={'star'} size={size || 20} style={[styles.myStarStyle, { color: primaryColor }]} />}
                    emptyStar={<RatingIcon name={'star-outline'} size={size || 20} style={[styles.myStarStyle, styles.myEmptyStarStyle, { color: primaryColor }]} />}
                    halfStar={<RatingIcon name={'star-half'} size={size || 20} style={[styles.myStarStyle, { color: primaryColor }]} />}
                />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor
    }
}

const styles = StyleSheet.create({
    myStarStyle: {
        color: '#ff8400',
        backgroundColor: 'transparent'
    },
    myEmptyStarStyle: {
        color: '#ff8400'
    }
});

export default connect(mapStateToProps)(RatingStars);