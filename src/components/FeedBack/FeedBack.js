import React, { Component } from 'react';
import { View, Text, TextInput, ScrollView, TouchableOpacity, ToastAndroid, Platform, Alert } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import CheckoutStyles from '../checkout/CheckoutStyle';
import Rating from './RatingStars';
import Api from '../../services/ProductService';
import CustomSpinner from '../shared/SpinKit/SpinKit.js';
import i18n from '../../../i18n';

import { connect } from 'react-redux';

class FeedBack extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product_id: '',
            feedBack: {
                FullName: props.userProfile.username,
                Feedback: '',
                ratings: 5,
                email: props.userProfile.email
            },
            feedbackError: false,
            loading: false
        }
    }

    componentDidMount() {
        this.setState({
            product_id: this.props.navigation.state.params.product_id
        })
    }

    onChange(name, val) {
        const feedBack = this.state.feedBack;
        feedBack[name] = val;
        if(name === 'Feedback') this.setState({ feedbackError: false });
        this.setState({ feedBack })
    };

    postConstumerReview = () => {
        let { product_id, feedBack } = this.state;
        if (!feedBack.Feedback.length) {
            this.setState({feedbackError: true});
            return null;
        }
        this.setState({loading: true});
        Api.PostReview(product_id, feedBack)
            .then(res => {
                Platform.OS === 'ios' ? Alert.alert('Message', 'Review Submitted!!', [{ text: 'OK' }], { cancelable: false }) : ToastAndroid.showWithGravityAndOffset('Review Submitted!!!', ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
                this.props.navigation.navigate('Home');
                this.setState({loading: false});
            })
        .catch(err => {
            this.setState({loading: false});
            Alert.alert('Error', 'Sorry we have experienced a problem. Please submit your review again', [{ text: 'OK' }], { cancelable: false });
            console.log('error message', err)
        })
    };

    ratingHandler = (position) => {
        let feedBack = {...this.state.feedBack};
        feedBack.ratings = position;
        this.setState({ feedBack });
    };

    render() {
        return (
            <ViewContainer style={CheckoutStyles.filterColor}>
                {!this.state.loading ?
                    <ViewContainer>
                        <ScrollView>
                            <View style={CheckoutStyles.feedBackContainer}>
                                <View style={CheckoutStyles.inputContainer}>
                                    <TextInput
                                        value={this.state.feedBack.Feedback}
                                        ref={(ref) => { this.FullName = ref }}
                                        onChangeText={this.onChange.bind(this, 'Feedback')}
                                        keyboardType="default"
                                        style={[CheckoutStyles.inputSearch, CheckoutStyles.textArea, this.state.feedbackError ? CheckoutStyles.errorBorder : null]}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        multiline={true}
                                        numberOfLines={5}
                                        placeholder={i18n.t('FeedBack_Screen.ENTER_FEEDBACK')} />
                                </View>
                                <View style={CheckoutStyles.giveRatingContainer}>
                                    <View style={CheckoutStyles.giveRatingWrapper}>
                                        <Text style={CheckoutStyles.giveRatingText}>{i18n.t('FeedBack_Screen.GIVE_RATINGS')}</Text>
                                    </View>
                                    <View style={CheckoutStyles.ratingWrapper}>
                                        <Rating defaultRating={5} ratingHandler={this.ratingHandler} />
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                        <View style={CheckoutStyles.buttonsContainer}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.goBack()}
                                activeOpacity={0.7}
                                style={CheckoutStyles.formButton1}>
                                <Text style={CheckoutStyles.formButtonText1}>{i18n.t('FeedBack_Screen.CANCEL')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.postConstumerReview}
                                              activeOpacity={0.7}
                                              style={CheckoutStyles.formButton}>
                                <Text style={CheckoutStyles.formButtonText}>{i18n.t('FeedBack_Screen.SUBMIT')}</Text>
                            </TouchableOpacity>
                        </View>
                    </ViewContainer>
                    :
                    <CustomSpinner/>
                }

            </ViewContainer>
        );
    }
}
function mapStateToProps(state) {
    return {
        userProfile: state.userProfileData.profile
    }
}
export default connect(mapStateToProps)(FeedBack);

