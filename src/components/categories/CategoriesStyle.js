import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    categoriesContainer: {
        marginTop: 15
    },
    categoriesWrapper: {
        marginTop: 2,
        marginHorizontal: 15,
        backgroundColor: '#fff',
        shadowColor: '#444',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.6,
        shadowRadius: 2.4,
        elevation: 2,
        marginBottom: 15,
        paddingHorizontal: 3,
        paddingTop: 3,
        borderRadius: 5
    },
    categoriesImgWrapper: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        overflow: 'hidden'
    },
    categoriesImg: {
        width: '100%',
        height: 110
    },
    categoriesUser: {
        fontSize: 18,
        color: '#2a2a2a',
        fontFamily: 'Lato-Bold',
        textAlign: 'center',
        paddingVertical: 5
    },
    loadingContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})