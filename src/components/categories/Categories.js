import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import ViewContainer from '../../components/shared/viewContainer/ViewContainer';
import CategoriesStyle from './CategoriesStyle';
import ProgressBar from 'react-native-progress/Circle';
import ImageProgress from 'react-native-image-progress';
import EmptyDataScreen from '../shared/emptyDataScreen/EmptyDataScreen';
import { connect } from 'react-redux';
import {fetchCategories} from "../../actions/ShopProducts";
import { I18n } from '../../../i18n';

class Categories extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const {language, fetchCategories} = this.props;
        fetchCategories(language)
    }

    categoriesList({ item }) {
        let categoryThumbnail = item.image !== null ? { uri: item.image.src } : require('../../assets/images/placeholder.png');
        return (
            <TouchableOpacity
                onPress={() => { this.props.navigation.navigate('ProductByCategory', { categoryTitle: item.name, id: item.id }) }}>
                <View style={CategoriesStyle.categoriesWrapper}>
                    <View style={CategoriesStyle.categoriesImgWrapper}>
                        <ImageProgress
                            style={CategoriesStyle.categoriesImg}
                            source={categoryThumbnail}
                            indicator={ProgressBar} />
                    </View>
                    <Text style={CategoriesStyle.categoriesUser} ellipsizeMode="tail" numberOfLines={1}>{item.name}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        const { allCategories } = this.props;
        let filterCategory = allCategories.filter(data => data.name != 'Uncategorized');
        return (
            <ViewContainer>
                {allCategories.length ?
                    <View style={CategoriesStyle.categoriesContainer}>
                        <FlatList
                            data={filterCategory}
                            renderItem={this.categoriesList.bind(this)}
                            keyExtractor={(item, index) => { return index.toString() }} />
                    </View>
                    :
                    <EmptyDataScreen message={I18n('Common.NO_CATEGORIES_FOUND')} icon='filter' />
                }
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        allCategories: state.shopData.categories,
        categoryLoading: state.shopData.categoryLoading,
        language: state.auth.language
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchCategories: (lang) => dispatch(fetchCategories(lang)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
