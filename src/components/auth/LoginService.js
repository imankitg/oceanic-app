import WooCommerceApi from "../../../WooCommerce/Api";
import axios from "axios";
import Constants from '../../../WooCommerce/Config' ;

const Api = {
    login: function login(user) {
        return axios.post(`${Constants.url.wc}/api/auth/generate_auth_cookie/?username= ${user.username}&password= ${user.password}`)
    },
    fbLogin: function fbLogin(access_token) {
        return axios.post(`${Constants.url.wc}/api/user/fb_connect/?access_token= ${access_token}`)
    },
    signUp: function signUp(user) {
        var data = {
            email: user.email,
            first_name: '',
            last_name: '',
            username: user.username,
            password: user.password,
            billing: {
                first_name: '',
                last_name: '',
                company: '',
                address_1: '',
                address_2: '',
                city: '',
                state: '',
                postcode: '',
                country: '',
                email: user.email,
                phone: ''
            },
            shipping: {
                first_name: '',
                last_name: '',
                company: '',
                address_1: '',
                address_2: '',
                city: '',
                state: '',
                postcode: '',
                country: ''
            }
        };
        return WooCommerceApi.post('customers', data)
    }
};

export default Api;



