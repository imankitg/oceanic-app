import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, Alert, AsyncStorage, TextInput } from 'react-native';
import { LoginStyles } from './LoginStyle';
import ViewContainer from '../../components/shared/viewContainer/ViewContainer';
import LoginService from './LoginService';
import { BlockButton } from '../shared/buttons/AppButton';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { FBAuth } from "../../utils/Utils";
import { AccessToken } from 'react-native-fbsdk';
import SignUp from './Signup';
import i18n from '../../../i18n';

// redux
import { connect } from 'react-redux'
import { login } from "../../actions/App";
import { fetchProfile } from '../../actions/Profile';
import { fetchOrderNumber } from '../../actions/Orders';
import CustomSpinner from '../shared/SpinKit/SpinKit.js';

var facebook_logo = require('../../assets/images/facebook_logo.png');

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            isLoading: false,
            isSignUp: true
        };
    }

    //Input Value Update
    onChange(name, val) {
        this.setState({ [name]: val });
    }

    fbLogin() {
        FBAuth().then(
            (user) => {
                this.setState({
                    isLoading: true
                });
                AccessToken.getCurrentAccessToken()
                    .then((res) => {
                        if (res) {
                            LoginService.fbLogin(res.accessToken)
                                .then((response) => {
                                    this.postLogin({ id: response.data.wp_user_id });
                                })
                                .catch((error, data) => {
                                    console.log('error network -', error, data);
                                });
                        }
                    });
            }),
            (err) => {
                Alert.alert(
                    'Login Failed',
                    'Login ' + err
                );
            }
    }

    login() {
        if (this.state.username && this.state.password) {
            this.setState({
                isLoading: true
            });
            LoginService.login(this.state)
                .then((response) => {
                    this.setState({
                        isLoading: false
                    });
                    if (response.data.error) {
                        return Alert.alert(
                            'Login Error',
                            response.data.error
                        )
                    }
                    this.postLogin(response.data.user);
                })
                .catch((error, data) => {
                    this.setState({
                        isLoading: false
                    });
                });
        }
        else Alert.alert(
            i18n.t('Login_Screen.LOGIN_ERROR'),
            i18n.t('Login_Screen.ERROR_MSG')
        )
    }

    postLogin = async (user) => {
        this.setState({
            username: '',
            password: '',
            isLoading: false
        });

        AsyncStorage.setItem('USER_LOGIN_INFO', JSON.stringify(user || {}));
        AsyncStorage.getItem('USER_LOGIN_INFO', (err, result) => {
            console.log("result: ", result);
        });
        this.props.fetchProfile(user.id);
        this.props.fetchOrderNumber(user.id);
        this.props.login();
        if (this.props.navigation.state.params && this.props.navigation.state.params.isCheckout) {
            this.props.navigation.navigate("Checkout");
        }
        else this.props.navigation.navigate("Home");
    };

    render() {
        let { isSignUp } = this.state;
        let { primaryColor, secondaryColor } = this.props;
        return (

            <ViewContainer style={LoginStyles.loginWrapper}>

                <View style={LoginStyles.btnContainer}>
                    <TouchableOpacity style={LoginStyles.btn} onPress={() => this.setState({ isSignUp: true })}>
                        <Text style={LoginStyles.btnColor}>{i18n.t('Login_Screen.LOGIN')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[LoginStyles.btn, { borderLeftWidth: 0.8 }]} onPress={() => this.setState({ isSignUp: false })}>
                        <Text style={LoginStyles.btnColor}>{i18n.t('Login_Screen.SIGN_UP')}</Text>
                    </TouchableOpacity>
                </View>
                {isSignUp ?
                    (!this.state.isLoading ?
                        <KeyboardAwareScrollView style={LoginStyles.loginContainer}>
                            <View style={LoginStyles.connectAccount}>
                                <Text style={LoginStyles.connectAccountHeading}>{i18n.t('Login_Screen.CONNECT_WITH')}</Text>
                            </View>

                            <View style={LoginStyles.socialIconsWrapper}>
                                <TouchableOpacity
                                    style={[LoginStyles.socialIcons, { backgroundColor: secondaryColor }]}
                                    onPress={this.fbLogin.bind(this)}>
                                    <Image source={facebook_logo} style={LoginStyles.facebookLogo} resizeMethod={'resize'} />
                                </TouchableOpacity>
                            </View>

                            <View style={LoginStyles.orContentContainer}>
                                <View style={LoginStyles.horizontalLine} />
                                <View style={LoginStyles.orTextCont}><Text style={LoginStyles.orText}>{i18n.t('Common.OR')}</Text></View>
                                <View style={LoginStyles.horizontalLine} />
                            </View>

                            <View style={LoginStyles.inputContainer}>
                                <TextInput onSubmitEditing={() => this.password.focus()}
                                    keyboardType={'email-address'}
                                    autoCapitalize="none"
                                    value={this.state.username}
                                    onChangeText={this.onChange.bind(this, 'username')}
                                    style={LoginStyles.inputSearch}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    placeholder={i18n.t('Login_Screen.USER_OR_EMAIL')} />
                            </View>

                            <View style={LoginStyles.inputContainer}>
                                    <TextInput value={this.state.password}
                                               onChangeText={this.onChange.bind(this, 'password')}
                                               style={LoginStyles.inputSearch}
                                               ref={(ref) => { this.password = ref }}
                                               underlineColorAndroid='rgba(0,0,0,0)'
                                               secureTextEntry={true}
                                               placeholder={i18n.t('Login_Screen.PASSWORD')}/>
                            </View>

                            <View style={LoginStyles.buttonsContainer}>
                                <BlockButton onPress={this.login.bind(this)} bgColor={primaryColor}>{i18n.t('Login_Screen.LOGIN')}</BlockButton>
                            </View>
                        </KeyboardAwareScrollView>
                        :
                        <CustomSpinner />)
                    :
                    <SignUp />
                }
            </ViewContainer>
        )
    }
}

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor
    }
}

function mapDispatchToProps(dispatch) {
    return {
        login: () => dispatch(login()),
        fetchProfile: (id) => dispatch(fetchProfile(id)),
        fetchOrderNumber: (id) => dispatch(fetchOrderNumber(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
