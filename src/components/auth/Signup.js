import React, { Component } from 'react';
import { View, Alert, TextInput } from 'react-native';
import { LoginStyles } from './LoginStyle';
import ViewContainer from '../../components/shared/viewContainer/ViewContainer';
import LoginService from './LoginService';
import { BlockButton } from '../shared/buttons/AppButton';
import CustomSpinner from '../shared/SpinKit/SpinKit.js';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import i18n from '../../../i18n';

// redux
import { connect } from 'react-redux';

class Signup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            email: '',
            username: '',
            password: '',
            cnfpassword: ''
        };
    }

    //Input Value Update
    onChange(name, val) {
        this.setState({ [name]: val });
    }

    signUp() {
        if (this.state.email && this.state.username && this.state.password) {
            if (this.state.password != this.state.cnfpassword) {
                return Alert.alert(
                    i18n.t('SignUp_Screen.ERROR_HEADING'),
                    i18n.t('SignUp_Screen.PASSWORD_ERROR')
                )
            }
            this.setState({
                isLoading: true
            });
            LoginService.signUp(this.state)
                .then((response) => {
                    if (response.data && response.data.errors) {
                        return Alert.alert(
                            i18n.t('SignUp_Screen.ERROR_HEADING'),
                            response.data.errors[0].message
                        )
                    }
                    this.setState({
                        isLoading: false,
                        email: '',
                        username: '',
                        password: '',
                        cnfpassword: ''
                    });
                    Alert.alert(
                        i18n.t('SignUp_Screen.ACCOUNT_CREATED'),
                        i18n.t('SignUp_Screen.ACCOUNT_CREATED_ALERT')
                    );
                })
                .catch((error) => {
                    this.setState({
                        isLoading: false
                    });
                    Alert.alert(
                        i18n.t('SignUp_Screen.ERROR_HEADING'),
                        i18n.t('SignUp_Screen.SIGNUP_ALERT'),
                    )
                });
        }
        else Alert.alert(
            i18n.t('SignUp_Screen.ERROR_HEADING'),
            i18n.t('SignUp_Screen.ERROR_MSG')
        )
    }

    render() {
        let { primaryColor } = this.props;
        return (
            <ViewContainer style={LoginStyles.signUpWrapper}>
                {!this.state.isLoading ?
                    <KeyboardAwareScrollView style={LoginStyles.signUpContainer}>
                        <View style={LoginStyles.inputContainer}>
                            <TextInput onSubmitEditing={() => this.email.focus()}
                                value={this.state.username}
                                autoCapitalize='none'
                                ref={(ref) => { this.username = ref }}
                                onChangeText={this.onChange.bind(this, 'username')}
                                style={LoginStyles.inputSearch}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder={i18n.t('SignUp_Screen.USERNAME')} />
                        </View>

                        <View style={LoginStyles.inputContainer}>
                            <TextInput onSubmitEditing={() => this.password.focus()}
                                value={this.state.email}
                                keyboardType={'email-address'}
                                autoCapitalize="none"
                                ref={(ref) => { this.email = ref }}
                                onChangeText={this.onChange.bind(this, 'email')}
                                style={LoginStyles.inputSearch}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder={i18n.t('SignUp_Screen.EMAIL')} />
                        </View>

                        <View style={LoginStyles.inputContainer}>
                            <TextInput onSubmitEditing={() => this.cnfpassword.focus()}
                                value={this.state.password}
                                ref={(ref) => { this.password = ref }}
                                onChangeText={this.onChange.bind(this, 'password')}
                                style={LoginStyles.inputSearch}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                secureTextEntry={true}
                                placeholder={i18n.t('SignUp_Screen.PASSWORD')} />
                        </View>

                        <View style={LoginStyles.inputContainer}>
                            <TextInput value={this.state.cnfpassword}
                                ref={(ref) => { this.cnfpassword = ref }}
                                onChangeText={this.onChange.bind(this, 'cnfpassword')}
                                style={LoginStyles.inputSearch}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                secureTextEntry={true}
                                placeholder={i18n.t('SignUp_Screen.CONFIRM_PASSWORD')} />
                        </View>

                        <View style={LoginStyles.buttonsContainer}>
                            <BlockButton onPress={this.signUp.bind(this)} bgColor={primaryColor}>{i18n.t('SignUp_Screen.SIGNUP')}</BlockButton>
                        </View>
                    </KeyboardAwareScrollView>
                    :
                    <CustomSpinner />
                }

            </ViewContainer>
        )
    }
}

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor,
    }
}

function mapDispatchToProps(dispatch) {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);