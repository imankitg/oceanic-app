import {StyleSheet, Dimensions} from 'react-native';
let {height, width} = Dimensions.get('screen');
import StyleVars from '../../Style/StyleVars';
import { gray } from 'ansi-colors';

export const LoginStyles = StyleSheet.create({

    loginWrapper: {
        backgroundColor : '#f9f9f9'
    },
    loginContainer: {
        paddingTop: 10
    },
    connectAccount: {
        alignItems: 'center',
        marginVertical: 25
    },
    facebookLogo: {
        resizeMode: 'contain',
        width: 90
    },
    socialIconsWrapper: {
        marginBottom: 40,
        paddingHorizontal: 30
    },
    socialIcons: {
        backgroundColor: '#1a5086',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        height: 34,
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 2
    },
    connectAccountHeading: {
        fontSize: 14,
        color: '#2a2a2a',
        fontFamily: 'Lato-Regular'
    },
    titleContainer: {
        flex: 1,
        flexDirection: 'row'
    },
    orContentContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
        paddingHorizontal: 30
    },
    horizontalLine: {
        borderWidth: 0.5,
        borderColor: '#a3a3a3',
        flex: 1,
        marginTop: 5
    },
    orTextCont: {
        marginHorizontal: 20
    },
    orText: {
        fontSize: 24,
        color: '#2a2a2a',
        fontFamily: 'Lato-Light',
    },

    titleBox: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonsContainer: {
        padding: 30,
        alignItems: 'center'
    },
    socialButtonsContainer: {
        flex: 1,
        marginTop: 10,
        alignItems: 'center',
        flexDirection: 'column'
    },
    formButton: {
        backgroundColor: '#1a5086',
        width: '100%',
        paddingVertical: 8,
        shadowColor: '#444',
        shadowOffset: { width: 0, height: 0.5 },
        shadowOpacity: 0.8,
        shadowRadius: 2.4,
        elevation: 1
    },
    gmailButton: {
        backgroundColor: '#df4a32'
    },
    fbButton: {
        backgroundColor: '#3d69a9'
    },
    formButtonText: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'Lato-Bold'
    },
    btnContainer : {
        width: '100%',
        flexDirection: 'row',
        paddingTop: 15
    },
    btn : {
        width: '50%',
        alignItems: 'center',
        borderColor: 'lightgray',
        paddingVertical: 8,
        borderBottomWidth: 0.8

    },
    btnColor: {
        fontSize: 14,
        color: '#2a2a2a',
        fontFamily: 'Lato-Regular'
    },
    inputSearch: {
        borderColor: "#a3a3a3",
        borderBottomWidth: 1,
        fontSize: 16,
        padding: 8,
        color: "#a2a2a2",
        backgroundColor: "#f9f9f9",
        marginBottom: 10
    },
    inputContainer: {
        marginTop: 10,
        paddingHorizontal: 30
    },
    signUpWrapper: {
        paddingTop: 20,
        backgroundColor: '#f9f9f9'
    },
    signUpContainer: {
    }
});
