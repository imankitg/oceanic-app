import React, { Component } from 'react';
import { View, Platform, ToastAndroid, Alert } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import ProfileStyles from './ProfileStyle';
import StyleVars from '../../Style/StyleVars';
import { AppButton } from '../shared/buttons/AppButton';
import HeaderBar from '../shared/headerBar/HeaderBar';
import ImageProgress from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Circle';
import { TextField } from 'react-native-material-textfield';
import SpinKit from '../shared/SpinKit/SpinKit';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import i18n from '../../../i18n';

// redux
import { connect } from 'react-redux';
import { updateProfile } from '../../actions/Profile';

class PersonalInformation extends Component {

    constructor(props) {
        super(props);

        const {
            first_name = '',
            last_name = '',
            username = '',
            email = '',
            billing = {},
            shipping = {},
            avatar_url = ''
        } = props.profile;

        this.state = {
            first_name,
            last_name,
            username,
            email,
            billing,
            shipping,
            avatar_url
        };
    }

    onCustomerChange(name, val) {
        this.setState({ [name]: val });
    }

    showAlert = (status, message) => {
        Alert.alert(
            status,
            message,
            [{ text: 'OK' }],
            { cancelable: false }
        );
    };

    onFocus() {
        let { errors = {} } = this.state;
        for (let name in errors) {
            let ref = this[name];
            if (ref && ref.isFocused()) {
                delete errors[name];
            }
        }
        this.setState({ errors });
    }

    update() {
        let errors = {};
        ['first_name', 'last_name', 'email']
            .forEach((name) => {
                let value = this[name].value();
                if (!value) {
                    errors[name] = `${[name]} is a required field`;
                }
            });
        this.setState({ errors }, () => {
            if (!Object.keys(errors).length) {
                let customer = Object.assign({}, this.state);
                this.props.updateProfile(this.props.profile.id, customer, (profile) => {
                    Platform.OS === 'ios' ?
                        this.showAlert('Success', profile.first_name + ' Your profile has been updated')
                        :
                        ToastAndroid.showWithGravityAndOffset(profile.first_name + " Your profile has been updated", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
                    this.props.navigation.navigate('Profile');
                });
            }
        })

    }

    render() {
        let { first_name, last_name, username, email, avatar_url, errors = {} } = this.state;
        return (
            <ViewContainer>
                {this.props.isLoading ?
                    <SpinKit/>
                    :
                    <KeyboardAwareScrollView behavior="padding">
                        <View>
                            <HeaderBar title={i18n.t('Profile_Screen.PERSONAL_DETAILS')} />
                            <View style={ProfileStyles.inputContainer}>
                                <View style={ProfileStyles.avatarWrapper}>
                                    <ImageProgress source={{ uri: avatar_url }} style={ProfileStyles.avatar} indicator={ProgressBar} />
                                </View>

                                <View style={ProfileStyles.inputFieldWrapper}>
                                    <TextField
                                        label={i18n.t('Profile_Screen.FIRST_NAME')}
                                        value={first_name}
                                        onChangeText={this.onCustomerChange.bind(this, 'first_name')}
                                        tintColor={'#fe8400'}
                                        onFocus={this.onFocus.bind(this)}
                                        ref={(ref) => { this.first_name = ref }}
                                        onSubmitEditing={() => this.last_name.focus()}
                                        error={errors.first_name}
                                    />

                                    <TextField
                                        label={i18n.t('Profile_Screen.LAST_NAME')}
                                        value={last_name}
                                        onChangeText={this.onCustomerChange.bind(this, 'last_name')}
                                        tintColor={'#fe8400'}
                                        onFocus={this.onFocus.bind(this)}
                                        ref={(ref) => { this.last_name = ref }}
                                        onSubmitEditing={() => this.username.focus()}
                                        error={errors.last_name}

                                    />

                                    <TextField
                                        label={i18n.t('Profile_Screen.DISPLAY_NAME')}
                                        placeholder={`${username} (${i18n.t("Common.CANT_CHANGE")})`}
                                        editable={false}
                                        onChangeText={this.onCustomerChange.bind(this, 'username')}
                                        tintColor={'#fe8400'}
                                        onFocus={this.onFocus.bind(this)}
                                        ref={(ref) => { this.username = ref }}
                                        onSubmitEditing={() => this.email.focus()}
                                        error={errors.username}
                                    />

                                    <TextField
                                        label={i18n.t('Profile_Screen.EMAIL')}
                                        value={email}
                                        onChangeText={this.onCustomerChange.bind(this, 'email')}
                                        tintColor={'#fe8400'}
                                        keyboardType={'email-address'}
                                        onFocus={this.onFocus.bind(this)}
                                        autoCapitalize="none"
                                        ref={(ref) => { this.email = ref }}
                                        error={errors.email}
                                    />
                                </View>
                            </View>

                            <View style={ProfileStyles.buttonsContainer}>
                                <AppButton onPress={() => this.props.navigation.goBack()} bgColor={StyleVars.Colors.grey}>{i18n.t('Profile_Screen.CANCEL')}</AppButton>
                                <AppButton onPress={this.update.bind(this)}>{i18n.t('Profile_Screen.UPDATE')}</AppButton>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                }
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        isLoading: state.userProfileData.isLoading,
        profile: state.userProfileData.profile
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateProfile: (id, profile, done) => dispatch(updateProfile(id, profile, done))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInformation);
