import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import ProfileStyles from './ProfileStyle';
import HeaderBar from '../shared/headerBar/HeaderBar';
import ImageProgress from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Circle';
import countries from '../../assets/countries/countries.json';
import i18n from '../../../i18n';

// redux
import { connect } from 'react-redux';
import { updateProfile } from '../../actions/Profile';

class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    renderAvatar() {
        if (this.props.profile && this.props.profile.avatar_url) {
            return (
                <ImageProgress source={{ uri: this.props.profile.avatar_url }} style={ProfileStyles.avatar} indicator={ProgressBar} />
            )
        }
        return (
            <Image source={require('../../assets/images/avatar.png')} style={ProfileStyles.avatar} resizeMethod={'resize'}/>
        )
    }

    render() {
        let { profile, navigation } = this.props;

        const billingCountry = countries.find(obj => obj.value === profile.billing.country);
        const shippingCountry = countries.find(obj => obj.value === profile.shipping.country);

        let billingState = {};
        let shippingState = {};

        if(billingCountry && billingCountry.states) billingState = billingCountry.states.find(state => state.value === profile.billing.state);
        else billingState.label = profile.billing.state;

        if(shippingCountry && shippingCountry.states) shippingState = shippingCountry.states.find(state => state.value === profile.shipping.state);
        else shippingState.label = profile.shipping.state;

        return (
            <ViewContainer>
                <ScrollView>
                    <View>
                        <HeaderBar title={i18n.t('Profile_Screen.PERSONAL_INFORMATION')} />
                        <TouchableOpacity onPress={() => navigation.navigate('PersonalInformation')}>
                            <View style={ProfileStyles.detailContainer}>
                                <View style={ProfileStyles.avatarWrapper}>
                                    {this.renderAvatar()}
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.FIRST_NAME')} {profile.first_name || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.LAST_NAME')} {profile.last_name || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.DISPLAY_NAME')} {profile.username || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.EMAIL')} {profile.email}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.PASSWORD')}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate('BillingInformation')}>
                            <HeaderBar title={i18n.t('Profile_Screen.BILLING_INFORMATION')} />
                            <View style={ProfileStyles.detailContainer}>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.FIRST_NAME')} {profile.billing.first_name || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.LAST_NAME')} {profile.billing.last_name || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.COMPANY')} {profile.billing.company || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.COUNTRY')} {billingCountry && billingCountry.label || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.ADDRESS_1')} {profile.billing.address_1 || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.ADDRESS_2')} {profile.billing.address_2 || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.TOWN/CITY')} {profile.billing.city || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.STATE')} {billingState && billingState.label || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.POSTCODE')} {profile.billing.postcode || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.PHONE')} {profile.billing.phone || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.EMAIL')} {profile.billing.email || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate('ShippingInformation')}>
                            <HeaderBar title={i18n.t('Profile_Screen.SHIPPING_INFORMATION')} />
                            <View style={ProfileStyles.detailContainer}>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.FIRST_NAME')} {profile.shipping.first_name || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.LAST_NAME')} {profile.shipping.last_name || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.COMPANY')} {profile.shipping.company || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.COUNTRY')} {shippingCountry && shippingCountry.label || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.ADDRESS_1')} {profile.shipping.address_1 || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.ADDRESS_2')} {profile.shipping.address_2 || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.TOWN/CITY')} {profile.shipping.city || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.STATE')} {shippingState && shippingState.label || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                                <View style={ProfileStyles.inputContainer}>
                                    <Text>{i18n.t('Profile_Screen.POSTCODE')} {profile.shipping.postcode || i18n.t("Common.NOT_AVAILABLE")}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        profile: state.userProfileData.profile
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateProfile: (id, profile, done) => dispatch(updateProfile(id, profile, done))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
