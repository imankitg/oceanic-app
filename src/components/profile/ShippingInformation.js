import React, { Component } from 'react';
import { View, Platform, Alert, ToastAndroid, Text } from 'react-native';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import StyleVars from '../../Style/StyleVars';
import { AppButton } from '../shared/buttons/AppButton';
import ProfileStyles from './ProfileStyle';
import HeaderBar from '../shared/headerBar/HeaderBar';
import { TextField } from 'react-native-material-textfield';
import SpinKit from '../shared/SpinKit/SpinKit';
import CustomPicker from '../shared/picker/Picker';
import countries from '../../assets/countries/countries.json';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import i18n from '../../../i18n';

// redux
import { connect } from 'react-redux';
import { updateProfile } from '../../actions/Profile';

class ShippingInformation extends Component {

    constructor(props) {
        super(props);
        const {
            first_name = '',
            last_name = '',
            company = '',
            address_1 = '',
            address_2 = '',
            city = '',
            state = 'FL',
            postcode = '',
            country = 'US',
        } = props.profile.shipping;

        this.state = {
            first_name,
            last_name,
            company,
            address_1,
            address_2,
            city,
            state: state || 'FL',
            postcode,
            country: country || 'US',
            statesData: []
        };
        this.countriesData = countries;
    }

    componentWillMount() {
        this.getStatesName()
    }

    getStatesName() {
        let { country } = this.state;
        let selectedCountry = this.countriesData.find((obj) => obj.value === country);
        this.setState({ statesData: selectedCountry.states || [] }, () => {
            if (!this.state.statesData) {
                this.setState({ state: '' })
            }
        });
    }

    onCustomerShippingChange(name, val) {
        this.setState({ [name]: val });
    }

    onFocus() {
        let { errors = {} } = this.state;
        for (let name in errors) {
            let ref = this[name];
            if (ref && ref.isFocused()) {
                delete errors[name];
            }
        }
        this.setState({ errors });
    }

    update() {
        let errors = {};
        ['first_name', 'last_name', 'address_1', 'city', 'postcode']
            .forEach((name) => {
                let value = this[name].value();
                if (!value) {
                    errors[name] = `${[name]} is a required field`;
                }
            });
        this.setState({ errors }, () => {
            if (!Object.keys(errors).length) {
                let customer = this.props.profile;
                customer.shipping = Object.assign({}, this.state);
                delete customer.shipping.statesData;
                this.props.updateProfile(this.props.profile.id, customer, (profile) => {
                    Platform.OS === 'ios' ?
                        Alert.alert(
                            "Success",
                            profile.first_name + " Your profile has been updated",
                            [{ text: 'OK' }],
                            { cancelable: false }
                        )
                        :
                        ToastAndroid.showWithGravityAndOffset(profile.first_name + " Your profile has been updated", ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
                    this.props.navigation.goBack();
                });
            }
        })
    }

    handleCountryChange(country) {
        this.setState({ country }, () => this.getStatesName());
    }

    handleStateChange = (state) => {
        this.setState({ state });
    };

    render() {
        const { first_name, last_name, company, address_1, address_2, city, state, postcode, country, statesData, errors = {} } = this.state;
        return (
            <ViewContainer>
                {this.props.isLoading ?
                    <SpinKit />
                    :
                    <KeyboardAwareScrollView>
                        <View>
                            <HeaderBar title={i18n.t('Profile_Screen.SHIPPING_DETAILS')} />
                            <View style={ProfileStyles.inputContainer}>
                                <View style={ProfileStyles.inputFieldWrapper}>
                                    <TextField
                                        label={i18n.t('Profile_Screen.FIRST_NAME')}
                                        value={first_name}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'first_name')}
                                        tintColor={'#fe8400'}
                                        onFocus={this.onFocus.bind(this)}
                                        ref={(ref) => { this.first_name = ref }}
                                        onSubmitEditing={() => this.last_name.focus()}
                                        error={errors.first_name} />

                                    <TextField
                                        label={i18n.t('Profile_Screen.LAST_NAME')}
                                        value={last_name}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'last_name')}
                                        tintColor={'#fe8400'}
                                        ref={(ref) => { this.last_name = ref }}
                                        onFocus={this.onFocus.bind(this)}
                                        onSubmitEditing={() => this.company.focus()}
                                        error={errors.last_name} />

                                    <TextField
                                        label={i18n.t('Profile_Screen.COMPANY_NAME')}
                                        value={company}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'company')}
                                        tintColor={'#fe8400'}
                                        ref={(ref) => { this.company = ref }}
                                        onSubmitEditing={() => this.address_1.focus()} />

                                    <View style={ProfileStyles.pickerWrapper}>
                                        <Text style={ProfileStyles.titleStyle}>{i18n.t('Profile_Screen.COUNTRY')}</Text>
                                        <CustomPicker
                                            itemsArray={this.countriesData}
                                            selectedValue={country}
                                            onValueChange={country => this.handleCountryChange(country)}
                                            pickerRef={'countries'}
                                            pickerContainer={ProfileStyles.pickerContainer} />
                                    </View>

                                    <TextField
                                        label={i18n.t('Profile_Screen.STREET_ADDRESS_1')}
                                        value={address_1}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'address_1')}
                                        tintColor={'#fe8400'}
                                        ref={(ref) => { this.address_1 = ref }}
                                        onSubmitEditing={() => this.address_2.focus()}
                                        onFocus={this.onFocus.bind(this)}
                                        error={errors.address_1} />

                                    <TextField
                                        label={i18n.t('Profile_Screen.STREET_ADDRESS_2')}
                                        value={address_2}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'address_2')}
                                        tintColor={'#fe8400'}
                                        ref={(ref) => { this.address_2 = ref }}
                                        onSubmitEditing={() => this.city.focus()} />

                                    <TextField
                                        label={i18n.t('Profile_Screen.TOWN/CITY')}
                                        value={city}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'city')}
                                        tintColor={'#fe8400'}
                                        ref={(ref) => { this.city = ref }}
                                        onFocus={this.onFocus.bind(this)}
                                        onSubmitEditing={() => this.postcode.focus()}
                                        error={errors.city} />

                                    {statesData && statesData.length ?
                                        <View style={ProfileStyles.pickerWrapper}>
                                            <Text style={ProfileStyles.titleStyle}>{i18n.t('Profile_Screen.STATE')}</Text>
                                            <CustomPicker
                                                itemsArray={statesData}
                                                selectedValue={state}
                                                onValueChange={state => this.handleStateChange(state)}
                                                pickerRef={'states'}
                                                pickerContainer={ProfileStyles.pickerContainer} />
                                        </View>
                                        :
                                        <TextField
                                            label={i18n.t('Profile_Screen.STATE')}
                                            value={state}
                                            onChangeText={this.onCustomerShippingChange.bind(this, 'state')}
                                            tintColor={'#fe8400'}
                                            onSubmitEditing={() => this.postcode.focus()}
                                            onFocus={() => this.setState({ state: '' })} />
                                    }

                                    <TextField
                                        label={i18n.t('Profile_Screen.POSTCODE')}
                                        value={postcode}
                                        onChangeText={this.onCustomerShippingChange.bind(this, 'postcode')}
                                        tintColor={'#fe8400'}
                                        keyboardType={'numeric'}
                                        onFocus={this.onFocus.bind(this)}
                                        ref={(ref) => { this.postcode = ref }}
                                        error={errors.postcode} />
                                </View>
                            </View>

                            <View style={ProfileStyles.buttonsContainer}>
                                <AppButton onPress={() => this.props.navigation.goBack()} bgColor={StyleVars.Colors.grey}>{i18n.t('Profile_Screen.CANCEL')}</AppButton>
                                <AppButton onPress={this.update.bind(this)}>{i18n.t('Profile_Screen.UPDATE')}</AppButton>
                            </View>

                        </View>
                    </KeyboardAwareScrollView>
                }
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        isLoading: state.userProfileData.isLoading,
        profile: state.userProfileData.profile
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateProfile: (id, profile, done) => dispatch(updateProfile(id, profile, done))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShippingInformation);
