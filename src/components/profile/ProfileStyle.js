import { StyleSheet, Dimensions, Platform } from 'react-native';
let { height, width } = Dimensions.get('screen');
import StyleVars from '../../Style/StyleVars';

export default StyleSheet.create({
    titleBox: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonsContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 10,
        paddingBottom: 30
    },
    updateBtnContainer: {
        flex: 1
    },
    cancelBtnContainer: {
        flex: 1
    },
    formCancelButton: {
        backgroundColor: StyleVars.Colors.grey
    },
    formButton: {
        backgroundColor: StyleVars.Colors.primary,
        paddingVertical: 18.5,
        marginVertical: 20,
        marginHorizontal: 20,
        borderRadius: 5
    },
    formButtonText: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 20,
        borderRadius: 5
    },
    "inputSearch": {
        "borderColor": "#ddd",
        "borderBottomWidth": 1,
        "fontSize": 14,
        "padding": 8,
        "marginHorizontal": 10,
        "color": "#333",
        "backgroundColor": "rgba(255,255,255,0.9)",
        "width": width - 20
    },
    inputContainer: {
        paddingVertical: 10
    },
    detailContainer: {
        padding: 20
    },
    titleStyle: {
        fontSize: 12,
        color: 'rgba(0, 0, 0, .38)',
        marginBottom: -5
    },
    avatarWrapper: {
        alignItems: 'center'
    },
    avatar: {
        width: 80,
        height: 80,
        borderRadius: Platform.OS == 'ios' ? 40 : 50,
        overflow: 'hidden'
    },
    inputFieldWrapper: {
        paddingHorizontal: 10
    },
    inputField: {
    },
    pickerWrapper: {
        backgroundColor: 'transparent',
        marginTop: 10
    },
    pickerContainer: {
        marginLeft: -8
    }
});