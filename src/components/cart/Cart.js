import React, { Component } from 'react';
import { Text, View, TouchableOpacity, FlatList, Image, ToastAndroid, Alert, Platform, AsyncStorage } from "react-native";
import CartStyles from "./CartStyle";
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Api from '../../services/ProductService'
import EmptyDataScreen from '../shared/emptyDataScreen/EmptyDataScreen';
import i18n from '../../../i18n';

// redux
import { connect } from 'react-redux';
import { updateCart, updateTotal, increment, decrement } from '../../actions/Cart';
import SpinKit from '../shared/SpinKit/SpinKit';

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cartList: props.cartData.cart
        }
    }

    componentDidMount() {
        let { cartList } = this.state;
        let totalAmount = 0;
        for (var key in cartList) {
            cartList[key].total_price = cartList[key].quantity * cartList[key].price;
            totalAmount += cartList[key].quantity * cartList[key].price;
        }
        this.props.updateTotal(totalAmount);
    }

    deleteCartItem(item, index) {
        let { cart, total } = this.props.cartData;
        AsyncStorage.removeItem('cart_data');
        let { params } = this.props.navigation.state;
        params && params.handleRemove ? params.handleRemove() : null;
        params && params.handleSave ? params.handleSave(cart[index].id) : null;
        cart.splice(index, 1);
        total = parseFloat(total) - (item.price * item.quantity);
        item.quantity = 0;
        this.props.updateCart(cart);
        this.props.updateTotal(total);
        AsyncStorage.setItem("cart_data", JSON.stringify(cart));
        Platform.OS === 'ios' ?
            Alert.alert(i18n.t('CartScreen.MESSAGE'), i18n.t('CartScreen.ITEM_REMOVE_MSG'))
            :
            ToastAndroid.showWithGravityAndOffset(i18n.t('CartScreen.ITEM_REMOVE_MSG'), ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
    }

    onChange = (type, index) => {
        let { cart, total } = this.props.cartData;
        let totalAmount = total;
        let updateCart = cart.slice();
        if (type === 'increment') {
            updateCart[index].quantity = updateCart[index].quantity + 1;
            updateCart[index].total_price = updateCart[index].quantity * updateCart[index].price;
            let totalAmount = 0;
            for (var key in cart) {
                totalAmount = totalAmount + Number(cart[key].total_price)
            }
            this.props.updateTotal(totalAmount);
        }
        else if (updateCart[index].quantity <= 1) {
            updateCart[index].quantity = 1
        }
        else {
            updateCart[index].quantity = updateCart[index].quantity - 1;
            updateCart[index].total_price = updateCart[index].quantity * updateCart[index].price;
            totalAmount = totalAmount - updateCart[index].price;
            this.props.updateTotal(totalAmount);
        }
        this.setState({ cartList: updateCart })
    };

    productList({ item, index }) {
        let { secondaryColor } = this.props;
        const { cart } = this.props.cartData;
        return (
            <View style={CartStyles.itemContainer} key={index}>
                <View style={CartStyles.imageContainer}>
                    <Image source={{ uri: Api.convertingURL(item.images[0].src) }} style={CartStyles.itemImage} resizeMethod={'resize'} />
                </View>
                <View style={CartStyles.detailContainer}>
                    <View style={CartStyles.detail}>
                        <View style={CartStyles.actionContainer}>
                            <Text style={CartStyles.itemTitle} ellipsizeMode="tail" numberOfLines={2}>{Api.convertingLanguage(item.name)}</Text>
                            <TouchableOpacity style={CartStyles.deleteIcons} onPress={() => this.deleteCartItem(item, index)}>
                                <MaterialIcon name="close" size={20} color='#b1b1b1' />
                            </TouchableOpacity>
                        </View>
                        <View style={CartStyles.detailCartContainer}>
                            <Text style={[CartStyles.itemPrice, { color: secondaryColor }]}>${item.price}</Text>
                            <View style={CartStyles.detailCartWrapper}>
                                <MaterialIcon style={CartStyles.addRemoveIcon} onPress={() => this.onChange('decrement', index)} name="remove" />
                                <Text style={CartStyles.quantityCart}>{item.quantity} </Text>
                                <MaterialIcon style={CartStyles.addRemoveIcon} onPress={() => this.onChange('increment', index)} name="add" />
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        let { secondaryColor } = this.props;
        let { cart, total } = this.props.cartData;
        updatewishList = cart ? cart.concat([]) : [];
        return (
            <View style={CartStyles.container}>
                {
                    cart.length ?
                        <View style={CartStyles.container}>
                            <View style={CartStyles.listWrapper}>
                                <FlatList
                                    data={updatewishList}
                                    renderItem={this.productList.bind(this)}
                                    keyExtractor={(item, index) => { return index.toString() }} />
                            </View>

                            <View style={CartStyles.totalContainer}>
                                <View style={CartStyles.total}>
                                    <Text style={CartStyles.itemTotalText}>
                                        {i18n.t('CartScreen.TOTAL')}
                                    </Text>
                                    <Text style={[CartStyles.itemTotal, { color: secondaryColor }]}>
                                        ${total}
                                    </Text>
                                </View>
                                <TouchableOpacity style={CartStyles.checkoutContainer} onPress={() => this.props.navigation.navigate("Checkout")} activeOpacity={0.5}>
                                    <View style={[CartStyles.cartBox, { backgroundColor: this.props.primaryColor }]}>
                                        <MaterialIcon style={CartStyles.checkoutCartIcon} name="shopping-cart" />
                                    </View>
                                    <View style={[CartStyles.btnClickContain, { backgroundColor: secondaryColor }]}>
                                        <Text style={CartStyles.btnText}>{i18n.t('CartScreen.CHECKOUT')}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        :
                        <EmptyDataScreen
                            message={i18n.t('EmptyCart_Screen.EMPTY_MSG')}
                            buttonText={i18n.t('EmptyCart_Screen.BTN_TEXT')}
                            icon='cart'
                            onPress={() => this.props.navigation.navigate('ShopNew')} />
                }
                {!this.props.getProfile ?
                    <View style={CartStyles.loadingView}>
                        <SpinKit />
                        <Text style={CartStyles.loadingText}>Please Wait While Fetching Profile Details...</Text>
                    </View> : null
                }
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        cartData: state.cartData,
        increment: state.cartData.counter,
        decrement: state.cartData.counter,
        getProfile: state.userProfileData.getProfile,
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor
    }
}

function mapDispatchToProps(dispatch) {
    return {
        IncrementItem: (data) => dispatch(increment(data)),
        DecrementItem: (data) => dispatch(decrement(data)),
        updateCart: (cart) => dispatch(updateCart(cart)),
        updateTotal: (total) => dispatch(updateTotal(total))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);