import { StyleSheet, Dimensions } from 'react-native';
import StyleVars from '../../Style/StyleVars';
const { height } = Dimensions.get("window");
import { Header } from 'react-navigation';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        position: 'relative'
    },
    listWrapper: {
        paddingTop: 10,
        flex: 1
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 2,
        padding: 10,
        marginBottom: 15,
        marginHorizontal: 10
    },
    imageContainer: {
        width: 70
    },
    detailContainer: {
        flex: 1,
        flexDirection: 'row'
    },
    detail: {
        flex: 1
    },
    actionContainer: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 10
    },
    deleteIcons: {
        alignItems: 'flex-end',
        marginTop: -3
    },
    itemTitle: {
        fontSize: 16,
        lineHeight: 18,
        fontFamily: 'Lato-Regular',
        color: '#2a2a2a',
        paddingBottom: 8,
        flex: 1,
        paddingRight: 12,
        height: 43,
        textAlign: 'left'
    },
    itemPrice: {
        fontSize: 20,
        color: '#1a5086',
        fontWeight: '600',
        fontFamily: 'Lato-Bold',
        fontStyle: 'italic'
    },
    itemImage: {
        height: 70,
        width: 70
    },
    itemsContainer: {
        flex: 1,
        marginHorizontal: 10,
        marginTop: 15
    },
    totalContainer: {
        flexDirection: 'row',
        backgroundColor: '#f4f4f4',
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    total: {
        width: '50%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    itemTotal: {
        color: '#1a5086',
        fontSize: 20,
        fontFamily: 'Lato-Bold',
        marginLeft: 5
    },
    itemTotalText: {
        color: '#2a2a2a',
        fontFamily: 'Lato-Bold',
        fontSize: 16
    },
    checkoutContainer: {
        width: '50%',
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    btnClickContain: {
        backgroundColor: '#1a5086',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        padding: 10,
        justifyContent: 'center'
    },
    btnText: {
        fontSize: 18,
        color: StyleVars.Colors.white,
        paddingHorizontal: 5,
        fontFamily: 'Lato-Regular'
    },
    detailCartContainer: {
        flexDirection: 'row',
        marginLeft: 10
    },
    detailCartWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    heartIcon: {
        fontSize: 18,
        color: '#2f2f2f',
        marginTop: 8,
        marginLeft: 15
    },
    addRemoveIcon: {
        color: '#2f2f2f',
        fontSize: 16,
        marginTop: 8
    },
    quantityCart: {
        paddingTop: 5,
        paddingBottom: 3,
        paddingHorizontal: 15,
        backgroundColor: '#f9f9f9',
        fontSize: 16,
        color: '#2f2f2f',
        fontFamily: 'Lato-Regular',
        marginHorizontal: 10
    },
    cartBox: {
        backgroundColor: '#ff8400',
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        paddingHorizontal: 10,
        justifyContent: 'center'
    },
    checkoutCartIcon: {
        fontSize: 20,
        color: '#fff'
    },
    wishListContainer: {
        marginHorizontal: 10,
        marginTop: 15
    },

    wishListBox: {
        marginHorizontal: 5,
        marginBottom: 15,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        padding: 15,
        elevation: 2,
        flexDirection: 'row',
        flex: 1
    },
    wishListImageBox: {
        width: 80
    },
    wishListImage: {
        width: 70,
        height: 70
    },
    wishListContent: {
        flexDirection: 'row',
        flex: 1
    },
    wishListWrapper: {
        flex: 1
    },
    materialDetailsContainer: {
        flex: 1,
        flexDirection: 'row'
    },
    materialDetails: {
        width: '93%',
        paddingRight: 10,
        color: '#2a2a2a',
        fontSize: 14,
        marginBottom: 5
    },
    cancelIcon: {
        fontSize: 20,
        color: '#3a3a3a',
        marginTop: -3
    },
    materialPriceContainer: {
        flexDirection: 'row'
    },
    materialPrice: {
        color: '#1a5086',
        fontSize: 20,
        fontWeight: 'bold',
        marginRight: 15
    },
    addCartButton: {
        backgroundColor: '#ff8400',
        justifyContent: 'center',
        paddingHorizontal: 15,
        borderRadius: 5
    },
    addCartButtonText: {
        color: '#fff',
        fontSize: 14
    },
    IcrementContainer: {
        flexDirection: 'row',
        flex: 1,
        position: 'absolute',
        right: 0,
        marginVertical: 5
    },
    Increment: {
        flexDirection: 'row'
    },
    IncrementLogo: {
        height: 22,
        width: 34,
        backgroundColor: '#f2f2f2',
        justifyContent: 'center'
    },
    IncrementLogoText: {
        textAlign: 'center'
    },
    loadingView: {
        flexDirection: 'column',
        position: 'absolute',
        top: -Header.HEIGHT,
        left: 0,
        height: height,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    loadingText: {
        color: 'white',
        position: 'absolute',
        paddingTop: 70,
        fontSize: 18,
        fontFamily: 'Lato-Regular',
        fontStyle: 'italic'
    }

});




