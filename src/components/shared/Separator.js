import React from 'react';
import { View, StyleSheet } from 'react-native';

const Separator = () => <View style={styles.separator} />;

const styles = StyleSheet.create({
    separator: {
        backgroundColor: '#b7b8b9',
        flex: 1,
        height: StyleSheet.hairlineWidth
    }
});

export default Separator;