import React, { Component } from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const AppButton = ({ onPress, children, bgColor, disabled }) => (
    <TouchableOpacity
        onPress={onPress}
        disabled={disabled}
        style={[styles.button, bgColor && { backgroundColor: bgColor }, disabled && styles.disabledButton]}
        activeOpacity={0.5}>
        <Text style={[styles.buttonText, disabled && styles.disabledButtonText]}>{children}</Text>
    </TouchableOpacity>
);

const BlockButton = ({ onPress, children, bgColor }) => (
    <TouchableOpacity
        onPress={onPress}
        style={[styles.button, styles.blockButton, { backgroundColor: bgColor }]}
        activeOpacity={0.5}>
        <Text style={styles.buttonText}>{children}</Text>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#ff8400',
        paddingHorizontal: 20,
        paddingVertical: 8,
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 2
    },
    blockButton: {
        width: '100%'
    },
    disabledButton: {
        backgroundColor: '#ddd'
    },
    disabledButtonText: {
        color: '#eee'
    },
    buttonText: {
        color: '#fff',
        fontSize: 16,
        fontFamily: 'Lato-Regular',
        textAlign: 'center'
    }
});

export { AppButton, BlockButton };