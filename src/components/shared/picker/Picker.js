import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, TouchableOpacity, Picker, Modal, PickerIOS, Dimensions, TouchableWithoutFeedback, I18nManager } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

////////////////////    Android Picker   ///////////////////////
const PickerAndroid = ({ itemsArray, onValueChange, selectedValue }) => {

    let items = itemsArray.map((item, i) => <Picker.Item label={item.label} color="#3b3b3b" size={14} style={{ fontFamily: 'Lato-Regular' }} value={item.value} key={i} />);

    return (
        <Picker
            onValueChange={onValueChange}
            style={{ backgroundColor: "transparent", height: 40 }}
            selectedValue={selectedValue}
            itemStyle={{ fontFamily: 'Lato-Regular' }}>
            {items}
        </Picker>
    )
};
////////////////////    Android Picker   ///////////////////////


////////////////////    IOS Picker   ///////////////////////
const PickerItemIOS = PickerIOS.Item;

const SCREEN_WIDTH = Dimensions.get('window').width;

const BORDER_RADIUS = 13;
const BACKGROUND_COLOR = 'white';
const BORDER_COLOR = '#d5d5d5';
const TITLE_FONT_SIZE = 13;
const TITLE_COLOR = '#8f8f8f';
const BUTTON_FONT_WEIGHT = 'normal';
const BUTTON_FONT_COLOR = '#007ff9';
const BUTTON_FONT_SIZE = 20;



const PickerIOSStyles = StyleSheet.create({
    basicContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)'
    },
    overlayContainer: {
        flex: 1,
        width: SCREEN_WIDTH

    },
    modalContainer: {
        width: SCREEN_WIDTH,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        backgroundColor: 'transparent'
    },
    bottomPicker: {
        width: SCREEN_WIDTH - 45,
        marginHorizontal: 10
    },
    pickerBtn: {
        position: 'relative',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadada'
    },
    pickerBtnText: {
        fontSize: 14,
        fontFamily: 'Lato-Regular',
        color: '#3b3b3b',
        lineHeight: 22,
        flex: 1,
        textAlign: 'left'
    },
    iconCaretDown: {
        marginLeft: 10,
        // backgroundColor: 'red'
    },
    contentContainer: {
        justifyContent: 'flex-end',
        margin: 10
    },
    datepickerContainer: {
        backgroundColor: BACKGROUND_COLOR,
        borderRadius: BORDER_RADIUS,
        marginBottom: 8,
        overflow: 'hidden'
    },
    titleContainer: {
        borderBottomColor: BORDER_COLOR,
        borderBottomWidth: StyleSheet.hairlineWidth,
        padding: 14,
        backgroundColor: 'transparent'
    },
    title: {
        textAlign: 'center',
        color: TITLE_COLOR,
        fontSize: TITLE_FONT_SIZE
    },
    confirmButton: {
        borderColor: BORDER_COLOR,
        borderTopWidth: StyleSheet.hairlineWidth,
        backgroundColor: 'transparent',
        height: 57,
        justifyContent: 'center'
    },
    confirmText: {
        textAlign: 'center',
        color: BUTTON_FONT_COLOR,
        fontSize: BUTTON_FONT_SIZE,
        fontWeight: BUTTON_FONT_WEIGHT,
        backgroundColor: 'transparent'
    },
    cancelButtonContainer: {
        paddingHorizontal: 10,
        paddingBottom: 10,
        width: '100%'
    },
    cancelButton: {
        backgroundColor: BACKGROUND_COLOR,
        borderRadius: BORDER_RADIUS,
        height: 57,
        justifyContent: 'center',
        width: '100%'
    },
    cancelText: {
        padding: 10,
        textAlign: 'center',
        color: BUTTON_FONT_COLOR,
        fontSize: BUTTON_FONT_SIZE,
        fontWeight: '600',
        backgroundColor: 'transparent'
    }
});


class PickerForIOS extends Component {
    constructor(props) {
        super(props);

        const selected = props.options.indexOf(props.selectedValue) || 0;

        this.state = {
            modalVisible: false,
            selectedOption: props.options[selected],
            selectedLabel: props.labels[selected]
        };

        this.onPressCancel = this.onPressCancel.bind(this);
        this.onPressSubmit = this.onPressSubmit.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onOverlayDismiss = this.onOverlayDismiss.bind(this);

        if ('buttonColor' in props) {
            console.warn('buttonColor as a prop is deprecated, please use buttonStyle instead.');
        }
    }

    componentWillReceiveProps(props) {
        // If options are changing, and our current selected option is not part of
        // the new options, update it.
        if (
            props.options
            && props.options.length > 0
            && props.options.indexOf(this.state.selectedOption) === -1
        ) {
            const previousOption = this.state.selectedOption;
            this.setState({
                selectedOption: props.options[0],
                selectedLabel: props.labels[0]
            }, () => {
                // Options array changed and the previously selected option is not present anymore.
                // Should call onSubmit function to tell parent to handle the change too.
                if (previousOption) {
                    this.onPressSubmit();
                }
            });
        }
    }

    onPressCancel() {
        const selected = this.props.options.indexOf(this.props.selectedValue) || 0;
        this.setState({
            selectedOption: this.props.options[selected],
            selectedLabel: this.props.labels[selected]
        });
        this.hide();
    }

    onPressSubmit() {
        if (this.props.onSubmit) {
            this.props.onSubmit(this.state.selectedOption, this.state.selectedLabel);
        }

        this.hide();
    }

    onOverlayDismiss() {
        this.hide();
    }

    onValueChange(option, index) {
        this.setState({
            selectedOption: option,
            selectedLabel: this.props.labels[index]
        });
    }

    show() {
        this.setState({
            modalVisible: true
        });
    }

    hide() {
        this.setState({
            modalVisible: false
        });
    }

    renderItem(option, index) {
        const label = (this.props.labels) ? this.props.labels[index] : option;

        return (
            <PickerItemIOS
                key={index}
                value={option}
                label={label}
            />
        );
    }

    render() {
        const { modalVisible, selectedOption, selectedLabel } = this.state;
        const {
            options,
            buttonStyle,
            itemStyle,
            cancelText,
            confirmText,
            disableOverlay,
        } = this.props;

        return (
            <Modal animationType={'fade'} transparent visible={modalVisible}>
                <View style={PickerIOSStyles.basicContainer}>
                    {!disableOverlay &&
                        <View style={PickerIOSStyles.overlayContainer}>
                            <TouchableWithoutFeedback onPress={this.onOverlayDismiss}>
                                <View style={PickerIOSStyles.overlayContainer} />
                            </TouchableWithoutFeedback>
                        </View>
                    }

                    <View style={PickerIOSStyles.datepickerContainer}>
                        <PickerIOS
                            ref={'picker'}
                            style={PickerIOSStyles.bottomPicker}
                            selectedValue={selectedOption}
                            onValueChange={(option, index) => this.onValueChange(option, index)}
                            itemStyle={itemStyle}>
                            {options.map((option, index) => this.renderItem(option, index))}
                        </PickerIOS>
                        <TouchableOpacity onPress={this.onPressSubmit} style={PickerIOSStyles.confirmButton}>
                            <Text style={[buttonStyle, PickerIOSStyles.confirmText]}>
                                {confirmText || 'Confirm'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={PickerIOSStyles.cancelButtonContainer}>
                        <TouchableOpacity onPress={this.onPressCancel} style={PickerIOSStyles.cancelButton}>
                            <Text style={[buttonStyle, PickerIOSStyles.cancelText]}>
                                {cancelText || 'Cancel'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
}
////////////////////    IOS Picker   ///////////////////////


export default class CustomPicker extends Component {

    constructor(props) {
        super(props);
        this.state = {
            options: props.itemsArray.map((obj) => obj.value),
            labels: props.itemsArray.map((obj) => obj.label)
        };
        props.onValueChange(props.selectedValue == 'undefined' ? props.itemsArray[0].value : props.selectedValue);
    }

    showLabel(value) {
        let label = '';
        for (let i = 0; i < this.props.itemsArray.length; i++) {
            if (this.props.itemsArray[i].value == value) {
                label = this.props.itemsArray[i].label;
                break;
            }
        }
        return <Text style={[PickerIOSStyles.pickerBtnText, this.props.IOSBtnText]}>{label}</Text>
    }

    renderPickerIOS() {
        let { itemsArray } = this.props;
        let itemValue = itemsArray.map((obj) => obj.value);
        let label = itemsArray.map((obj) => obj.label);
        return (
            <View style={this.props.container}>
                <TouchableOpacity onPress={() => this.refs[this.props.pickerRef].show()} style={[PickerIOSStyles.pickerBtn, this.props.IOSBtnStyle]}>
                    {this.showLabel(this.props.selectedValue)}
                    <Icon name={'ios-arrow-down'} color="#595959" size={24} style={[PickerIOSStyles.iconCaretDown, this.props.IOSBtnCaret]} />
                </TouchableOpacity>

                <PickerForIOS
                    ref={this.props.pickerRef}
                    options={itemValue}
                    labels={label}
                    selectedValue={this.props.selectedValue}
                    onSubmit={(val) => {
                        this.props.onValueChange(val);
                    }}
                />
            </View>
        )
    }

    renderPickerAndroid() {
        return (
            <View style={[PickerAndroidStyles.container, this.props.pickerContainer]}>
                <Icon name={'ios-arrow-down'} color="#595959" size={this.props.size || 24} style={[PickerAndroidStyles.iconCaretDown, this.props.caretStyle]} />
                <PickerAndroid itemsArray={this.props.itemsArray} selectedValue={this.props.selectedValue}
                    onValueChange={(val) => this.props.onValueChange(val)} />
            </View>
        )
    }

    render() {
        return Platform.OS === 'ios' ? this.renderPickerIOS() : this.renderPickerAndroid();
    }
}

const PickerAndroidStyles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: 'rgba(0, 0, 0, .38)',
        marginLeft: 0
    },
    iconCaretDown: {
        position: 'absolute',
        top: Platform.OS === 'android' ? 9 : 3,
        right: I18nManager.isRTL ? null : 5,
        left: I18nManager.isRTL ? 5 : null
    }
});