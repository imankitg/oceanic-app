import React from 'react';
import { View, Platform, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import SpinKit from 'react-native-spinkit';

class CustomSpinner extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <SpinKit type='Wave' color={this.props.primaryColor} size={30} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: Platform.OS == 'ios' ? 15 : 0, 
        alignItems: 'center', 
        justifyContent: 'center', 
        flex: 1
    }
});

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor
    }
}


export default connect(mapStateToProps)(CustomSpinner);