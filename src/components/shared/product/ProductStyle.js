import React, { StyleSheet, Dimensions, Platform } from "react-native";
const { width, height } = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "cards": {
        "position": "relative",
        "backgroundColor": "#fff",
        "alignItems": "center",
        "justifyContent": "center",
        "marginTop": 5,
        "marginBottom": 5,
        "shadowColor": "#000",
        "borderRadius": 2
    },
    "productItem": {
        "width": width - 30,
        "height": (width - 20) * 962 / 875,
        "marginTop": 5,
        "marginRight": 5,
        "marginBottom": 5,
        "marginLeft": 5
    },
    "productName": {
        "marginTop": 5,
        "marginRight": 5,
        "marginBottom": 5,
        "marginLeft": 5,
        "paddingLeft": 1
    },
    "discountPrice": {
        "color": "#535353",
        "paddingLeft": 5,
        "fontSize": 12,
        "fontWeight": "600",
        "marginLeft": 1,
        "marginRight": 1,
        "paddingBottom": 8
    },
    container: {
        flex: 1,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    imageContainer: {
        width: 80,
        marginRight: 10
    },
    detailContainer: {
        flex: 1,
        flexWrap: 'wrap'
    },
    title: {
        fontSize: 16,
        fontFamily: 'Lato-Regular',
        color: '#2a2a2a',
        lineHeight: 16,
        textAlign: 'left'
    },
    price: {
        fontSize: 22,
        color: '#1a5086',
        fontFamily: 'Lato-Bold',
        fontWeight: 'bold',
        marginRight: 8,
        textAlign: 'left'
    },
    photo: {
        height: 80,
        width: 80
    },
    ratingStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ratingNumbers: {
        fontSize: 18,
        fontFamily: 'Lato-Regular',
        marginLeft: 10,
        color: '#2a2a2a'
    },
    productPriceContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    previousPriceBox: {
        position: 'relative'
    },
    regularPrice: {
        fontSize: 18,
        fontFamily: 'Lato-Regular',
        fontWeight: Platform.OS ? '400' : '100',
        fontStyle: 'italic',
        color: '#999'
    },
    previousBorder: {
        position: 'absolute',
        top: 13,
        width: '100%',
        height: 1,
        backgroundColor: '#999'
    }
});