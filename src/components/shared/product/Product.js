'use strict';

import React, { Component } from "react";
import { Text, View } from "react-native";
import ProductStyle from "./ProductStyle";
import RatingStars from '../../FeedBack/RatingStars';
import ProgressBar from 'react-native-progress/Circle';
import Api from '../../../services/ProductService';
import ImageProgress from 'react-native-image-progress';
import { connect } from 'react-redux';

class Product extends Component {

    render() {
        let { product, secondaryColor } = this.props;
        let dataVariationsLength = product.variationsData && product.variationsData.length - 1;
        return (
            <View style={ProductStyle.container}>
                <View style={ProductStyle.imageContainer}>
                    <ImageProgress
                        style={ProductStyle.photo}
                        resizeMethod={'resize'}
                        source={{ uri: Api.convertingURL(product.images[0].src) }}
                        indicator={ProgressBar} size={30} />
                </View>
                <View style={ProductStyle.detailContainer}>
                    <Text style={ProductStyle.title} ellipsizeMode="tail" numberOfLines={1}>{Api.convertingLanguage(product.name)}</Text>
                    {
                        product.variationsData && product.variationsData.length ?
                            <View>
                                <Text ellipsizeMode="tail" style={[ProductStyle.price, { color: secondaryColor }]} numberOfLines={1}>{`$${product.price}-$${product.variationsData[dataVariationsLength].price}`}</Text>
                            </View> :
                            <View style={ProductStyle.productPriceContainer}>
                                <Text style={[ProductStyle.price, { color: secondaryColor }]}>${product.price}</Text>
                                <View style={ProductStyle.previousPriceBox}>
                                    <Text style={ProductStyle.regularPrice}>{product.regular_price ? `$${product.regular_price}` : null}</Text>
                                    <Text style={ProductStyle.previousBorder} />
                                </View>
                            </View>
                    }
                    <View style={ProductStyle.ratingStyle} pointerEvents="none">
                        <RatingStars defaultRating={product.average_rating} size={16} />
                        <Text style={ProductStyle.ratingNumbers}>({Math.ceil(product.average_rating)})</Text>
                    </View>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        secondaryColor: state.productsData.secondaryColor
    }
}

export default connect(mapStateToProps)(Product);