import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

const Footer = ({ topBorder, primaryColor }) => (
    <View style={[styles.container, topBorder && styles.topBorder]}>
        <ActivityIndicator color={primaryColor} animating size="large" />
    </View>
);

const styles = StyleSheet.create({
    container: {
        paddingVertical: 20
    },
    topBorder: {
        borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: "#b7b8b9"
    }
});

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor
    }
}

export default connect(mapStateToProps)(Footer);