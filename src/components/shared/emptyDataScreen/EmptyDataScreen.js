import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import ViewContainer from '../viewContainer/ViewContainer';
import { BlockButton } from '../buttons/AppButton';

let { width } = Dimensions.get('window');
import { connect } from 'react-redux';

const EmptyDataScreen = ({ message, icon, buttonText, onPress, headingText, primaryColor, secondaryColor }) => {
    let imagePath;
    switch (icon) {
        case 'cart':
            imagePath = require(`../../../assets/images/cart.png`);
            break;
        case 'wishlist':
            imagePath = require(`../../../assets/images/wishlist.png`);
            break;
        case 'filter':
            imagePath = require(`../../../assets/images/filter.png`);
            break;
        case 'on_internet':
            imagePath = require(`../../../assets/images/no-internet-icon.png`)
    }
    return (
        <ViewContainer>
            <View style={styles.contentContainer}>
                {headingText && <Text style={styles.errorMessage}>{headingText}</Text>}
                <Text style={styles.messageText}>{message}</Text>
                <Image source={imagePath} style={styles.image} tintColor={secondaryColor} resizeMethod={'resize'} />
            </View>
            {buttonText ?
                <View style={styles.buttonContainer}>
                    <BlockButton onPress={onPress} bgColor={primaryColor}>{buttonText}</BlockButton>
                </View>
                : null
            }
        </ViewContainer>
    )
};

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f2f2f2'
    },
    messageText: {
        width: '80%',
        textAlign: 'center',
        fontSize: 18,
        marginBottom: 30,
        fontFamily: 'Lato-Regular'
    },
    errorMessage: {
        width: '80%',
        textAlign: 'center',
        fontSize: 30,
        color: '#ff8400',
        marginBottom: 10,
        fontFamily: 'Lato-Bold'
    },
    image: {
        width: width / 2,
        height: width / 2,
        resizeMode: 'contain'
    },
    buttonContainer: {
        paddingHorizontal: 25,
        paddingBottom: 20,
        backgroundColor: '#f2f2f2'
    }
});

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor
    }
}

export default connect(mapStateToProps)(EmptyDataScreen);