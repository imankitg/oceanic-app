import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const HeaderBar = ({ title }) => (
    <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
    </View>
);

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#e1e1e1'
    },
    title: {
        fontSize: 20,
        color: '#2a2a2a',
        padding: 15,
        fontFamily: 'Lato-Bold'
    }
});

export default HeaderBar;