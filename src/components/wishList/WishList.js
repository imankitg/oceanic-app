import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Alert, AsyncStorage, FlatList, ToastAndroid, Platform } from 'react-native';
import WishListStyle from './WishListStyle';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import EmptyDataScreen from '../shared/emptyDataScreen/EmptyDataScreen';
import i18n from '../../../i18n';
import Api from '../../services/ProductService'

// redux
import { connect } from 'react-redux';
import { updateCart, updateWishListCount, updateWishList } from '../../actions/Cart';

class WishList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            position: 1,
            interval: null,
            wishList: [],
            isEmpty: null,
            cartList: [],
            isSelected: false
        };
    }

    componentDidMount() {
        this.getWishList()
    }

    componentWillMount() {
        this.props.navigation.setParams({ handleSave: this.removeFromCart })
    }

    deleteWishListItem = async (index) => {
        const { wishList } = this.state;
        let { params } = this.props.navigation.state;
        params && params.check ? params.check() : null;
        wishList.splice(index, 1);
        let updatedValue = this.props.wishListCount - 1;
        this.props.minusWishListCount(updatedValue);
        this.props.update_wish_list(wishList);
        this.setState({ wishList });
        AsyncStorage.setItem("user", JSON.stringify(wishList));
        Platform.OS === 'ios' ? Alert.alert(i18n.t('WishList_Screen.MESSAGE'),  i18n.t('WishList_Screen.ITEM_REMOVE_MSG')) : ToastAndroid.showWithGravityAndOffset(i18n.t('WishList_Screen.ITEM_REMOVE_MSG'), ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
    };

    removeFromCart = (id) => {
        const { wishList } = this.state;
        let copy = wishList.slice();
        for (var key in copy) {
            if (copy[key].id === id) {
                copy[key].add = false
            }
        }
        this.setState({ wishList: copy })
    };

    addToCart = (index) => {
        const { wishList, cartList } = this.state;
        const { cartData, addToCart } = this.props;
        let copy = wishList.slice();
        copy[index].quantity = 1;
        copy[index].total_price = 0;
        copy[index].add = true;
        cartData.length !== 0 ? cartData.push(copy[index]) : cartList.push(copy[index]);
        cartData.length !== 0 ? addToCart(cartData) : addToCart(cartList);
        this.setState({ wishList: copy })
    };

    getWishList = () => {
        this.setState({
            wishList: this.props.wishListData
        })
    };

    wishListProducts = ({ item, index }) => {
        let { primaryColor, secondaryColor } = this.props
        return (
            <View key={index} style={WishListStyle.wishListContainer} >
                <View style={WishListStyle.wishListImageBox}>
                    <Image style={WishListStyle.wishListImage} source={{ uri: item.images[0].src }} resizeMethod={'resize'} />
                </View>
                <View style={WishListStyle.wishListContent}>
                    <View style={WishListStyle.wishListWrapper}>
                        <View style={WishListStyle.materialDetailsContainer}>
                            <Text style={WishListStyle.materialDetails} ellipsizeMode="tail" numberOfLines={2}>{Api.convertingLanguage(item.name)}</Text>
                            <MaterialIcon style={WishListStyle.cancelIcon}
                                onPress={() => this.deleteWishListItem(index)} name="close" />
                        </View>
                        <View style={WishListStyle.materialPriceContainer}>
                            <Text style={[WishListStyle.materialPrice, { color: secondaryColor }]}>${item.sale_price}</Text>
                            <TouchableOpacity
                                onPress={() => this.addToCart(index)}
                                disabled={item.add && item.add ? true : false}
                                activeOpacity={0.7}
                                style={[WishListStyle.addCartButton, { backgroundColor: primaryColor }]}>
                                <Text style={WishListStyle.addCartButtonText}>{item.add && item.add ? i18n.t('WishList_Screen.ADDED_TO_CART') : i18n.t('WishList_Screen.ADD_TO_CART')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    };

    render() {
        let { wishListData } = this.props;
        updatewishList = wishListData ? wishListData.concat([]) : [];
        return (
            <ViewContainer style={WishListStyle.container}>
                {updatewishList.length ?
                    <FlatList
                        data={updatewishList}
                        renderItem={this.wishListProducts.bind(this)}
                        keyExtractor={(item, index) => { return index.toString() }}
                    />
                    :
                    <EmptyDataScreen
                        message={i18n.t('EmptyCart_Screen.EMPTY_MSG')}
                        buttonText={i18n.t('EmptyCart_Screen.BTN_TEXT')}
                        icon='wishlist'
                        onPress={() => this.props.navigation.navigate('ShopNew')} />
                }
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        cartData: state.cartData.cart,
        wishListCount: state.cartData.wishListCount,
        wishListData: state.cartData.wishListData,
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor
    }
}

function mapDispatchToProps(dispatch) {
    return {
        minusWishListCount: (data) => dispatch(updateWishListCount(data)),
        addToCart: (addToCartArray) => dispatch(updateCart(addToCartArray)),
        update_wish_list: (data) => dispatch(updateWishList(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WishList)                                                                                                   