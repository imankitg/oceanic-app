import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#f2f2f2'
    },
    wishListContainer: {
        marginTop: 10,
        marginHorizontal: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 2,
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    wishListWrapper: {
        marginLeft: 20,
        flex: 1
    },
    materialDetailsContainer: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row'
    },
    materialPriceContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    wishListImageBox: {
        width: 65
    },
    wishListImage: {
        width: 75,
        height: 75
    },
    wishListContent: {
        flexDirection: 'row',
        flex: 1
    },
    materialDetails: {
        flex: 1,
        fontFamily: 'Lato-Regular',
        paddingRight: 10,
        color: '#2a2a2a',
        fontSize: 16,
        marginBottom: 5,
        lineHeight: 18,
        height: 37,
        textAlign: 'left'
    },
    materialPrice: {
        color: '#1a5086',
        fontSize: 22,
        fontWeight: '600',
        fontFamily: 'Lato-Regular',
        fontStyle: 'italic',
        marginRight: 15
    },
    addCartButton: {
        backgroundColor: '#ff8400',
        justifyContent: 'center',
        paddingHorizontal: 10,
        borderRadius: 3,
        height: 18,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 2
    },
    addCartButtonText: {
        color: '#fff',
        fontFamily: 'Lato-Regular',
        fontSize: 12
    },
    cancelIcon: {
        fontSize: 20,
        color: '#b1b1b1',
        marginTop: -5
    },
    activityView: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    activityText: {
        fontSize: 20,
        fontFamily: 'Lato-Regular'
    }
})