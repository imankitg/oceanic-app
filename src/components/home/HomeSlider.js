import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import HomeStyle from './HomeStyle';
import SpinKit from '../shared/SpinKit/SpinKit';
import ViewContainer from '../../components/shared/viewContainer/ViewContainer';
import Swiper from 'react-native-swiper';
import ImageProgress from 'react-native-image-progress';
import Api from '../../services/ProductService';
import ProgressBar from 'react-native-progress/Circle';
import { connect } from 'react-redux';
import i18n, {I18n} from '../../../i18n';

class HomeSlider extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstTimeLoaded: false,
            images: props.images || []
        }
    }

    componentWillReceiveProps({ images }) {
        this.setState({ images, firstTimeLoaded: true });
    }

    render() {
        let { firstTimeLoaded, images } = this.state;
        return (
            <ViewContainer>
                {firstTimeLoaded ?
                    images.length ?
                        <Swiper
                            style={HomeStyle.swiperWrapper}
                            autoplay={true}
                            dotColor='#fff'
                            paginationStyle={HomeStyle.paginationStyle}
                            activeDot={<View style={[HomeStyle.activeDots, { backgroundColor: this.props.primaryColor }]} />}
                        >
                            {images.map((data, index) => {
                                return (
                                    <TouchableOpacity key={index} onPress={() => this.props.navigation.navigate('ShopNew')}>
                                        <ImageProgress
                                            style={HomeStyle.swiperImage}
                                            source={{ uri: data }}
                                            indicator={ProgressBar} size={50} />
                                    </TouchableOpacity>
                                )
                            })
                            }
                        </Swiper>
                        :
                        <View style={HomeStyle.sliderView}>
                            <Text style={HomeStyle.sliderText}>{I18n('Common.NO_SLIDER_IMAGES')}...</Text>
                        </View> :
                    <View style={HomeStyle.sliderLoader}>
                        <SpinKit />
                    </View>
                }
            </ViewContainer>
        )
    }
}

function mapStateToProps(state) {
    return {
        primaryColor: state.productsData.primaryColor
    }
}

export default connect(mapStateToProps)(HomeSlider);
