import { StyleSheet, Dimensions, Platform } from 'react-native';
let { height, width } = Dimensions.get('screen');
import i18n from '../../../i18n';

var sliderHeight = height * 0.3448;

export default StyleSheet.create({
    sliderContainer: {
        height: sliderHeight,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 2
    },
    newArrivals: {
        fontSize: 20,
        color: '#1a5086',
        fontFamily: 'Lato-Bold',
        textAlign: i18n.locale == 'ar' ? 'right' : 'left'
    },
    arrivalsContainer: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        marginTop: 15,
        marginBottom: 15,
        justifyContent: 'space-between'
        // alignItems: 'center'
    },
    productWrapper: {
        width: width / 2.8,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.2,
        shadowRadius: 4,
        elevation: 2,
        marginHorizontal: 4,
        marginBottom: 10,
        marginTop: 5,
        backgroundColor: '#fff'
    },
    productImage: {
        height: 150,
        width: '100%'
    },
    productWrapperDetail: {
        paddingHorizontal: 5,
        paddingVertical: 10,
        alignItems: 'center',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    productTitle: {
        fontSize: 16,
        color: '#2a2a2a',
        textAlign: 'center',
        fontFamily: 'Lato-Regular',
        paddingVertical: 2
    },
    productPrice: {
        fontSize: 18,
        color: '#1a5086',
        fontWeight: '600',
        fontFamily: 'Lato-Bold',
        marginTop: 5
    },
    resortShirtsContainer: {
        paddingHorizontal: 11,
        paddingVertical: 5
    },
    resortShirtsWrapper: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 5,
        elevation: 2,
        paddingHorizontal: 0
    },
    resortImage: {
        width: '100%',
        height: 150,
        resizeMode: 'cover'
    },
    shopCategoriesContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 15,
        marginHorizontal: 15
    },
    categoryContainer: {
        marginHorizontal: 15,
        alignItems: 'center'
    },
    categoryWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        flex: 1
    },
    categoryContent: {
        width: '49%',
        height: 165,
        backgroundColor: '#fff',
        padding: 3,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 5,
        elevation: 2,
        borderRadius: 10,
        marginBottom: 8
    },
    categoryImageWrapper: {
        flex: 1,
        borderTopLeftRadius: Platform.OS === 'ios' ? 10 / 1.5 : 10,
        borderTopRightRadius: Platform.OS === 'ios' ? 10 / 1.5 : 10,
        overflow: 'hidden'
    },
    categoryImage: {
        width: '100%',
        flex: 1
    },
    categoryTitle: {
        color: '#2a2a2a',
        textAlign: 'center',
        fontSize: 18,
        fontFamily: 'Lato-Bold',
        paddingTop: 11,
        paddingBottom: 8,
        paddingHorizontal: 10
    },
    productSliderWrapper: {
        marginHorizontal: 11
    },
    bestSellingWrapper: {
        backgroundColor: '#fff',
        padding: 3,
        width: width / 2.4,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 5,
        elevation: 2,
        marginHorizontal: 4,
        marginBottom: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    sellingImage: {
        width: '100%',
        height: 164
    },
    sellingTitle: {
        fontSize: 16,
        fontFamily: 'Lato-Regular',
        textAlign: 'center',
        color: '#2a2a2a',
        paddingVertical: 7,
        paddingHorizontal: 5
    },
    mobileWrapper: {
        marginBottom: 15
    },
    sliderText: {
        color: 'white', 
        fontFamily: 'Lato-Regular', 
        fontWeight: '600', 
        fontStyle: 'italic'
    },
    sliderView: {
        height: sliderHeight, 
        backgroundColor: 'black', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    sliderLoader: {
        height: sliderHeight, 
        backgroundColor: 'black'
    },
    NoProductText: {
        fontFamily: 'Lato-Regular', 
        fontWeight: '600', 
        fontStyle: 'italic'
    },
    NoProductView: {
        justifyContent: 'center', 
        alignItems: 'center', 
        flex: 1 
    },
    swiperWrapper: {
        height: sliderHeight
    },
    swiperImage: {
        width: '100%',
        height: sliderHeight
    },
    activeDots: {
        backgroundColor: '#ff8400', 
        width: 14,
        height: 8,
        borderRadius: 4,
        margin: 3
    },
    paginationStyle: {
        backgroundColor: 'rgba(0,0,0,0.7)', 
        width: '100%', 
        height: 20, 
        bottom: 0
    } 
});