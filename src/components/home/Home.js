import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native';
import ViewContainer from '../../components/shared/viewContainer/ViewContainer';
import { AppButton } from '../../components/shared/buttons/AppButton';
import Api from '../../services/ProductService';
import HomeStyle from './HomeStyle';
import HomeSlider from './HomeSlider';
import ImageProgress from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Circle';
import SpinKit from '../shared/SpinKit/SpinKit';
import moment from 'moment';
import { newArival, getProductSuccess, fetchCategories } from '../../actions/ShopProducts';
import { changeColorTheme, firstTimeLoad } from '../../actions/Products';
import { setLanguageIntoRedux } from '../../actions/App'
import i18n, {I18n} from '../../../i18n';

// redux
import { connect } from 'react-redux';

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            allProducts: [],
            allCategories: [],
            array_id: [],
            images: [],
            payload: {
                per_page: 10,
                page: 1
            },
            firstTimefetchTopProducts: false,
            firstTimeLoadedBestProducts: false,
            firstTimeLoadedCategories: false,
            bestSellingProduct: [],
            disabledCategory: true,
            disabledArrivals: true
        };
    }
    async componentDidMount() {
        // require for best selling product
        let max_date = moment().toISOString().split('T');
        let lastMonth = moment().subtract(0, 'months').startOf('month');
        let min_date = lastMonth._d.toISOString().split('T');

        let promise = new Promise((resolve, reject) => {
            AsyncStorage.getItem('USER_LANGUAGE', (err, language) => {
                if(err) reject(err);
                else resolve(language)
            })
        });

        let language = await promise;
        this.props.setLanguageIntoRedux(language);
        Api.fetchTopProducts(this.state.payload, null, language).then(async (res) => {
            let variationProductsArray = [];
            let withoutVariationsArray = [];
            let allProducts = [];
            res.data.filter(data => {
                if (data.attributes.length) variationProductsArray.push(data);
                else withoutVariationsArray.push(data)
            });
            if (variationProductsArray.length) {
                for (let key in variationProductsArray) {
                    let res = await Api.fetchVariationProducts(variationProductsArray[key].id);
                    variationProductsArray[key].variationsData = res.data;
                }
                allProducts = [...withoutVariationsArray, ...variationProductsArray];
                this.props.newArival(allProducts);
                this.props.getProductSuccess(allProducts)
            }
            else {
                allProducts = withoutVariationsArray;
                this.props.newArival(allProducts);
                this.props.getProductSuccess(allProducts)
            }

            this.setState({ allProducts, disabledArrivals: false, firstTimefetchTopProducts: true })
        });

        Api.getPageWithSlug('slider').then(res => {
            let imageUrls = [];
            let data = res.data[0].acf.images;
            data.map((image) => {
                if (image) {
                    imageUrls.push(image.image)
                }
            });
            this.setState({ images: imageUrls })
        });

        Api.bestSellingProduct(max_date, min_date, language).then(res => {
            let array_id = [];
            let bestSellingProducts = res.data;
            for (var key in bestSellingProducts) {
                array_id.push(bestSellingProducts[key].product_id)
            }

            if (!bestSellingProducts.length) {
                this.setState({
                    bestSellingProduct: [],
                    firstTimeLoadedBestProducts: true
                })
            }

            if (bestSellingProducts.length) {
                Api.BestSellingProductsDetails(array_id, language).then(res => {
                    this.setState({
                        bestSellingProduct: res.data,
                        firstTimeLoadedBestProducts: true
                    })
                })
            }
        });
        this.props.fetchCategories(language)
    }

    productByCategory = (type, title) => {
        let { allProducts } = this.state;
        let categoryArray = [];
        for (var key in allProducts) {
            if (allProducts[key].categories[0].name === type) {
                categoryArray.push(allProducts[key])
            }
        }
        this.props.sortByCategory(categoryArray);
        this.props.navigation.navigate('ProductByCategory', { categoryTitle: title })
    };

    renderNewArrivals() {
        const { navigation, allProducts, secondaryColor } = this.props;
        return (
            <ScrollView horizontal={true} style={HomeStyle.productSliderWrapper} showsHorizontalScrollIndicator={false}>
                {allProducts.map((data, index) => {
                    let dataVariationsLength = data.variationsData && data.variationsData.length - 1;
                    return (
                        <View style={HomeStyle.productWrapper} key={index}>
                            <TouchableOpacity onPress={() => navigation.navigate('ShopNewDetail', { product: allProducts[index] })}>
                                <ImageProgress style={HomeStyle.productImage}
                                    source={{ uri: Api.convertingURL(data.images[0].src) }}
                                    indicator={ProgressBar} size={50} />
                                <View style={HomeStyle.productWrapperDetail}>
                                    <Text ellipsizeMode="tail" numberOfLines={3} style={HomeStyle.productTitle}>{Api.convertingLanguage(data.name)}</Text>
                                    {data.variationsData && data.variationsData.length ?
                                        <Text ellipsizeMode="tail" numberOfLines={1} style={[HomeStyle.productPrice, { color: secondaryColor }]}>{`$${data.price}-$${data.variationsData[dataVariationsLength].price}`}</Text>
                                        :
                                        <Text style={[HomeStyle.productPrice, { color: secondaryColor }]}>${data.sale_price}</Text>
                                    }
                                </View>
                            </TouchableOpacity>
                        </View>
                    )
                })}
            </ScrollView>
        )
    }

    renderCategories() {
        const { allCategories, categoryLoading } = this.props;
        let filterCategory = allCategories.filter(data => data.name != 'Uncategorized' && data.name != 'غير مصنف');
        const categories = filterCategory.slice(0, 4);
         let categoryThumbnail;
        return (
            <View style={HomeStyle.categoryWrapper}>
                {
                    !categoryLoading ?
                        filterCategory.length ?
                            categories.map((category, index) => {
                                categoryThumbnail = (category.image && category.image.src !== false) ? { uri: Api.convertingURL(category.image.src) } : require('../../assets/images/placeholder.png');
                                return (
                                    <TouchableOpacity
                                        key={index}
                                        onPress={() => this.props.navigation.navigate('ProductByCategory', { categoryTitle: category.name, id: category.id })}
                                        style={[HomeStyle.categoryContent]}>
                                        <View style={{ flex: 1 }}>
                                            <View style={HomeStyle.categoryImageWrapper}>
                                                <ImageProgress style={HomeStyle.categoryImage}
                                                    source={categoryThumbnail}
                                                    indicator={ProgressBar} size={50} />
                                            </View>
                                            <Text style={HomeStyle.categoryTitle} ellipsizeMode="tail" numberOfLines={1}>{category.name}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })
                            :
                            <View style={HomeStyle.NoProductView}><Text style={HomeStyle.NoProductText}>{I18n('Common.NO_CATEGORIES_FOUND')}!</Text></View> : <SpinKit />

                }
            </View>
        )
    };

    render() {
        { console.disableYellowBox = true }
        const { navigation, categoryLoading, allCategories, primaryColor, secondaryColor } = this.props;
        const { allProducts, images, bestSellingProduct, firstTimefetchTopProducts, firstTimeLoadedBestProducts } = this.state;
        return (
            <ViewContainer style={{ backgroundColor: '#f2f2f2' }}>
                <ScrollView>
                    <View style={HomeStyle.sliderContainer}>
                        <HomeSlider images={images} navigation={navigation} />
                    </View>
                    <View style={HomeStyle.arrivalsContainer}>
                        <Text style={[HomeStyle.newArrivals, { color: secondaryColor }]}>{i18n.t('Home_Screen.NEW_ARRIVALS')}</Text>
                    </View>
                    {firstTimefetchTopProducts ?
                        typeof (allProducts) !== 'string' && allProducts.length ?
                            this.renderNewArrivals()
                            :
                            <View style={HomeStyle.NoProductView}><Text style={HomeStyle.NoProductText}>{I18n('Common.NO_PRODUCTS_FOUND')}!</Text></View> : <SpinKit />
                    }
                    <View style={HomeStyle.resortShirtsContainer}>
                        <View style={HomeStyle.resortShirtsWrapper}>
                            <Image style={HomeStyle.resortImage} source={require('../../assets/images/slide2.jpg')} resizeMethod={'resize'} />
                        </View>
                    </View>

                    <View style={HomeStyle.shopCategoriesContainer}>
                        <Text style={[HomeStyle.newArrivals, { color: secondaryColor }]}>{i18n.t('Home_Screen.SHOP_BY_CATEGORY')}</Text>
                        <AppButton onPress={() => this.props.navigation.navigate('Categories', { allCategories })} disabled={categoryLoading} bgColor={primaryColor}>{i18n.t('Home_Screen.VIEW_ALL')}</AppButton>
                    </View>

                    <View style={HomeStyle.categoryContainer}>
                        {this.renderCategories()}
                    </View>

                    <View style={HomeStyle.shopCategoriesContainer}>
                        <Text style={[HomeStyle.newArrivals, { color: secondaryColor }]}>{i18n.t('Home_Screen.BEST_SELLING_PRODUCT')}</Text>
                    </View>
                    {
                        firstTimeLoadedBestProducts ?
                            bestSellingProduct.length ?
                                <ScrollView horizontal={true} style={HomeStyle.productSliderWrapper} showsHorizontalScrollIndicator={false}>
                                    {bestSellingProduct.map((data, index) => {
                                        return (
                                            <TouchableOpacity key={index} onPress={() => navigation.navigate('ShopNewDetail', { product: bestSellingProduct[index] })}>
                                                <View style={HomeStyle.bestSellingWrapper}>
                                                    <ImageProgress style={HomeStyle.sellingImage}
                                                        source={{ uri: Api.convertingURL(data.images[0].src) }}
                                                        indicator={ProgressBar} size={50} />
                                                    <Text ellipsizeMode="tail" numberOfLines={2} style={HomeStyle.sellingTitle}>{Api.convertingLanguage(data.name)}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    })}
                                </ScrollView>
                                :
                                <View style={HomeStyle.NoProductView}><Text style={HomeStyle.NoProductText}> !{i18n.t('Common.NO_BEST_SELLING_PRODUCT')}! </Text></View> : <SpinKit />
                    }

                    <View style={HomeStyle.resortShirtsContainer}>
                        <View style={[HomeStyle.resortShirtsWrapper, HomeStyle.mobileWrapper]}>
                            <Image style={HomeStyle.resortImage} source={require('../../assets/images/summer-slider.png')} resizeMethod={'resize'} />
                        </View>
                    </View>
                </ScrollView>
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        allProducts: state.shopData.allProducts,
        allCategories: state.shopData.categories,
        categoryLoading: state.shopData.categoryLoading,
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor
    }
}

function mapDispatchToProps(dispatch) {
    return {
        sortByCategory: (data) => dispatch(firstTimeLoad(data)),
        newArival: (data) => dispatch(newArival(data)),
        getProductSuccess: (data) => dispatch(getProductSuccess(data)),
        fetchCategories: (lang) => dispatch(fetchCategories(lang)),
        changeColorTheme: (color) => dispatch(changeColorTheme(color)),
        setLanguageIntoRedux: language => dispatch(setLanguageIntoRedux(language))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
