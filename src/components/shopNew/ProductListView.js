import React, { Component } from 'react';
import { TouchableOpacity, FlatList, RefreshControl, Dimensions, View } from 'react-native';
import Separator from "../../components/shared/Separator";
import Footer from "../../components/shared/Footer";
import Product from "../../components/shared/product/Product";
import ViewContainer from '../../components/shared/viewContainer/ViewContainer';
import EmptyDataScreen from '../shared/emptyDataScreen/EmptyDataScreen';
import i18n from '../../../i18n';

// redux
import { connect } from 'react-redux';

// Actions
import { fetchProducts, refreshAllProducts, filterApplied } from '../../actions/ShopProducts';

class ProductListView extends Component {

    constructor(props) {
        super(props);
    }

    product({ item, index }) {
        let { navigation, shopData } = this.props;
        return (
            <TouchableOpacity onPress={() => navigation.navigate('ShopNewDetail', { product: shopData.products[index], index: index })}>
                <Product product={item} />
            </TouchableOpacity>
        )
    }

    renderFooter = () => {
        if (!this.props.shopData.loadingData) return null;
        return (
            <Footer topBorder />
        );
    };

    emptyScreen = () => {
        const { height } = Dimensions.get("window");
        return (
            <View style={{ height: height / 1.195 }}>
                <EmptyDataScreen message={i18n.t('Common.NO_PRODUCTS_FOUND')} icon='filter' />
            </View>
        )
    };

    render() {
        let { shopData, refreshAllProducts, fetchProducts, filterApplied, language } = this.props;
        console.log('ProductListView language', language)
        return (
            <ViewContainer style={{ backgroundColor: "#f2f2f2" }}>
                <FlatList
                    data={shopData.products}
                    renderItem={this.product.bind(this)}
                    keyExtractor={(item, index) => { return index.toString() }}
                    ItemSeparatorComponent={Separator}
                    ListEmptyComponent={this.emptyScreen}
                    refreshControl={<RefreshControl
                        refreshing={shopData.isRefreshing}
                        onRefresh={() => refreshAllProducts(language)}
                        tintColor="#ff8400" />
                    }
                    onEndReached={() => shopData.isFilter ? filterApplied(shopData.payload, true, language) : fetchProducts(shopData.payload, isFilter = false, language)}
                    onEndReachedThreshold={0.1}
                    ListFooterComponent={this.renderFooter} />
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        shopData: state.shopData
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchProducts: (payload, check, lang) => dispatch(fetchProducts(payload, check, lang)),
        filterApplied: (payload, check, language) => dispatch(filterApplied(payload, check, language)),
        refreshAllProducts: (language) => dispatch(refreshAllProducts(language))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductListView)
