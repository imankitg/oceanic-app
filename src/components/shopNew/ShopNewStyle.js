import { StyleSheet, Platform, I18nManager } from 'react-native';

export default StyleSheet.create({
    tabsContainer: {
        flexDirection: 'row',
        marginBottom: 15
    },
    filterButton: {
        backgroundColor: '#ff8400',
        flex: 50,
        paddingVertical: 15,
        shadowColor: '#444',
        shadowOffset: { width: 0, height: 0.5 },
        shadowOpacity: 0.6,
        shadowRadius: 1,
        elevation: 2
    },
    listButton: {
        backgroundColor: '#1a5086'
    },
    filterListBtnText: {
        textAlign: 'center',
        fontSize: 16,
        color: '#fff',
        fontFamily: 'Lato-Bold',
        fontWeight: '100'
    },
    filterListText: {
        textAlign: 'center'
    },
    shopContainer: {
        paddingHorizontal: 10
    },
    shopDetails: {
        flex: 1,
        borderRadius: 5,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        elevation: 2,
        marginBottom: 10,
        marginHorizontal: 5,
        paddingBottom: 10,
        overflow: 'hidden'
    },
    shopDetailsBox: {
        marginLeft: 10
    },
    shopProductImageWrapper: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        overflow: 'hidden'
    },
    shopProduct: {
        width: '100%',
        height: 180
    },
    ratingContainer: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        marginVertical: 5,
        alignItems: 'center'
    },
    ratingIcon: {
        fontSize: 16,
        color: '#ff8400'
    },
    ratingNumbers: {
        fontSize: 18,
        fontFamily: 'Lato-Regular',
        marginLeft: 10,
        color: '#2a2a2a'
    },
    productDetailContainter: {
        paddingHorizontal: 10,
        height: 32,
        marginVertical: 2.5
    },
    productDetails: {
        fontSize: 16,
        fontFamily: 'Lato-Regular',
        color: '#2a2a2a',
        lineHeight: 16
    },
    productPriceContainer: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        alignItems: 'center'
    },
    price: {
        fontSize: 22,
        color: '#1a5086',
        fontFamily: 'Lato-Bold',
        fontWeight: 'bold',
        marginRight: 8
    },
    regularPrice: {
        fontSize: 18,
        fontFamily: 'Lato-Regular',
        fontWeight: Platform.OS ? '400': '100',
        fontStyle: 'italic',
        color: '#999'
    },
    favoriteIcon: {
        color: '#2a2a2a',
        fontSize: 18
    },
    activeFavoriteIcon: {
        color: '#ff8400'
    },
    previousPriceContainer: {
        flexDirection: 'row',
        position: 'relative'
    },
    previousPrice: {
        color: '#2a2a2a',
        fontSize: 16
    },
    previousPriceBorder: {
        position: 'absolute',
        top: 12,
        left: 0,
        width: '100%',
        height: 1,
        backgroundColor: '#2a2a2a'
    },
    saleContainer: {
        alignItems: 'center',
        backgroundColor: 'rgba(18, 59, 102, 0.9)',
        position: 'absolute',
        bottom: 0,
        right: 0,
        paddingVertical: 10,
        borderTopLeftRadius: 8,
        paddingHorizontal: 15
    },
    saleText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
    productDetailsContainer: {
        marginTop: 15,
        flex: 1
    },
    productDetailsWrapper: {
        flex: 1,
        paddingHorizontal: 15,
        marginLeft: I18nManager.isRTL ? (Platform.OS === 'ios' ? 30 : 0) : 0
    },
    productSelectedContainer: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        elevation: 2,
        marginBottom: 15
    },
    productSelected: {
        width: '100%',
        height: 275,
        borderRadius: 5
    },
    productSelectContainer: {
        flexDirection: 'row',
        flex: 1,
        borderBottomWidth: 1,
        borderColor: '#cecece',
        paddingBottom: 15
    },
    galeryImage: {
        width: 100,
        paddingHorizontal: 3,
        position: 'relative',
        overflow: 'hidden'
    },
    productSelectContainer1: {
        flexDirection: 'row',
        flex: 1,
        width: '100%',
        borderBottomWidth: 1,
        borderColor: '#cecece',
        paddingBottom: 15
    },
    productSelect: {
        width: '100%',
        height: 90,
        marginRight: 5,
        borderRadius: 5,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: '#1a5086'
    },
    overlayBox: {
        position: 'absolute',
        top: 0,
        left: 3,
        backgroundColor: 'rgba(0,0,0,0.7)',
        zIndex: 10,
        width: '100%',
        height: 90,
        borderRadius: 5
    },
    checkBoxRatingContainer: {
        paddingTop: 10
    },
    withoutVariantProduct: {
        marginTop: 15,
        borderTopWidth: 1,
        borderColor: '#e1e1e1'
    },
    checkBoxRatingTitle: {
        fontSize: 22,
        color: '#2a2a2a',
        fontFamily: 'Lato-Bold',
        fontWeight: '600',
        marginBottom: 10
    },
    checkBoxRatingRow: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#e1e1e1',
        paddingBottom: 10
    },
    productLeftContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    productRightContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    currentPrice: {
        fontSize: 24,
        color: '#ff8400',
        fontFamily: 'Lato-Bold',
        fontWeight: '600',
        marginRight: 10
    },
    previousPriceBox: {
        position: 'relative'
    },
    previousPriceText: {
        fontSize: 16,
        fontFamily: 'Lato-Regular',
        color: '#2a2a2a',
        fontStyle: 'italic'
    },
    previousBorderGrid: {
        position: 'absolute',
        top: 13,
        width: '100%',
        height: 1,
        backgroundColor: '#999'
    },
    previousBorder: {
        position: 'absolute',
        top: 10,
        width: '100%',
        height: 1,
        backgroundColor: '#2a2a2a'
    },
    ratingNumberText: {
        fontSize: 16,
        fontFamily: 'Lato-Regular',
        color: '#2a2a2a',
        marginLeft: 10
    },
    ratingStar: {
        fontSize: 20,
        color: '#ff8400'
    },
    descriptionContainer: {
        marginTop: 15,
        borderBottomWidth: 1,
        borderColor: '#e1e1e1',
        paddingBottom: 5
    },
    descriptionTitle: {
        fontSize: 20,
        color: '#2a2a2a',
        fontWeight: '600',
        fontFamily: 'Lato-Bold',
        marginBottom: 5
    },
    descriptionDetail: {
        fontSize: 16,
        fontFamily: 'Lato-Regular',
        color: '#2a2a2a',
        lineHeight: 26,
        marginBottom: 10
    },
    reviewsContainer: {
        marginTop: 15
    },
    reviewsTitle: {
        fontSize: 20,
        fontWeight: '600',
        color: '#2a2a2a',
        fontFamily: 'Lato-Bold',
        marginBottom: 15,
    },
    reviewsCard: {
        flex: 1,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        elevation: 2,
        marginBottom: 8,
        paddingHorizontal: 10,
        paddingVertical: 20,
        backgroundColor: '#fff'
    },
    reviewsUserDetailsBox: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    reviewsUserNameWrapper: {
        flex: 1,
        flexWrap: 'wrap',
        marginLeft: 15
    },
    userReviewTextWrapper: {
        marginLeft: 85
    },
    reviewsUserName: {
        fontSize: 17,
        color: '#2a2a2a',
        fontFamily: 'Lato-Bold',
        textAlign: 'left'
    },
    reviewsUserRating: {
        fontSize: 20,
        color: '#ff8400'
    },
    reviewsUserRatingComment: {
        fontSize: 16,
        color: '#2a2a2a',
        fontFamily: 'Lato-Regular'
    },
    productFooterContainer: {
        backgroundColor: 'green',
        flexDirection: 'row'
    },
    commentFooterBox: {
        flex: 25,
        backgroundColor: '#1a5086',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 10
    },
    heartFooterBox: {
        flex: 25,
        backgroundColor: '#ff8400',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 10
    },
    disableHeartFooterBox: {
        flex: 25,
        backgroundColor: '#ff8400',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 10
    },
    cartFooterBox: {
        flex: 50,
        backgroundColor: '#1a5086',
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 10
    },
    commentFooterIcon: {
        fontSize: 24,
        color: '#fff'
    },
    feedbackIcon: {
        width: 24,
        height: 24,
        resizeMode: 'contain',
        tintColor: '#fff'
    },
    cartFooterIcon: {
        fontSize: 24,
        color: '#fff',
        marginLeft: 35
    },
    addToCartText: {
        fontSize: 18,
        color: '#fff',
        fontFamily: 'Lato-Bold',
        alignItems: 'center'
    },
    cartContainer: {
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    noReviewsView: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        paddingVertical: 20
    },
    noReviewsText: {
        fontSize: 17,
        fontFamily: 'Lato-Regular'
    },
    disabledStyle: {
        backgroundColor: '#ddd'
    },
    variantPickerContainer: {
        marginTop: 10,
        paddingRight: 10,
        backgroundColor: 'white'
    },
    pickerTitle: {
        marginTop: 10,
        marginBottom: Platform.OS == 'ios' ? -3 : -10,
        marginLeft: 10,
        fontSize: 12,
        fontFamily: 'Lato-Regular'
    },
    clearButtonWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 10
    },
    clearButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    clearButtonIcon: {
        fontSize: 22,
        fontFamily: 'Lato-Regular',
        color: '#2a2a2a',
        marginLeft: 5
    },
    clearButtonText: {
        fontSize: 18,
        color: '#2a2a2a'
    }
})