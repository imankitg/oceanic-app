import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, RefreshControl, Dimensions } from 'react-native';
import ShopNewStyle from './ShopNewStyle';
import ViewContainer from '../../components/shared/viewContainer/ViewContainer';
import Footer from "../../components/shared/Footer";
import EmptyDataScreen from '../shared/emptyDataScreen/EmptyDataScreen';
import RatingStars from '../FeedBack/RatingStars';
import ProgressBar from 'react-native-progress/Circle';
import ImageProgress from 'react-native-image-progress';
import Api from '../../services/ProductService';

// redux
import { connect } from 'react-redux';

// Actions
import { fetchProducts, refreshAllProducts, filterApplied } from '../../actions/ShopProducts';

class ProductGridView extends Component {

    constructor(props) {
        super(props);
    }

    product({ item, index }) {
        let { navigation, shopData, secondaryColor } = this.props;
        let dataVariationsLength = item.variationsData && item.variationsData.length - 1;
        return (
            <TouchableOpacity
                style={ShopNewStyle.shopDetails}
                onPress={() => navigation.navigate('ShopNewDetail', { product: shopData.products[index], index: index })}>
                <View>
                    <ImageProgress style={ShopNewStyle.shopProduct}
                        resizeMethod={'resize'}
                        source={{ uri: Api.convertingURL(item.images[0].src) }}
                        indicator={ProgressBar} />
                </View>
                <View style={ShopNewStyle.ratingContainer} pointerEvents="none">
                    <RatingStars defaultRating={item.average_rating} size={16} />
                    <Text style={ShopNewStyle.ratingNumbers}>({Math.ceil(item.average_rating)})</Text>
                </View>
                <View style={ShopNewStyle.productDetailContainter}>
                    <Text style={ShopNewStyle.productDetails} ellipsizeMode="tail" numberOfLines={2}>{Api.convertingLanguage(item.name)}</Text>
                </View>
                {
                    item.variationsData && item.variationsData.length ?
                        <View style={ShopNewStyle.productPriceContainer}>
                            <Text ellipsizeMode="tail" style={[ShopNewStyle.price, { color: secondaryColor }]} numberOfLines={1}>{`$${item.price}-$${item.variationsData[dataVariationsLength].price}`}</Text>
                        </View> :
                        <View style={ShopNewStyle.productPriceContainer}>
                            <Text style={[ShopNewStyle.price, { color: secondaryColor }]}>${item.price}</Text>
                            <View style={ShopNewStyle.previousPriceBox}>
                                <Text style={ShopNewStyle.regularPrice}>{item.regular_price ? `$${item.regular_price}` : null}</Text>
                                <Text style={ShopNewStyle.previousBorderGrid} />
                            </View>
                        </View>
                }
            </TouchableOpacity>
        )
    }

    emptyScreen = () => {
        const { height } = Dimensions.get("window");
        return (
            <View style={{ height: height / 1.195 }}>
                <EmptyDataScreen message={`No Products Found`} icon='filter' />
            </View>
        )
    };

    renderFooter = () => {
        if (!this.props.shopData.loadingData) return null;
        return (
            <Footer />
        );
    };

    render() {
        let { shopData, refreshAllProducts, fetchProducts, filterApplied, language} = this.props;
        return (
            <ViewContainer style={{ backgroundColor: "#f2f2f2" }}>
                <FlatList
                    data={shopData.products}
                    renderItem={this.product.bind(this)}
                    numColumns={2}
                    ListEmptyComponent={this.emptyScreen}
                    keyExtractor={(item, index) => { return index.toString() }}
                    refreshControl={<RefreshControl
                        refreshing={shopData.isRefreshing}
                        onRefresh={() => refreshAllProducts(language)}
                        tintColor="#ff8400" />
                    }
                    onEndReached={() => shopData.isFilter ? filterApplied(shopData.payload, true, language) : fetchProducts(shopData.payload, false, language)}
                    onEndReachedThreshold={0.1}
                    ListFooterComponent={this.renderFooter}
                    contentContainerStyle={{ paddingHorizontal: 10 }}
                />
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        shopData: state.shopData,
        secondaryColor: state.productsData.secondaryColor
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchProducts: (payload, check, language) => dispatch(fetchProducts(payload, check, language)),
        filterApplied: (payload, check, language) => dispatch(filterApplied(payload, check, language)),
        refreshAllProducts: (language) => dispatch(refreshAllProducts(language))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductGridView)
