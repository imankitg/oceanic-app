import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView, Alert, AsyncStorage, ToastAndroid, Platform, FlatList, StyleSheet } from 'react-native';
import ImageProgress from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Circle';
import ShopNewStyle from './ShopNewStyle';
import Toast from 'react-native-easy-toast';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import CustomPicker from '../shared/picker/Picker';
import RatingStars from '../FeedBack/RatingStars';
import Icon from 'react-native-vector-icons/FontAwesome';

import Api from '../../services/ProductService';
import { connect } from 'react-redux';
import { updateCart, updateWishListCount, updateWishList, updateTotal } from '../../actions/Cart';
import SpinKit from '../shared/SpinKit/SpinKit';
import HTMLView from 'react-native-htmlview';
import _ from 'lodash';
import i18n from '../../../i18n';
import ViewContainer from "../shared/viewContainer/ViewContainer";

class ProductDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            position: 0,
            url: '',
            allProducts: [],
            isAdded: true,
            isSelected: false,
            reviewsArray: [],
            reviewLoading: true,
            imageUri: '',
            wishList: [],
            renderVariationData: [], // Variations Data
            filterArray: [],
            variantAttributes: [],
            selectedItem: {},
            showPrice: false,
            changeOption: false
        }
    }

    componentDidMount() {
        let { id, images } = this.props.navigation.state.params.product;
        this.wishListAdded();
        this.addedOrNot();
        Api.getReviews(id).then(res => {
            this.setState({
                reviewsArray: res.data,
                reviewLoading: false
            })
        });
        this.setState({
            imageUri: images[0].src
        })
    }

    componentWillMount() {
        let { variationsData } = this.props.navigation.state.params.product;
        this.props.navigation.setParams({ check: this.getData });
        if (variationsData && variationsData.length) {
            this.variantDataManuplate(variationsData);
        }
    }

    onChangeProductVariant = (value, name) => {
        let keysArray = [], renderVariationData = [];
        let { filterArray, variantAttributes } = this.state;
        let filterData = filterArray.filter((obj) => obj[name] == value);

        keysArray = filterData.length ? Object.keys(filterData[0]) : [];
        variantAttributes = keysArray.filter((value, index, self) => {
            if (value != 'sale_price' && value != 'regular_price' && value != 'variation_id') {
                return self.indexOf(value) === index;
            }
        });

        variantAttributes.forEach(key => {
            let data = _.uniq(_.map(filterData, key));
            data.unshift('Choose an option');
            renderVariationData.push({
                name: key, values: data.map((el, i) => {
                    if (i == 0) {
                        return { label: el, value: '' }
                    }
                    return { label: el, value: el };
                })
            })
        });
        if (renderVariationData.length) {
            this.setState({
                [name]: value,
                filterArray: filterData,
                renderVariationData,
                changeOption: true
            }, () => {
                let result = !Object.keys(this.state).some(key => {
                    if (variantAttributes.includes(key)) return this.state[key] === '';
                });
                if (result) {
                    this.setState({ selectedItem: filterData[0], showPrice: true })
                }
            })
        }
    };

    onClearPress = () => {
        let { variationsData } = this.props.navigation.state.params.product;
        const { variantAttributes } = this.state;
        this.setState({ renderVariationData: [] }, () => {
            this.variantDataManuplate(variationsData);
        });
        variantAttributes.forEach(key => {
            this.setState({ [key]: '' })
        });
        this.setState({ changeOption: false, selectedItem: {}, showPrice: false })
    };

    variantDataManuplate = (variationsData) => {
        let { renderVariationData, variantAttributes } = this.state;
        let productVariations = [], keysArray = [];

        variationsData.forEach(data => {
            let obj = { sale_price: data.sale_price, regular_price: data.regular_price, variation_id: data.id };
            data.attributes.forEach((attribute) => {
                obj[attribute.name] = attribute.option;
            });
            productVariations.push(obj);
        });

        keysArray = Object.keys(productVariations[0]);
        variantAttributes = keysArray.filter((value, index, self) => {
            if (value != 'sale_price' && value != 'regular_price' && value != 'variation_id') {
                return self.indexOf(value) === index;
            }
        });


        variantAttributes.forEach(key => {
            let data = _.uniq(_.map(productVariations, key));
            data.unshift('Choose an option');
            this.setState({ [key]: '' });
            renderVariationData.push({
                name: key, values: data.map((el, i) => {
                    if (i == 0) {
                        return { label: el, value: '' }
                    }
                    return { label: el, value: el };
                })
            })
        });
        this.setState({ renderVariationData, productVariations, filterArray: productVariations, variantAttributes });
    };

    removeFromCartList = () => {
        this.setState({ isSelected: false })
    };

    wishListAdded = async () => {
        const { navigation, wishListCount } = this.props;
        flag = true;
        try {
            await AsyncStorage.getItem('user').then(res => {
                let parsed = JSON.parse(res);
                if (JSON.parse(res) !== null) {
                    for (var key in parsed) {
                        if (parsed[key].id === navigation.state.params.product.id) {
                            wishListCount(parsed.length);
                            this.setState({ isAdded: false });
                            flag = false;
                        }
                    }
                    if (flag) {
                        this.setState({ isAdded: true });
                    }
                }
                this.setState({
                    wishList: parsed
                })
            })
        }
        catch (e) {
            console.log(e)
        }
    };

    renderDescription = () => {
        const { navigation } = this.props;
        let description = navigation.state.params.product.description;
        description = Api.convertingLanguage(description)
        return (
            description ?
                <View style={ShopNewStyle.descriptionContainer}>
                    <Text style={ShopNewStyle.descriptionTitle}>{i18n.t('ProductDetails_Screen.DESCRIPTION')}</Text>

                    <HTMLView
                        value={description.toString()}
                    />
                </View>
                : null
        );
    };

    save = async (data) => {
        try {
            await AsyncStorage.setItem('user', JSON.stringify(data));
        } catch (e) {
            Alert.alert(e)
        }
    };

    saveCartData = async (data) => {
        try {
            await AsyncStorage.setItem('cart_data', JSON.stringify(data));
        } catch (e) {
            Alert.alert(e)
        }
    };

    getData = async () => {
        const { navigation, wishListNumber, isLoggedIn, wishListArray, wishListCount, update_wish_list } = this.props;
        if (isLoggedIn) {
            let wishData = [];
            var flag = true;
            if (this.state.isAdded) {
                try {
                    await AsyncStorage.getItem('user').then(val => {
                        if (JSON.parse(val) !== null) {
                            let parsed = JSON.parse(val);
                            for (var i in parsed) {
                                if (parsed[i].id === navigation.state.params.product.id) {
                                    flag = false;
                                }
                                wishData.push(parsed[i])
                            }
                            if (flag) {
                                wishListArray.push(navigation.state.params.product);
                                wishListCount(wishListNumber + 1);
                                update_wish_list(wishListArray);
                                navigation.state.params.product.quantity = 0;
                                wishData.push(navigation.state.params.product);
                                this.setState({ isAdded: false });
                                Platform.OS === 'ios' ? Alert.alert(i18n.t('ProductDetails_Screen.MESSAGE'), i18n.t('ProductDetails_Screen.ADDED_TO_WISHLIST')) :
                                    ToastAndroid.showWithGravityAndOffset(i18n.t('ProductDetails_Screen.ADDED_TO_WISHLIST'), ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
                                this.setState({ wishList: wishData })
                            }
                        }
                        else {
                            navigation.state.params.product.quantity = 0;
                            wishListArray.push(navigation.state.params.product);
                            wishListCount(wishListNumber + 1);
                            update_wish_list(wishListArray);
                            wishData.push(navigation.state.params.product);
                            this.setState({ isAdded: false, wishList: wishData })
                        }
                        this.save(wishData)
                    }).catch((e) => {
                        Alert.alert(e)
                    });
                }
                catch (e) {
                    Alert.alert(e)
                }
            }
            else {
                let { wishList } = this.state;
                let wishListCopy = wishList.slice();
                for (var key in wishListCopy) {
                    if (wishListCopy[key].id === navigation.state.params.product.id) {
                        wishListCopy.splice(key, 1)
                    }
                }
                wishListArray.pop(navigation.state.params.product);
                wishListCount(wishListNumber - 1);
                update_wish_list(wishListArray);
                this.setState({ isAdded: true });
                this.save(wishListCopy)
            }
        }
        else navigation.navigate('Login')
    };

    addedOrNot = () => {
        let { cartData, navigation } = this.props;
        let { cart } = cartData;
        let flag = true;
        for (var key in cartData) {
            if (cartData[key].id === navigation.state.params.product.id) {
                this.setState({ isSelected: true });
                flag = false
            }
        }
    };

    addCart = () => {
        const { navigation, cartData, cartTotal } = this.props;
        let { product } = navigation.state.params;
        let cloneProduct = Object.assign({}, product);
        let { selectedItem } = this.state;
        var total = cartTotal;
        var added = false;
        let a = cloneProduct.quantity ? cloneProduct.quantity + 1 : 1;
        let { id, quantity } = cloneProduct;

        let sameProduct = !!cartData.find(product => {
            return product.id === id
        });

        if (sameProduct) {
            let foundVariationProduct = cartData.findIndex(item => {
                return item.variation_id == selectedItem.variation_id;
            });

            if (foundVariationProduct == -1) {
                added = true;
                let variantProduct = Object.assign({}, cloneProduct);
                variantProduct.quantity = 1;
                variantProduct.price = selectedItem.sale_price;
                variantProduct.variation_id = selectedItem.variation_id;
                variantProduct.total_price = 0;
                total += Number(selectedItem.sale_price);
                cartData.push(variantProduct);
                this.props.addToCart(cartData);
                this.saveCartData(cartData);
            }
            else {
                added = true;
                cartData[foundVariationProduct].quantity = cartData[foundVariationProduct].quantity + 1;
                Object.keys(selectedItem).length ? total += Number(selectedItem.sale_price) : total += Number(cartData[foundVariationProduct].price);
            }
        }

        let updated = [];
        cloneProduct.add = true;
        cloneProduct.quantity = a;
        cloneProduct.total_price = 0;
        cloneProduct.price = Object.keys(selectedItem).length ? selectedItem.sale_price : cloneProduct.price;
        if (added === false) {
            Object.keys(selectedItem).length ? total += cloneProduct.quantity * Number(selectedItem.sale_price) : total += cloneProduct.quantity * cloneProduct.price;
            Object.keys(selectedItem).length ? cloneProduct.variation_id = selectedItem.variation_id : null;
            cartData.length ? cartData.push(cloneProduct) : updated.push(cloneProduct);
            cartData.length ? this.props.addToCart(cartData) : this.props.addToCart(updated);
            cartData.length ? this.saveCartData(cartData) : this.saveCartData(updated);
        }
        this.props.update_total(total);
        this.setState({ isSelected: true });
        Platform.OS === 'ios' ?
            Alert.alert(i18n.t('ProductDetails_Screen.SUCCESS'), i18n.t('ProductDetails_Screen.PRODUCT_ADDED_MSG'))
            :
            ToastAndroid.showWithGravityAndOffset(i18n.t('ProductDetails_Screen.PRODUCT_ADDED_MSG'), ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
    };

    checkOut(id) {
        let { cartData, navigation } = this.props;
        let listArray = [];
        let purchaseArray = [];
        let state = false;
        AsyncStorage.getItem('USER_LOGIN_INFO', (err, result) => {
            let user = JSON.parse(result);
            if (user) {
                let orderlist = this.props.orderList;
                if (orderlist.length) {
                    orderlist.forEach(list => {
                        listArray.push(list.line_items)
                    });
                    listArray.map((data, index) => {
                        data.map(orders => {
                            purchaseArray.push(orders)
                        })
                    });
                    for (var key in purchaseArray) {
                        if (purchaseArray[key].product_id === id) {
                            state = true;
                        }
                    }
                    if (state) {
                        navigation.navigate('FeedBack', { product_id: id })
                    }
                    else {
                        Alert.alert(i18n.t('ProductDetails_Screen.MESSAGE'), i18n.t('ProductDetails_Screen.ALERT'), [{ text: 'OK' }], { cancelable: false })
                    }
                }
                else {
                    Alert.alert(i18n.t('ProductDetails_Screen.MESSAGE'), i18n.t('ProductDetails_Screen.ALERT'), [{ text: 'OK' }], { cancelable: false })
                }
            }
            else Alert.alert(i18n.t('ProductDetails_Screen.MESSAGE'), i18n.t('ProductDetails_Screen.ALERT'), [{ text: 'OK' }], { cancelable: false })
        });
    }

    renderCapName(name) {
        return name.charAt(0).toUpperCase() + name.slice(1);
    }

    renderReviews({ item }) {
        return (
            <View style={ShopNewStyle.reviewsCard}>
                <View style={ShopNewStyle.reviewsUserDetailsBox}>
                    <Icon name='user-circle-o' size={70} color='rgba(18, 59, 102, 0.3)' />
                    <View style={ShopNewStyle.reviewsUserNameWrapper}>
                        <Text ellipsizeMode="tail" numberOfLines={1} style={ShopNewStyle.reviewsUserName}>{this.renderCapName(item.name)}</Text>
                    </View>
                    <View pointerEvents="none">
                        <RatingStars defaultRating={item.rating} />
                    </View>
                </View>
                <View style={ShopNewStyle.userReviewTextWrapper}>
                    <Text ellipsizeMode="tail" numberOfLines={1} style={ShopNewStyle.reviewsUserRatingComment}>{item.review}</Text>
                </View>
            </View>
        )
    }

    render() {
        const { navigation, primaryColor, secondaryColor } = this.props;
        let { isAdded, isSelected, reviewsArray, reviewLoading, imageUri, renderVariationData, selectedItem, showPrice, changeOption } = this.state;
        let { id, average_rating, images, name, sale_price, regular_price } = navigation.state.params.product;

        if (renderVariationData.length && Object.keys(selectedItem).length) {
            sale_price = selectedItem.sale_price;
            regular_price = selectedItem.regular_price;
        }
        else if (!renderVariationData.length) {
            showPrice = true;
        }

        return (
            <ViewContainer style={ShopNewStyle.productDetailsContainer}>
                <ViewContainer>
                    <ScrollView style={ShopNewStyle.productDetailsWrapper}>
                        {imageUri ? <View style={ShopNewStyle.productSelectedContainer}>
                            <ImageProgress
                                style={ShopNewStyle.productSelected}
                                imageStyle={{ borderRadius: 5 }}
                                resizeMethod={'resize'}
                                source={{ uri: Api.convertingURL(imageUri) }}
                                indicator={ProgressBar} size={50} />
                        </View> : null}
                        {images.length > 1 ? <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            {
                                images.map((obj, index) => {
                                    return (
                                        <TouchableOpacity style={ShopNewStyle.galeryImage} key={index} onPress={() => this.setState({ imageUri: obj.src })}>
                                            {obj.src !== imageUri ? <View style={ShopNewStyle.overlayBox} /> : null}
                                            <ImageProgress
                                                style={ShopNewStyle.productSelect}
                                                imageStyle={{ borderRadius: 5 }}
                                                resizeMethod={'resize'}
                                                source={{ uri: Api.convertingURL(obj.src) }}
                                                indicator={ProgressBar} size={30} />
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </ScrollView> : <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <TouchableOpacity style={ShopNewStyle.galeryImage} onPress={() => this.setState({ imageUri })}>
                                <ImageProgress
                                    style={ShopNewStyle.productSelect}
                                    source={{ uri: Api.convertingURL(images[0].src) }}
                                    indicator={ProgressBar} size={30} />
                            </TouchableOpacity>
                            <TouchableOpacity style={ShopNewStyle.galeryImage} onPress={() => this.setState({ imageUri })}>
                                <ImageProgress
                                    style={ShopNewStyle.productSelect}
                                    source={{ uri: Api.convertingURL(images[0].src) }}
                                    indicator={ProgressBar} size={30} />
                            </TouchableOpacity>
                            <TouchableOpacity style={ShopNewStyle.galeryImage} onPress={() => this.setState({ imageUri })}>
                                <ImageProgress
                                    style={ShopNewStyle.productSelect}
                                    source={{ uri: Api.convertingURL(images[0].src) }}
                                    indicator={ProgressBar} size={30} />
                            </TouchableOpacity>
                            <TouchableOpacity style={ShopNewStyle.galeryImage} onPress={() => this.setState({ imageUri })}>
                                <ImageProgress
                                    style={ShopNewStyle.productSelect}
                                    source={{ uri: Api.convertingURL(images[0].src) }}
                                    indicator={ProgressBar} size={30} />
                            </TouchableOpacity>
                        </ScrollView>}
                        {renderVariationData.length ? renderVariationData.map((attribute, index) => {
                            return (
                                <View key={index} style={ShopNewStyle.variantPickerContainer}>
                                    <Text style={ShopNewStyle.pickerTitle}>{this.renderCapName(attribute.name)}</Text>
                                    <CustomPicker
                                        itemsArray={attribute.values}
                                        selectedValue={this.state[attribute.name]}
                                        onValueChange={(val) => this.onChangeProductVariant(val, attribute.name)}
                                        pickerRef={`${attribute.name}`}
                                    />
                                </View>
                            )
                        }) : null }
                        {renderVariationData.length && changeOption ? <View style={ShopNewStyle.clearButtonWrapper}>
                            <TouchableOpacity onPress={this.onClearPress} style={ShopNewStyle.clearButton} activeOpacity={0.5}>
                                <MaterialIcon style={ShopNewStyle.clearButtonIcon} name="cached" />
                                <Text style={ShopNewStyle.clearButtonText}>Clear</Text>
                            </TouchableOpacity>
                        </View> : null}
                        <View style={[ShopNewStyle.checkBoxRatingContainer, renderVariationData.length ? null : ShopNewStyle.withoutVariantProduct]}>
                            <Text style={ShopNewStyle.checkBoxRatingTitle}>{Api.convertingLanguage(name)}</Text>
                        </View>
                        <View style={ShopNewStyle.checkBoxRatingRow}>
                            <View style={ShopNewStyle.productLeftContainer}>
                                {sale_price ? <Text style={[ShopNewStyle.currentPrice, { color: primaryColor }]}>${sale_price}</Text> : <Text />}
                                {regular_price ? <View style={ShopNewStyle.previousPriceBox}>
                                    <Text style={ShopNewStyle.previousPriceText}>${regular_price}</Text>
                                    <Text style={ShopNewStyle.previousBorder} />
                                </View> : <View />}
                            </View>
                            <View style={ShopNewStyle.productRightContainer}>
                                <View pointerEvents="none">
                                    <RatingStars defaultRating={average_rating} />
                                </View>
                                <Text style={ShopNewStyle.ratingNumberText}>({average_rating})</Text>
                            </View>
                        </View>
                        {this.renderDescription()}
                        {reviewLoading ? <SpinKit /> : reviewsArray.length ? <View style={ShopNewStyle.reviewsContainer}>
                            <Text style={ShopNewStyle.reviewsTitle}>{i18n.t('ProductDetails_Screen.REVIEW')}</Text>
                            <FlatList data={reviewsArray} keyExtractor={(item, index) => index.toString()} renderItem={this.renderReviews.bind(this)}/>
                        </View> : null }
                    </ScrollView>
                </ViewContainer>
                <View style={ShopNewStyle.productFooterContainer}>
                    <TouchableOpacity
                        style={[ShopNewStyle.commentFooterBox, { backgroundColor: secondaryColor }]}
                        onPress={() => this.checkOut(id)}
                        activeOpacity={0.7}>
                        <Image source={require('../../assets/images/feedback.png')} style={ShopNewStyle.feedbackIcon} resizeMethod={'resize'} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[isAdded ? ShopNewStyle.disableHeartFooterBox : ShopNewStyle.heartFooterBox, { backgroundColor: primaryColor }]}
                        onPress={this.getData}
                        activeOpacity={0.7}>
                        <MaterialIcon style={ShopNewStyle.commentFooterIcon} name={isAdded ? "favorite-border" : "favorite"} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[ShopNewStyle.cartFooterBox, { backgroundColor: secondaryColor }, !showPrice ? ShopNewStyle.disabledStyle : null]}
                        onPress={this.addCart}
                        activeOpacity={0.7}
                        disabled={!showPrice}>
                        <View style={ShopNewStyle.cartContainer}>
                            <MaterialIcon style={ShopNewStyle.cartFooterIcon} name="shopping-cart" />
                        </View>
                        <Text style={ShopNewStyle.addToCartText}>{i18n.t('ProductDetails_Screen.ADD_TO_CART')}</Text>
                    </TouchableOpacity>
                </View>
                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'gray' }}
                    position='center'
                    opacity={1}
                    defaultCloseDelay={100}
                    textStyle={{ color: 'white' }} />
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn: state.auth.isLoggedIn,
        cartData: state.cartData.cart,
        wishListNumber: state.cartData.wishListCount,
        orderList: state.ordersData.count,
        wishListArray: state.cartData.wishListData,
        cartTotal: state.cartData.total,
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addToCart: (addToCartArray) => dispatch(updateCart(addToCartArray)),
        wishListCount: (data) => dispatch(updateWishListCount(data)),
        update_wish_list: (data) => dispatch(updateWishList(data)),
        update_total: (total) => dispatch(updateTotal(total))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);