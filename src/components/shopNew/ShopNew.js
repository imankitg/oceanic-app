import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import ViewContainer from '../../components/shared/viewContainer/ViewContainer';
import ShopNewStyle from './ShopNewStyle';
import ProductGridView from './ProductGridView';
import ProductListView from './ProductListView';
import SpinKit from '../shared/SpinKit/SpinKit';
import i18n from '../../../i18n';
// redux
import { connect } from 'react-redux';
// Actions
import { changeGrid } from '../../actions/ShopProducts';

class ShopNew extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { navigation, shopData, changeGrid, primaryColor, secondaryColor, language } = this.props;
        const { categoryLoading, grid, loading } = shopData;

        return (
            <ViewContainer style={{ backgroundColor: "#f2f2f2" }}>
                <View style={ShopNewStyle.tabsContainer}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Filter')}
                        activeOpacity={0.7}
                        style={[ShopNewStyle.filterButton, { backgroundColor: primaryColor }, categoryLoading ? ShopNewStyle.disabledStyle : null]}
                        disabled={categoryLoading}>
                        <Text style={ShopNewStyle.filterListBtnText}>{i18n.t('ShopNew_Screen.APPLY_FILTER')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => changeGrid(!grid)}
                        activeOpacity={0.7}
                        style={[ShopNewStyle.filterButton, {backgroundColor: secondaryColor}]}>
                        <Text style={ShopNewStyle.filterListBtnText}>{grid ? i18n.t('ShopNew_Screen.GRID_VIEW') : i18n.t('ShopNew_Screen.LIST_VIEW')}</Text>
                    </TouchableOpacity>
                </View>
                {
                    !loading ?
                        (grid ?
                            <ProductListView navigation={navigation} language={language}/>
                            :
                            <ProductGridView navigation={navigation} language={language}/>)
                        :
                        <SpinKit />
                }
            </ViewContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        shopData: state.shopData,
        firstTimeProductLoaded: state.shopData.firstTimeProductLoaded,
        primaryColor: state.productsData.primaryColor,
        secondaryColor: state.productsData.secondaryColor,
        language: state.auth.language
    }
}

function mapDispatchToProps(dispatch) {
    return {
        changeGrid: (grid) => dispatch(changeGrid(grid))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopNew)
