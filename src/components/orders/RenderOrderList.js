import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Platform, LayoutAnimation, UIManager } from "react-native";
import OrdersStyle from "./OrdersStyle";
import HeaderBar from '../shared/headerBar/HeaderBar';
import moment from 'moment';
import i18n from '../../../i18n';
import Api from '../../services/ProductService'

import { connect } from 'react-redux';

class RenderOrderList extends Component {
    constructor() {
        super();
        this.state = {
            isToggle: false
        };
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentWillUpdate() {
        LayoutAnimation.spring();
    }

    render() {
        let { order, secondaryColor } = this.props;
        let date = order.date_created;
        let DateRequired = moment(date).format('ll');
        return (
            <View>
                <HeaderBar title={`Order #${order.id}`} />
                <View style={{ paddingHorizontal: 20, paddingVertical: 10, backgroundColor: '#f9f9f9' }}>
                    <View style={OrdersStyle.orderDetails}>
                        <Text style={OrdersStyle.orderDetailsText}>{i18n.t('Order_Screen.ORDER_DATE')}</Text>
                        <Text style={OrdersStyle.orderDetailsText}>{DateRequired}</Text>
                    </View>
                    <View style={OrdersStyle.orderDetails}>
                        <Text style={OrdersStyle.orderDetailsText}>{i18n.t('Order_Screen.STATUS')}</Text>
                        <Text style={OrdersStyle.orderDetailsText}>{order.status}</Text>
                    </View>
                    <View style={OrdersStyle.orderDetails}>
                        <Text style={OrdersStyle.orderDetailsText}>{i18n.t('Order_Screen.PAYMENT_METHOD')}</Text>
                        <Text style={OrdersStyle.orderDetailsText}>{order.payment_method_title}</Text>
                    </View>
                    <View style={OrdersStyle.orderDetails}>
                        <Text style={OrdersStyle.orderDetailsText}>{i18n.t('Order_Screen.TOTAL')}</Text>
                        <Text style={OrdersStyle.orderDetailsTotal}>${order.total}</Text>
                    </View>
                    <View style={OrdersStyle.orderDetails}>
                        <Text style={OrdersStyle.orderDetailsText} />
                        <TouchableOpacity onPress={() => this.setState({ isToggle: !this.state.isToggle })}><Text style={[OrdersStyle.orderDetailsDescription, { color: secondaryColor }]}>{this.state.isToggle ? i18n.t('Order_Screen.HIDE_DETAILS') : i18n.t('Order_Screen.VIEW_DETAILS')}</Text></TouchableOpacity>
                    </View>
                    {
                        this.state.isToggle ?
                            <View>
                                {order.line_items.map((data, key) => {
                                    return (
                                        <View key={key}>
                                            <View style={OrdersStyle.orderDetails}>
                                                <Text style={OrdersStyle.orderItemLists}>{Api.convertingLanguage(data.name)}</Text>
                                                <Text style={OrdersStyle.orderDetailsText}>x {data.quantity}</Text>
                                                <Text style={OrdersStyle.orderDetailsText}>${data.total}</Text>
                                            </View>
                                        </View>
                                    )
                                })}
                                <View style={OrdersStyle.orderDetails}>
                                    <Text style={OrdersStyle.orderDetailsText} />
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ShippingAdress', { categoryTitle: order.id, billing: order.billing, shipping: order.shipping })}><Text style={[OrdersStyle.orderDetailsDescription, { color: secondaryColor }]}>{i18n.t('Order_Screen.VIEW_SHIPPING_ADDRESS')}</Text></TouchableOpacity>
                                </View>
                            </View>
                            : null
                    }

                </View>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        orderList: state.ordersData.count,
        secondaryColor: state.productsData.secondaryColor
    }
}
export default connect(mapStateToProps)(RenderOrderList);