import React, { Component } from 'react';
import { Text, View } from "react-native";
import HeaderBar from '../shared/headerBar/HeaderBar';
import i18n from '../../../i18n';


class ShippingAdress extends React.Component {
    constructor() {
        super();
    }

    render() {
        let { billing, shipping } = this.props.navigation.state.params;
        return (
            <View style={{ backgroundColor: '#f9f9f9', flex: 1 }}>
                <HeaderBar title={i18n.t('Customer_Shipping_Address.BILLING_ADDRESS')} />
                <View style={{ paddingHorizontal: 15, paddingVertical: 14, justifyContent: 'center' }}>
                    <Text>{billing.address_1}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text>{billing.city},</Text>
                        <Text>{billing.postcode}</Text>
                    </View>
                </View>
                <HeaderBar title={i18n.t('Customer_Shipping_Address.SHIPPING_ADDRESS')} />
                <View style={{ paddingHorizontal: 15, paddingVertical: 14, justifyContent: 'center', backgroundColor: '#f9f9f9' }}>
                    <Text>{shipping.address_1}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text>{shipping.city},</Text>
                        <Text>{shipping.postcode}</Text>
                    </View>
                </View>
            </View>
        )
    }
}
export default ShippingAdress;