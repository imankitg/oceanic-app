import { StyleSheet } from "react-native";
import StyleVars from '../../Style/StyleVars';

export default StyleSheet.create({
    "cards": {
        "backgroundColor": "#fff",
        "marginTop": 5,
        "marginBottom": 5,
        "shadowColor": "#000",
        "padding": 10,
        "borderBottomColor": '#ddd',
        "borderBottomWidth": 2
    },
    modalHeader: {
        backgroundColor: StyleVars.Colors.primary,
        height: 56,
        flexDirection: 'row',
        alignItems: 'center',
        elevation: 4,
        justifyContent: 'space-between'
    },
    modalBody: {
        marginTop: 22,
        flex: 1,
        padding: 10
    },
    orderDetails: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 5
    },
    orderDetailsText: {
        fontFamily: 'Lato-Regular',
        color: '#2a2a2a'
    },
    orderItemLists: {
        fontFamily: 'Lato-Regular',
        color: '#2a2a2a',
        width: 200
    },
    orderDetailsTotal: {
        fontFamily: 'Lato-Black',
        color: 'black'
    },
    orderDetailsDescription: {
        fontFamily: 'Lato-Black',
        color: '#1a5086'
    },
    orderListEmptyView: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    orderListEmptyText: {
        fontSize: 20,
        fontFamily: 'Lato-Regular'
    }
});