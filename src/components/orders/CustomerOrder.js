import React, { Component } from 'react';
import {View, FlatList, RefreshControl, Dimensions, AsyncStorage} from "react-native";
import RenderOrderList from './RenderOrderList';
import ViewContainer from '../shared/viewContainer/ViewContainer';
import EmptyDataScreen from '../shared/emptyDataScreen/EmptyDataScreen';
import { fetchOrderNumber } from '../../../src/actions/Orders';
import i18n from '../../../i18n';

//redux
import { connect } from 'react-redux';


class Orders extends React.Component {
    constructor() {
        super();
        this.state = {
            isToggle: false,
            orderList: [],
            firstTimeLoaded: true,
            isRefreshing: false
        }
    }

    async componentDidMount() {

        let promise = new Promise((resolve, reject) => {
            AsyncStorage.getItem('USER_LANGUAGE', (err, language) => {
                if(err) reject(err);
                else resolve(language)
            })
        });

        let lang = await promise;
        this.language = lang;

    }

    getOrderList = () => {
        this.props.getCustomerOrderList(this.props.userid)
    };

    renderOrderList({ item }) {
        return (
            <RenderOrderList order={item} navigation={this.props.navigation} />
        )
    }
    emptyScreen = () => {
        let { navigation } = this.props;
        const { height } = Dimensions.get("window");
        return(
            <View style={{height: height/1.195}}>
            <EmptyDataScreen
                message={i18n.t('EmptyCart_Screen.EMPTY_MSG')}
                buttonText={i18n.t('EmptyCart_Screen.BTN_TEXT')}
                icon='cart'
                onPress={() => navigation.navigate('ShopNew', {language: this.language})} />
            </View>
        )
    };

    render() {
        let { isRefreshing } = this.props;
        let { customerOrderList } = this.props;
        updateorderList = customerOrderList ? customerOrderList.concat([]) : [];
        return (
            <ViewContainer>
                <FlatList
                    data={updateorderList}
                    renderItem={this.renderOrderList.bind(this)}
                    keyExtractor={(item, index) => { return index.toString() }}
                    ListEmptyComponent={this.emptyScreen}
                    refreshControl={<RefreshControl
                                refreshing={isRefreshing}
                                onRefresh={() => this.getOrderList()}
                                tintColor="#ff8400"/>
                            }
                    style={{backgroundColor: '#f2f2f2'}}
                />
            </ViewContainer>
        )
    }
}
function mapStateToProps(state) {
    return {
        isRefreshing: state.ordersData.isRefreshing,
        customerOrderList: state.ordersData.count,
        userid: state.userProfileData.profile.id
    }
}
function mapDispatchToProps(dispatch) {
    return {
        getCustomerOrderList: (id) => dispatch(fetchOrderNumber(id))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Orders);
