'use strict';
// Redux
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import reduxThunk from 'redux-thunk';
import RootReducer from '../reducers/RootReducer';

// Middleware
const logger = createLogger({
    // ...options
});
const middleware = () => {
    return applyMiddleware(reduxThunk, logger)
};
export default createStore(
    combineReducers({
        ...RootReducer
    }),
    middleware()
)

