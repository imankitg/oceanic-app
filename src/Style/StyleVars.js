export default {
    Colors: {
        primary: "#F44336",
        buttonBorderColor: "#375e97",
        headingTextColor: "#ffffff",
        bgWhite: "#ffffff",
        bgTransparent: "transparent",
        green: "#36be41",
        Golden: "#ddb100",
        blue: "#0052cc",
        red: "#A72D06",
        white: "#ffffff",
        grey: "#c3c9ce",
        navBarBackground: "rgb(42, 55, 62)",
        placeholderField: "#BABABD",
        error: "#ff0000",
        lightText: "#b6b5b6"
    },

    Fonts: {}
}
