import { StyleSheet, Platform } from 'react-native';
import StyleVars from '../Style/StyleVars';

export default StyleSheet.create({
    heading: {
        backgroundColor: '#f9f9f9',
        height: 56
    },
    headingText: {
        alignSelf: 'center',
        color: '#2a2a2a',
        fontWeight: Platform.OS === 'ios' ? '500' : '200',
        fontFamily: 'Lato-Regular',
        fontSize: 18,
        lineHeight: 24
    },
    headerLeft: {
        paddingLeft: 16
    },
    headerRight: {
        paddingRight: 16
    },
    headerRightText: {
        fontSize: 17,
        lineHeight: 26,
        color: StyleVars.Colors.headingTextColor
    },
    counter: {
        position: 'absolute',
        marginTop: 5,
        marginLeft: 26,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 12,
        borderRadius: 50,
        borderWidth: 1,
        backgroundColor: StyleVars.Colors.primary,
        borderColor: StyleVars.Colors.primary,
        color: StyleVars.Colors.white,
        width: 20,
        height: 20
    },
    cartIcon: {
        color: '#1a5086'
    }
});
