import {
    ON_FETCH_PRODUCTS_START,
    ON_GET_PRODUCT_SUCCESS,
    ON_FETCH_PRODUCT_FAIL,
    CHANGE_GRID,
    ON_REFRESH_PRODUCT_START,
    ON_REFRESH_PRODUCT_SUCCESS,
    ON_END_REACHED,
    FILTER_APPLIED,
    ON_FETCH_CATEGORY_START,
    ON_GET_CATEGORIES_SUCCESS,
    CHANGED_SLIDER_RANGE,
    CHANGED_INPUT_RANGE,
    ON_GET_FILTER_PRODUCT_SUCCESS,
    ON_END_FILTER_DATA,
    ON_FETCH_FILTER_PRODUCTS_START,
    GET_MORE_FILTER_PRODUCT_SUCCESS,
    UPDATE_CATEGORY,
    CHANGED_FILTER_DATA,
    NEWARIVALS,
} from './types';

import ProductService from '../services/ProductService';
import Api from '../services/ProductService';

export function changeGrid(data) {
    return {
        type: CHANGE_GRID,
        data
    }
}

export function newArival(data) {
    return {
        type: NEWARIVALS,
        data
    }
}

export function fetchProductStart() {
    return {
        type: ON_FETCH_PRODUCTS_START
    }
}

export function getProductSuccess(data) {
    return {
        type: ON_GET_PRODUCT_SUCCESS,
        data
    }
}


export function fetchProductFail() {
    return {
        type: ON_FETCH_PRODUCT_FAIL
    }
}

export function refreshProductSuccess(data) {
    return {
        type: ON_REFRESH_PRODUCT_SUCCESS,
        data
    }
}

export function refreshProductStart() {
    return {
        type: ON_REFRESH_PRODUCT_START
    }
}

export function productFinished() {
    return {
        type: ON_END_REACHED
    }
}

export function fetchCategoryStart() {
    return {
        type: ON_FETCH_CATEGORY_START
    }
}

export function fetchCategories(lang) {
    return (dispatch) => {
        dispatch(fetchCategoryStart());
        ProductService.categories(lang)
            .then(response => {
                dispatch({ type: ON_GET_CATEGORIES_SUCCESS, data: response.data })
            })
    }
}

export function changeSliderRange(values) {
    return {
        type: CHANGED_SLIDER_RANGE,
        values
    }
}

export function changeInputRange({ prop, value }) {
    return {
        type: CHANGED_INPUT_RANGE,
        data: { prop, value }
    }
}

export function getFilterProductSuccess(data) {
    return {
        type: ON_GET_FILTER_PRODUCT_SUCCESS,
        data
    }
}

export function changedFilterData() {
    return {
        type: CHANGED_FILTER_DATA
    }
}

export function updateCategory(selectedCategory) {
    return {
        type: UPDATE_CATEGORY,
        selectedCategory
    }
}

export function filterApplied(payload, check, language) {
    return (dispatch, getState) => {
        if (getState().shopData.loadingData) return null;
        if (!getState().shopData.filterDataEnd && check) return null;
        check ? dispatch(fetchProductStart()) : dispatch({ type: ON_FETCH_FILTER_PRODUCTS_START });

        if (payload && check) {
            payload.page++;
        }

        if (getState().shopData.isCategorySelect) {
            payload.category = getState().shopData.selectedCategory.id;
        }
        ProductService.fetchTopProducts(payload, true, language)
            .then(response => {
                if (response.data && response.data.length) {
                    check ? dispatch({ type: GET_MORE_FILTER_PRODUCT_SUCCESS, data: response.data }) : dispatch(getFilterProductSuccess(response.data));
                }
                else {
                    check ? dispatch({ type: ON_END_FILTER_DATA }) : dispatch(getFilterProductSuccess(response.data));
                }
            });
    };
}

export function fetchProducts(payload, check, lang) {
    let variationProductsArray = [];
    let withoutVariationsArray = [];
    return (dispatch, getState) => {
        if (getState().shopData.loadingData) return null;
        dispatch(fetchProductStart());
        if (payload) {
            payload.page++;
        }
        ProductService.fetchTopProducts(payload, check, lang)
            .then(async (response) => {
                if (response.data && response.data.length) {
                    response.data.filter(data => {
                        if (data.attributes.length) {
                            variationProductsArray.push(data)
                        }

                        else {
                            withoutVariationsArray.push(data)
                        }
                    })
                    if (variationProductsArray.length) {
                        for (let key in variationProductsArray) {
                            let res = await Api.fetchVariationProducts(variationProductsArray[key].id)
                            variationProductsArray[key].variationsData = res.data;
                        }
                        allProducts = [...withoutVariationsArray, ...variationProductsArray]
                        dispatch(getProductSuccess(allProducts));
                    }
                    else {
                        allProducts = withoutVariationsArray;
                        dispatch(getProductSuccess(response.data));
                    }
                }
                else {
                    dispatch(productFinished());
                }
            })
            .catch((err) => dispatch(fetchProductFail()))
    }
}

export function refreshAllProducts(language) {
    console.log('refreshAllProducts language', language)
    return (dispatch, getState) => {
        dispatch(refreshProductStart());
        if (getState().shopData.payload.hasOwnProperty('category')) {
            delete getState().shopData.payload.category;
        }
        ProductService.fetchTopProducts({ page: 1 }, null, language)
            .then(response => {
                if (response.data && response.data.length) {
                    dispatch(refreshProductSuccess(response.data));
                }
            })
            .catch((err) => dispatch())
    }
}
