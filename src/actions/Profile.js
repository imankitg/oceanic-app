export const REMOVE_PROFILE = 'REMOVE_PROFILE';
export const FETCHING_PROFILE_SUCCESS = 'FETCHING_PROFILE_SUCCESS';
export const FETCHING_PROFILE_FAILURE = 'FETCHING_PROFILE_FAILURE';
export const IS_LOADING = 'IS_LOADING';

import CustomerService from '../services/CustomerService';
import { AsyncStorage } from 'react-native';

export function getProfileSuccess(profile) {
    return {
        type: FETCHING_PROFILE_SUCCESS,
        profile
    }
}

export function getProfileFailure() {
    return {
        type: FETCHING_PROFILE_FAILURE
    }
}

export function removeProfile() {
    return {
        type: REMOVE_PROFILE
    }
}

export function loading() {
    return {
        type: IS_LOADING
    }
}

export function updateProfile(id, profile, done) {
    return (dispatch) => {
        dispatch(loading());
        CustomerService.updateCustomer(id, profile)
            .then((response) => {
                AsyncStorage.setItem('USER_LOGIN_INFO', JSON.stringify(response.data || {}));
                let profile = response.data;
                dispatch(getProfileSuccess(profile));
                done && done(profile);
            })
            .catch((error, data) => {
                console.log("error", error);
                console.log("data", data);
                dispatch(getProfileFailure());
            });
    }
}

export function fetchProfile(id) {
    return (dispatch) => {
        CustomerService.customer(id)
            .then((response) => {
                AsyncStorage.setItem('USER_LOGIN_INFO', response.data.user ? JSON.stringify(response.data.user || {}) : JSON.stringify(response.data || {}));
                let profile = response.data;
                dispatch(getProfileSuccess(profile));
            })
            .catch((error, data) => {
                dispatch(getProfileFailure());
            });
    }
}