export const ON_CART_UPDATE = 'ON_CART_UPDATE';
export const ON_WISHLIST_RESET = 'ON_WISHLIST_RESET';
export const ON_TOTAL_UPDATE = 'ON_TOTAL_UPDATE';
export const ON_TOTAL_RESET = 'ON_TOTAL_RESET';
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const SHIPPING_ADDRESS = 'SHIPPING_ADDRESS';
export const WISHLIST_COUNT = 'WISHLIST_COUNT';
export const WISHLIST_DATA = 'WISHLIST_DATA';
export const RESET_CART = 'RESET_CART';

export function updateTotal(data) {
    return {
        type: ON_TOTAL_UPDATE,
        data
    }
}

export function increment(data){
    return{
        type: INCREMENT,
        data
    }
}

export function decrement(data){
    return{
        type: DECREMENT,
        data
    }
}

export function updateCart(data) {
    return {
        type: ON_CART_UPDATE,
        data
    }
}

export function updateWishListCount(data){
    return {
        type: WISHLIST_COUNT,
        data
    }
}

export function updateWishList(data){
    return{
        type: WISHLIST_DATA,
        data
    }
}

export function resetTotal() {
    return {
        type: ON_TOTAL_RESET
    }
}

export function resetWishList() {
    return {
        type: ON_WISHLIST_RESET
    }
}

export function resetCartList() {
    return {
        type: RESET_CART
    }
}

export function shippingAddress(data){
    return {
        type: SHIPPING_ADDRESS,
        data
    }
}