export const ON_REFRESH_COMPLETE = 'ON_REFRESH_COMPLETE';
export const ON_LOAD_MORE_START = 'ON_LOAD_MORE_START';
export const ON_LOAD_MORE_COMPLETE = 'ON_LOAD_MORE_COMPLETE';
export const UPDATE_PAGE_NO = 'UPDATE_PAGE_NO';
export const FIRST_TIME_LOAD = 'FIRST_TIME_LOAD';
export const ON_PRODUCT_DETAIL = 'ON_PRODUCT_DETAIL';
export const ON_TOP_PRODUCT_LOAD = 'ON_TOP_PRODUCT_LOAD';
export const SORT_BY_CATEGORY = 'SORT_BY_CATEGORY';
export const PRODUCT_COMPLETED = 'PRODUCT_COMPLETED';
export const PRODUCT_REFRESH = 'PRODUCT_REFRESH';
export const RELOAD_STATE = 'RELOAD_STATE';
export const CHANGE_PRIMARY_COLOR_THEME = 'CHANGE_PRIMARY_COLOR_THEME';
export const CHANGE_SECONDARY_COLOR_THEME = 'CHANGE_SECONDARY_COLOR_THEME';
export const UPDATE_PRIMARY_COLOR_SHEME = 'UPDATE_PRIMARY_COLOR_SHEME';
export const UPDATE_SECONDARY_COLOR_SHEME = 'UPDATE_SECONDARY_COLOR_SHEME';
export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';

import ProductService from '../services/ProductService';

export function topProducts(data) {
    return {
        type: ON_TOP_PRODUCT_LOAD,
        data
    }
}

export function loadMoreProducts() {
    return {
        type: ON_LOAD_MORE_START
    }
}

export function firstTimeLoad(data) {
    return {
        type: FIRST_TIME_LOAD,
        data
    }
}

export function sortByCategory(data) {
    return {
        type: SORT_BY_CATEGORY,
        data
    }
}

export function onRefreshComplete(data) {
    return {
        type: ON_REFRESH_COMPLETE,
        data
    }
}

export function onLoadMoreComplete(data) {
    return {
        type: ON_LOAD_MORE_COMPLETE,
        data
    }
}
export function reloadState() {
    return {
        type: RELOAD_STATE
    }
}

export function updatePage(pageNo) {
    return {
        type: UPDATE_PAGE_NO,
        pageNo
    }
}

export function onProductDetail(productDetail) {
    return {
        type: ON_PRODUCT_DETAIL,
        productDetail
    }
}

export function refreshProducts(pageNo) {
    return (dispatch) => {
        dispatch(updatePage(pageNo));
    }
}

export function fetchTopProducts() {
    return (dispatch, getState) => {
        if (getState().productsData.topProducts.length) return dispatch(topProducts(getState().productsData.topProducts));
        ProductService.fetchTopProducts()
            .then((response) => {
                dispatch(topProducts(response.data));
            })
            .catch((err) => {
                console.log("err", err);
            })
    }
}

export function productComplete() {
    return {
        type: PRODUCT_COMPLETED
    }
}

export function changePrimaryColorTheme(color) {
    return {
        type: CHANGE_PRIMARY_COLOR_THEME,
        color
    }
}

export function changeSecondaryColor(color) {
    return {
        type: CHANGE_SECONDARY_COLOR_THEME,
        color
    }
}

export function updatePrimaryColorScheme(color_obj) {
    return {
        type: UPDATE_PRIMARY_COLOR_SHEME,
        color_obj
    }
}

export function updateSecondaryColorScheme(color_obj) {
    return {
        type: UPDATE_SECONDARY_COLOR_SHEME,
        color_obj
    }
}

export function changeUserLanguage() {
    return{
        type: CHANGE_LANGUAGE,
    }
}

export function productRefresh() {
    return {
        type: PRODUCT_REFRESH
    }
}

export function fetchProducts(event) {
    return (dispatch, getState) => {
        ProductService.fetchProducts(event.id, getState().productsData.limit, getState().productsData.pageNo, event.language)
            .then((response) => {
                switch (event.type) {
                    case ON_LOAD_MORE_COMPLETE:
                        if (response.data < 10) {
                            dispatch(productComplete())
                        }
                        dispatch(onLoadMoreComplete(response.data));
                        break;
                    case ON_REFRESH_COMPLETE:
                        dispatch(onRefreshComplete(response.data));
                        break;
                    default:
                        dispatch(firstTimeLoad(response.data))
                }
            })
            .catch((err) => {
                console.log("err", err);
            })
    }
}
