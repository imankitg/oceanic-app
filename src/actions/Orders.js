export const ON_UPDATE_ORDER_COUNT = 'ON_UPDATE_ORDER_COUNT';
export const ON_ORDER_REFRESH_COMPLETE = 'ON_ORDER_REFRESH_COMPLETE';
export const ON_ORDER_LOAD_MORE_START = 'ON_ORDER_LOAD_MORE_START';
export const ON_ORDER_LOAD_MORE_COMPLETE = 'ON_ORDER_LOAD_MORE_COMPLETE';
export const ORDER_UPDATE_PAGE_NO = 'ORDER_UPDATE_PAGE_NO';
export const ORDER_FIRST_TIME_LOAD = 'ORDER_FIRST_TIME_LOAD';
export const CUSTOMER_ORDERS = 'CUSTOMER_ORDERS';
export const SELECT_ORDER = 'SELECT_ORDER';
export const SHIPPING_RATES = 'SHIPPING_RATES';
export const SHIPPING_METHOD = 'SHIPPING_METHOD';
export const ON_REFRESHING_START = 'ON_REFRESHING_START';

import OrderService from '../services/OrderService';

export function updateOrderCount(count) {
    return {
        type: ON_UPDATE_ORDER_COUNT,
        count
    }
}

export function loadMoreOrders() {
    return {
        type: ON_ORDER_LOAD_MORE_START
    }
}

export function selectorder(orderId){
    return{
        type: SELECT_ORDER,
        payload: orderId
    }
}

export function firstTimeLoad(data) {
    return {
        type: ORDER_FIRST_TIME_LOAD,
        data
    }
}

export function onRefreshComplete(data) {
    return {
        type: ON_ORDER_REFRESH_COMPLETE,
        data
    }
}

export function onLoadMoreComplete(data) {
    return {
        type: ON_ORDER_LOAD_MORE_COMPLETE,
        data
    }
}

export function updatePage(pageNo) {
    return {
        type: ORDER_UPDATE_PAGE_NO,
        pageNo
    }
}

export function refreshOrders(pageNo) {
    return (dispatch) => {
        dispatch(updatePage(pageNo));
    }
}

export function fetchCustomerOrders(order){
    return{
        type: CUSTOMER_ORDERS,
        order
    }
}

export function fetchOrderNumber(userId) {
    return (dispatch) => {
        dispatch({type: ON_REFRESHING_START});
        OrderService.fetchOrders()
            .then((response) => {                                                                                   
                dispatch(updateOrderCount(response.data.filter(order => order.customer_id == userId)))
            })                                                                                                   
            .catch((err) =>  {console.log("err", err)})                         
    }
}
export function shippingRates(rate){
    return{
        type: SHIPPING_RATES,
        rate
    }
}
export function shippingMethod(method){
    return{
        type: SHIPPING_METHOD,
        method
    }
}


export function fetchOrders(event) {
    return (dispatch, getState) => {
        OrderService.fetchOrders(getState().ordersData.limit, getState().ordersData.pageNo)
            .then((response) => {
                switch(event.type) {
                    case ON_ORDER_LOAD_MORE_COMPLETE:
                        dispatch(onLoadMoreComplete(response.data));
                        break;
                    case ON_ORDER_REFRESH_COMPLETE:
                        dispatch(onRefreshComplete(response.data));
                        break;
                    default:
                        dispatch(firstTimeLoad(response.data))
                }
            })
            .catch((err) =>  {
                console.log("err", err);
            })
    }
}