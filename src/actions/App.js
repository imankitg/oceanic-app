export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

export const SET_LANGUAGE_INTO_REDUX = 'SET_LANGUAGE_INTO_REDUX';

export function login() {
    return {
        type: LOGIN
    }
}

export function logout() {
    return {
        type: LOGOUT
    }
}

export function setLanguageIntoRedux(language) {
    return {
        type: SET_LANGUAGE_INTO_REDUX,
        language: language
    };
}
