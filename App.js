import React, { Component } from 'react';
import { AsyncStorage, Platform, NetInfo } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import RNLanguages from 'react-native-languages';
import i18n, { I18n } from './i18n';
import AppNavigation from './src/routes/Routes';
import SplashScreen from 'react-native-smart-splash-screen';
// Redux
import { connect } from 'react-redux';
import { login } from './src/actions/App';
import { fetchProfile, removeProfile } from './src/actions/Profile';
import { fetchOrderNumber } from './src/actions/Orders';
import { updateCart, updateTotal } from './src/actions/Cart';
import EmptyScreen from './src/components/shared/emptyDataScreen/EmptyDataScreen';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            checkedSignIn: false,
            status: true
        }
    }

    componentWillMount() {
        this.getdata();
        AsyncStorage.getItem('USER_LOGIN_INFO', (err, result) => {
            if (result) {
                let user = JSON.parse(result);
                this.props.fetchProfile(user.id);
                this.props.fetchOrderNumber(user.id);
                this.props.login();
            }
            else this.props.removeProfile();
            AsyncStorage.getItem('CART', (err, result) => {
                this.setState({ checkedSignIn: true });
                if (result) {
                    let cart = JSON.parse(result);
                    let total = 0;
                    if (cart.length) {
                        cart.forEach(item => {
                            total = total + (item.product.price * item.qty);
                        })
                    }
                    this.props.updateTotal(total);
                    this.props.updateCart(cart)
                }
            });
        });
    }

    componentDidMount() {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange)

        SplashScreen.close({
            animationType: SplashScreen.animationType.scale,
            duration: 850,
            delay: 500
        });

        RNLanguages.addEventListener('change', this._onLanguagesChange);
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);

        RNLanguages.removeEventListener('change', this._onLanguagesChange);
    }

    _onLanguagesChange = ({ language }) => {
        i18n.locale = language;
      };

    handleConnectionChange = (isConnected) => {
        if (isConnected) this.setState({ status: isConnected });
        else this.setState({ status: isConnected });
    };

    getdata = async () => {
        try {
            await AsyncStorage.getItem('USER_LOGIN_INFO').then(res => {
                let wishlist = JSON.parse(res)
            })
        }
        catch (e) {
            console.log(e)
        }
    };

    render() {
        const { status } = this.state;
        return (
            status ? (
                Platform.OS === 'ios' ?
                    <SafeAreaView style={{ flex: 1 }} forceInset={{ 'top': 'never' }}><AppNavigation /></SafeAreaView>
                    :
                    <AppNavigation />
            ) :
                <EmptyScreen
                    headingText={`${I18n('Common.OOPS')}!`}
                    message={I18n('Common.NO_INTERNET')}
                    icon='on_internet' />
        )
    }
}

const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: () => dispatch(login()),
        updateCart: (cart) => dispatch(updateCart(cart)),
        updateTotal: (total) => dispatch(updateTotal(total)),
        fetchProfile: (id) => dispatch(fetchProfile(id)),
        fetchOrderNumber: (id) => dispatch(fetchOrderNumber(id)),
        removeProfile: () => dispatch(removeProfile())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App)
