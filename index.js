import { AppRegistry } from 'react-native';
import React, { Component } from "react";
import App from './App';

// Redux
import { Provider } from 'react-redux';
import store from './src/store/CreateStore';

class TradeBakerZ extends Component {
    constructor(props) {
        super(props);
        console.ignoredYellowBox = ['Warning: BackAndroid', 'Warning: View.propTypes'];
    }

    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        );
    }
}


AppRegistry.registerComponent('TradeBakerZ', () => TradeBakerZ);
